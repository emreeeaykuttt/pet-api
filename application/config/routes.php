<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['api/users/(:num)/addresses']['GET'] = 'api/users/addresses/$1';
$route['api/users/(:num)/addresses/(:num)']['GET'] = 'api/users/addresses/$1/$2';
$route['api/users/(:any)/baskets']['GET'] = 'api/users/baskets/$1';
$route['api/users/(:any)/baskets']['POST'] = 'api/users/baskets/$1';
$route['api/users/(:any)/baskets/(:num)']['PUT'] = 'api/users/baskets/$1/$2';
$route['api/users/(:any)/baskets']['DELETE'] = 'api/users/baskets/$1';
$route['api/users/(:any)/baskets/(:num)']['DELETE'] = 'api/users/baskets/$1/$2';
$route['api/users/(:num)/addresses']['POST'] = 'api/users/addresses/$1';
$route['api/users/(:num)/addresses/(:num)']['PUT'] = 'api/users/addresses/$1/$2';
$route['api/users/(:num)/addresses/(:num)']['DELETE'] = 'api/users/addresses/$1/$2';
$route['api/users/(:num)/coupons']['GET'] = 'api/users/coupons/$1';
$route['api/users/(:num)/coupons/(:num)']['GET'] = 'api/users/coupons/$1/$2';
$route['api/users/(:num)/coupons']['POST'] = 'api/users/coupons/$1';
$route['api/users/(:num)/coupons/(:num)']['PUT'] = 'api/users/coupons/$1/$2';
$route['api/users/(:num)/favorites']['GET'] = 'api/users/favorites/$1';
$route['api/users/(:num)/favorites']['POST'] = 'api/users/favorites/$1';
$route['api/users/(:num)/favorites/(:num)']['DELETE'] = 'api/users/favorites/$1/$2';
$route['api/products/(:num)/options']['GET'] = 'api/products/options/$1';
$route['api/products/(:num)/options/(:num)']['GET'] = 'api/products/options/$1/$2';
$route['api/countries/(:num)/cities']['GET'] = 'api/countries/cities/$1';
$route['api/cities/(:num)/districts']['GET'] = 'api/cities/districts/$1';
$route['api/orders/(:num)/details']['GET'] = 'api/orders/details/$1';
$route['api/orders/(:num)/details']['POST'] = 'api/orders/details/$1';
$route['api/admin/option_groups/(:num)/options']['GET'] = 'api/admin/option_groups/options/$1';
$route['api/admin/option_groups/(:num)/options/(:num)']['GET'] = 'api/admin/option_groups/options/$1/$2';
$route['api/admin/orders/(:num)/details']['GET'] = 'api/admin/orders/details/$1';
$route['api/admin/orders/(:num)/details/(:num)']['GET'] = 'api/admin/orders/details/$1/$2';
$route['api/admin/product_options/(:num)/photos']['GET'] = 'api/admin/product_options/photos/$1';
$route['api/admin/products/(:num)/options']['GET'] = 'api/admin/products/options/$1';
$route['api/admin/products/(:num)/options/(:num)']['GET'] = 'api/admin/products/options/$1/$2';
$route['api/admin/coupons/(:num)/users']['GET'] = 'api/admin/coupons/users/$1';
$route['api/admin/coupons/(:num)/users']['POST'] = 'api/admin/coupons/users/$1';
$route['api/admin/coupons/(:num)/users/(:num)']['DELETE'] = 'api/admin/coupons/users/$1/$2';
$route['api/admin/users/(:num)/addresses']['GET'] = 'api/admin/users/addresses/$1';
$route['api/admin/users/(:num)/addresses/(:num)']['GET'] = 'api/admin/users/addresses/$1/$2';











