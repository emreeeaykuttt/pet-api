<?php
     
class Authentication extends MY_Controller 
{
    
    public function __construct()
    {
       parent::__construct();

       $this->load->helper('security');
       $this->load->model('user/user_model','user');
    }

    public function test_get()
    {
        $tos = array('emreeeaykutt@gmail.com');
        $ccs = array();
        $bccs = array();

        $html_message = 'Test content';

        $status = $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com','Test Mail', $html_message);
        print_r($status);
    }

    public function index_post()
    {
        $post = $this->post(null, true);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserFirstName', 'İsim', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('UserLastName', 'Soyisim', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('UserPassword', 'Şifre', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('UserPasswordAgain', 'Şifre Tekrar', 'trim|matches[UserPassword]');
        $this->form_validation->set_rules('UserEmail', 'E-Posta', 'trim|required|valid_email|is_unique[Users.UserEmail]');
        $this->form_validation->set_rules('UserDistanceSalesContract', 'Üyelik Sözleşmesi', 'trim|required');
        $this->form_validation->set_rules('UserCommunicationPermit', 'Kişisel Veri Paylaşımı ve İletişim İzni', 'trim|required');

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');

        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Kayıt işleminiz başarısız oldu. Lütfen tekrar deneyiniz.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'UserFirstName' => $post['UserFirstName'],
                'UserLastName' => $post['UserLastName'],
                'UserPassword' => password_hash($post['UserPassword'], PASSWORD_DEFAULT, ['cost' => 12]),
                'UserEmail' => $post['UserEmail'],
                'UserVerified' => 0,
                'UserVerificationToken' => md5(md5(uniqid(mt_rand(), true)) . $post['UserEmail'] . date("mdY_His")),
                'UserRememberToken' => md5(md5(uniqid(mt_rand(), true)) . $post['UserEmail'] . date("mdY_His")),
                'UserDistanceSalesContract' => 1,
                'UserCommunicationPermit' => 1,
                'UserCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                'UserUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );
            
            $user_id = $this->user->save($data);

            $tos = array($data['UserEmail']);
            $ccs = array();
            $bccs = array();
            $link = WEB_URL . 'auth/login?token='.$data['UserVerificationToken'].'&user_id='.$user_id.'&code='.$post['UserReferenceCode'];
            $link_text = 'Doğrula';

            $mail_content = 
                'Merhaba <b>'. $data['UserFirstName'] . ' ' . $data['UserLastName'] .' ,</b><br>
                Kaydınız oluşturulmuştur.<br />
                Aktifleştirmek için onayınız gerekmektedir.<br />
                Lütfen aşağıdaki butona tıklayarak kaydınızı onaylayınız.';

            $html_message = $this->mail_template($mail_content, $link, $link_text, '1.png');

            $status = $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com','Hesap Doğrulaması', $html_message);

            $this->response(
                [
                    'user_id' => $user_id,
                    'status' => true,
                    'message' => 'Kayıt işleminiz onaylanması için email adresinize gelen maildeki linke tıklayınız.'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }

    public function token_post()
    {
        $this->load->model('db/currency_model','currency');

        $post = $this->post(null, true);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserEmail', 'E-Posta', 'trim|required|valid_email');
        $this->form_validation->set_rules('UserPassword', 'Şifre', 'trim|required|min_length[6]');

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Giriş işleminiz başarısız oldu.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $row = $this->user->validate($post['UserEmail'], $post['UserPassword']);

            if ($row)
            {
                $currency = $this->currency->getByID(1);

                $issued_at = time();
                $not_before = $issued_at + 10;
                $expire = $not_before + 60; 

                $token = AUTHORIZATION::generateToken(
                    array(
                        "iss" => 'emreeeaykut',
                        'scopes' => ['user'],
                        'jti' => md5(uniqid(mt_rand(), true)),
                        "iat" => $issued_at,
                        "nbf" => $not_before,
                        "exp" => $expire,
                        "data" => 
                        array(
                            'UserID' => $row->UserID,
                            'UserFirstName' => $row->UserFirstName,
                            'UserLastName' => $row->UserLastName,
                            'UserEmail' => $row->UserEmail
                        )
                    )
                );

                $this->response(
                    [
                        'token' => $token,
                        'user' => $row,
                        'currency' => $currency,
                        'status' => true,
                        'message' => 'Kişi bulundu.'
                    ], 
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'E-Posta veya şifrenizi yanlış girmiş olabilirsiniz. Tekrar girininiz.'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function reset_post()
    {
        $post = $this->post(null, true);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserPassword', 'Şifre', 'trim|required|min_length[6]');
        $this->form_validation->set_rules('UserPasswordAgain', 'Şifre Tekrar', 'trim|matches[UserPassword]');

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');

        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Şifre değiştirme işleminiz başarısız oldu.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'UserPassword' => password_hash($post['UserPassword'], PASSWORD_DEFAULT, ['cost' => 12]),
                'UserRememberDate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                'UserRememberToken' => md5(md5(uniqid(mt_rand(), true)) . $post['UserPassword'] . date("mdY_His")),
            );

            $control = $this->user->passwordUpdate($data, $post['UserRememberToken'], $post['UserID']);

            if ($control)
            {
                $this->response(
                    [
                        'status' => true,
                        'message' => 'Şifreniz başarılı bir şekilde değiştirilmiştir.'
                    ], 
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Şifre değiştirme işleminde sorun oldu. Lütfen tekrar istek gönderiniz.'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function verification_get()
    {
        $this->load->library('form_validation');

        $config = [
            [
                    'field' => 'UserVerificationToken',
                    'label' => 'UserVerificationToken',
                    'rules' => 'trim|required',
            ],
            [
                    'field' => 'UserID',
                    'label' => 'UserID',
                    'rules' => 'trim|required',
            ],
        ];

        $data = $this->get();

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Hesabınız etkinleştirilirken bir sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $verification_token = $this->security->xss_clean($this->get('UserVerificationToken'));
            $user_id = $this->security->xss_clean($this->get('UserID'));
            $reference_code = $this->get('UserReferenceCode');

            $control = $this->user->verification($verification_token, $user_id);

            if ($control)
            {
                if ($reference_code) 
                {
                    $reference_arr = explode('SREF', $reference_code);

                    if (isset($reference_arr[1])) 
                    {
                        $reference_user_id = $reference_arr[1];
                        $user = $this->user->getByID($reference_user_id);

                        if ($user) 
                        {
                            $this->coupon_create($user_id, $user->UserID);
                        }
                    }
                }

                $this->response(
                    [
                        'status' => true,
                        'message' => 'Hesabınız başarılı bir şekilde etkinleştirilmiştir'
                    ], 
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Hesabınız etkinleştirilirken bir sorun oluştu'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function forgot_get()
    {
        $this->load->library('form_validation');

        $config = [
            [
                    'field' => 'UserEmail',
                    'label' => 'UserEmail',
                    'rules' => 'trim|required|valid_email',
                    'errors' => [
                        'required' => 'E-Posta alanı boş bırakılamaz.',
                        'valid_email' => 'E-Posta alanı geçerli bir e-posta adresi içermelidir.',
                    ],
            ],
        ];

        $data = $this->get();

        $this->form_validation->set_data($data);
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Giriş Hatalı.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $email = $this->security->xss_clean($this->get('UserEmail'));
            $row = $this->user->getByEmail($email);

            if ($row)
            {
                $user = $row;
                $tos = array($user->UserEmail);
                $ccs = array();
                $bccs = array();
                $link = WEB_URL . 'auth/reset?token='.$user->UserRememberToken.'&user_id='.$user->UserID;
                $link_text = 'Şifreyi Değiştir';

                $mail_content = 
                    'Merhaba <b>'. $user->UserFirstName . ' ' . $user->UserLastName .' ,</b><br />
                    Şifrenizi değiştirmek için doğrulanma yapılmıştır.<br />
                    Aşağıdaki butona tıklayıp şifreyi değiştirebilirsiniz.';

                $html_message = $this->mail_template($mail_content, $link, $link_text, '2.png');
                
                $status = $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', 'Şifre Değiştirme', $html_message);

                $this->response(
                    [
                        'status' => true,
                        'message' => 'E-Posta adresinize şifrenizi sıfırlamak için bağlantı gönderdik. Maile girip şifrenizi sıfırlayabilirsiniz.'
                    ], 
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Bu e-postaya kayıtlı kimse yok.'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function invite_post()
    {
        $post = $this->post(null, true);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserEmail', 'E-Posta', 'trim|required|valid_email|is_unique[Users.UserEmail]');

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');

        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Davet gönderme işlemi başarısız oldu.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $user = $this->user->getByID($post['UserID']);
            $tos = array($post['UserEmail']);
            $ccs = array();
            $bccs = array();
            $link = WEB_URL . 'auth/register?code=SREF' . $post['UserID'] . '&email=' . $post['UserEmail'];
            $link_text = 'Kayıt Ol';

            $mail_content = 
                'Merhaba,<br>
                Arkadaşınız <b>'. $user->UserFirstName . ' ' . $user->UserLastName .'</b> sizi petshopevinde.com sitemize kayıt olmak için davet etti.<br />
                Kayıt olmak isterseniz aşağıdaki butona tıklayarak kayıt sayfasından gerekli bilgileri doldurup kayıt olabilirsiniz.<br />
                Kayıt olduğunuz takdirde ilk alışverişte 100 TL ve üzeri sepet tutarınızdan 10TL indirim kodunu kullanmaya hak kazanacaksınız.';

            $html_message = $this->mail_template($mail_content, $link, $link_text, '3.png');

            $status = $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com','petshopevinde.com websitesine kayıt için davet', $html_message);

            $this->response(
                [
                    'status' => true,
                    'message' => 'Davet gönderildi.'
                ], 
                REST_Controller::HTTP_OK
            );
        }
    }

    public function guest_post()
    {
        $this->load->library('form_validation');
        $this->load->model('user/address_model','address');
        $post = $this->post(null, true);

        $this->form_validation->set_rules('UserFirstName', 'İsim', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('UserLastName', 'Soyisim', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('UserPhone', 'Telefon', 'trim|required|regex_match[/^[0-9]{10}$/]');
        $this->form_validation->set_rules('UserEmail', 'E-Posta', 'trim|required|valid_email|is_unique[Users.UserEmail]');
        $this->form_validation->set_rules('UserDistanceSalesContract', 'Üyelik Sözleşmesi', 'trim|required');
        $this->form_validation->set_rules('UserCommunicationPermit', 'Kişisel Veri Paylaşımı ve İletişim İzni', 'trim|required');
        $this->form_validation->set_rules('AddressCountryID', 'Ülke', 'trim|required');
        $this->form_validation->set_rules('AddressOpenAddress', 'Açık Adres', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('AddressIdentityNumber', 'TC Kimlik No', 'trim|required|min_length[11]');
        $this->form_validation->set_rules('AddressZipCode', 'Posta Kodu', 'trim|required|min_length[3]');

        if ($post['AddressCountryID'] == 65 || empty($post['AddressCountryID'])) 
        {
            $this->form_validation->set_rules('AddressCityID', 'Şehir', 'trim|required');
            $this->form_validation->set_rules('AddressDistrictID', 'İlçe', 'trim|required');
        }
        else
        {
            $this->form_validation->set_rules('AddressAlternativeCity', 'Şehir', 'trim|required|min_length[2]');
        }

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');
        $this->form_validation->set_message('regex_match', '{field} alanı 10 rakamdan oluşmaktadır. Örn: 5300000000');

        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Üye olmadan devam etme işleminiz başarısız oldu. Lütfen tekrar deneyiniz.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $this->load->model('db/currency_model','currency');
            $currency = $this->currency->getByID(1);
            $password_generate = crc32($post['UserEmail']);
            
            $data = array(
                'UserFirstName' => $post['UserFirstName'],
                'UserLastName' => $post['UserLastName'],
                'UserPassword' => password_hash($password_generate, PASSWORD_DEFAULT, ['cost' => 12]),
                'UserEmail' => $post['UserEmail'],
                'UserPhone' => $post['UserPhone'],
                'UserVerified' => 1,
                'UserVerificationToken' => md5(md5(uniqid(mt_rand(), true)) . $post['UserEmail'] . date("mdY_His")),
                'UserRememberToken' => md5(md5(uniqid(mt_rand(), true)) . $post['UserEmail'] . date("mdY_His")),
                'UserDistanceSalesContract' => 1,
                'UserCommunicationPermit' => 1,
                'UserCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                'UserUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );
            
            $user_id = $this->user->save($data);

            $address_data = array(
                'AddressUserID' => $user_id,
                'AddressTitle' => 'Adres',
                'AddressType' => 'delivery',
                'AddressFirstName' => $post['UserFirstName'],
                'AddressLastName' => $post['UserLastName'],
                'AddressCountryID' => $post['AddressCountryID'],
                'AddressCityID' => $post['AddressCityID'],
                'AddressDistrictID' => $post['AddressDistrictID'],
                'AddressAlternativeCity' => !empty($post['AddressAlternativeCity']) ? $post['AddressAlternativeCity'] : 0,
                'AddressOpenAddress' => $post['AddressOpenAddress'],
                'AddressIdentityNumber' => $post['AddressIdentityNumber'],
                'AddressZipCode' => $post['AddressZipCode'],
                'AddressPhone' => $post['UserPhone'],
                'AddressEmail' => $post['UserEmail'],
                'AddressBillType' => '',
                'AddressCompanyName' => '',
                'AddressCompanyTaxNumber' => 0,
                'AddressCompanyTaxAdministration' => '',
            );

            $this->address->save($address_data);

            $issued_at = time();
            $not_before = $issued_at + 10;
            $expire = $not_before + 60; 

            $token = AUTHORIZATION::generateToken(
                array(
                    "iss" => 'emreeeaykut',
                    'scopes' => ['user'],
                    'jti' => md5(uniqid(mt_rand(), true)),
                    "iat" => $issued_at,
                    "nbf" => $not_before,
                    "exp" => $expire,
                    "data" => 
                    array(
                        'UserID' => $user_id,
                        'UserFirstName' => $post['UserFirstName'],
                        'UserLastName' => $post['UserLastName'],
                        'UserEmail' => $post['UserEmail']
                    )
                )
            );

            $tos = array($data['UserEmail']);
            $ccs = array();
            $bccs = array();
            $link = WEB_URL . 'auth/login';
            $link_text = 'petshopevinde.com';

            $mail_content = 
                '<b>Merhaba '. $data['UserFirstName'] . ' ' . $data['UserLastName'] .' ,</b><br />
                Kaydınız oluşturulmuştur. Aşağıdaki şifre ile sitemize giriş yapabilir veya şifremi unuttum alanından şifrenizi değiştirebilirsiniz.<br /><br />
                E-Posta: '. $data['UserEmail'] .'<br />
                Şifre: '. $password_generate .'<br /><br />
                Aşağıdaki linkten websitemize geçiş yapabilirsiniz.';

            $html_message = $this->mail_template($mail_content, $link, $link_text, '4.png');

            $status = $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com','Üyelik kaydınız açıldı.', $html_message);

            $this->response(
                [
                    'user_id' => $user_id,
                    'token' => $token,
                    'currency' => $currency,
                    'status' => true,
                    'message' => 'Üye olmadan devam etme işleminiz başarılı oldu. Alışverişe devam edebilirsiniz.'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }

    private function coupon_create($new_user_id, $user_id)
    {
        $this->load->model('product/coupon_model','coupon');
        $this->load->model('product/coupon_relation_model','coupon_relation');
        $this->load->model('user/user_coupon_model','user_coupon');
        $this->load->model('language/language_model', 'language');

        $language_count = $this->language->getCountByStatus();

        $new_user_code = 'Coupon' . $new_user_id . $user_id . 369;
        $old_user_code = 'Coupon' . $user_id . $new_user_id . 369;

        $this->coupon->lang_id = 1;
        $coupon = $this->coupon->getByCode($new_user_code);

        if (empty($coupon)) 
        {
            $new_user_content_id = $this->coupon_relation->save(
                array('CouponRelationModul' => 'Coupon', 'CouponRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            $old_user_content_id = $this->coupon_relation->save(
                array('CouponRelationModul' => 'Coupon', 'CouponRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $new_user = array(
                    'CouponIsActive' => 1,
                    'CouponPublic' => 0,
                    'CouponName' => 'Referans kodu ile üye olunduğu için sepette 10TL indirim',
                    'CouponCode' => $new_user_code,
                    'CouponDescription' => 'Bu kuponu sadece referans kodu ile üye olanlar kullanabiliyor',
                    'CouponAmount' => 10,
                    'CouponRate' => NULL,
                    'CouponMinAmount' => 99,
                    'CuoponQuantity' => 0,
                    'CuoponUnlimited' => 0,
                    'CouponStartDate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    'CouponEndDate' => NULL,
                    'CouponIsEndDate' => 1,
                    'CouponLangID' => $i,
                    'CouponContentID' => $new_user_content_id,
                    'CouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $new_user_coupon_id = $this->coupon->save($new_user);
            }

            $data = array(
                'UserCouponIsUsed' => 0,
                'UserCouponCouponID' => $new_user_coupon_id,
                'UserCouponCouponContentID' => $new_user_content_id,
                'UserCouponUserID' => $new_user_id,
                'UserCouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );
            
            $this->user_coupon->save($data);

            for ($i=1; $i <= $language_count; $i++) 
            {
                $old_user = array(
                    'CouponIsActive' => 1,
                    'CouponPublic' => 0,
                    'CouponName' => 'Referans kodunuz ile birisi üye olduğu için sepette 20TL indirim',
                    'CouponCode' => $old_user_code,
                    'CouponDescription' => 'Bu kuponu sadece referans kodunuz ile başka üye kazandırdığınız zaman kullanabiliyor',
                    'CouponAmount' => 20,
                    'CouponRate' => NULL,
                    'CouponMinAmount' => 99,
                    'CuoponQuantity' => 0,
                    'CuoponUnlimited' => 0,
                    'CouponStartDate' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    'CouponEndDate' => NULL,
                    'CouponIsEndDate' => 1,
                    'CouponLangID' => $i,
                    'CouponContentID' => $old_user_content_id,
                    'CouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $old_user_coupon_id = $this->coupon->save($old_user);
            }

            $data = array(
                'UserCouponIsUsed' => 0,
                'UserCouponCouponID' => $old_user_coupon_id,
                'UserCouponCouponContentID' => $old_user_content_id,
                'UserCouponUserID' => $user_id,
                'UserCouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );
            
            $this->user_coupon->save($data);
        }
    }

}