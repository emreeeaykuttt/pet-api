<?php
     
class Baskets extends MY_Controller 
{
   
    public function __construct() {
       parent::__construct();
       $this->load->model('product/basket_model','basket');
    }
    
    public function index_delete()
    {
        $this->basket->deleteByUnregisteredID();

        $this->response(
        [
            'status' => true,
            'message' => 'Sepetteki üye olmayan kişilerin ürünleri silindi'
        ],
        REST_Controller::HTTP_OK
    );
    }
}