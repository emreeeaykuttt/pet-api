<?php

class Categories extends MY_Controller 
{
    
    public function __construct() {
       parent::__construct();

       $this->load->model('product/category_model','category');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->category->getByContentID($content_id);
        }
        elseif (!empty($this->get('parent_id'))) 
        {
            $result = $this->category->getAllByParentID($this->get('parent_id'));
        }
        else
        {
            $parentArr = array();
            $categories = $this->category->getAll();

            foreach($categories as $key => $val){
                $parentArr[$val['CategoryParentID']][$val['CategoryContentID']] = array(
                    'CategoryName' => $val['CategoryName'],
                    'CategoryParentID' => $val['CategoryParentID'],
                    'CategorySlug' => $val['CategorySlug'],
                    'CategoryContentID' => $val['CategoryContentID'],
                    'CategoryLangID' => $val['CategoryLangID'],
                );
            }

            $result = $parentArr;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kategori bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}