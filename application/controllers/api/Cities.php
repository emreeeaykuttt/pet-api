<?php

class Cities extends MY_Controller
{
    
    public function __construct()
    {
       parent::__construct();
       $this->load->model('db/district_model','district');
    }

    public function districts_get($id)
    {
        if (!empty($id)) 
        {
            $result = $this->district->getAllByCityID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'İlçe bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}