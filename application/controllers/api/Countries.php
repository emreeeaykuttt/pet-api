<?php

class Countries extends MY_Controller
{
    
    public function __construct()
    {
       parent::__construct();
       $this->load->model('db/country_model','country');
       $this->load->model('db/city_model','city');
    }

    public function index_get($id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->country->lang_id = $this->get('lang_id');
        }

        if (!empty($id)) 
        {
            $result = $this->country->getByID($id);
        }
        else
        {
            $result = $this->country->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ülke bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function cities_get($id)
    {
        if (!empty($id)) 
        {
            $result = $this->city->getAllByCountryID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Şehir bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}