<?php

class Option_groups extends MY_Controller
{
    
    public function __construct()
    {
       parent::__construct();
       $this->load->model('product/option_group_model','option_group');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->option_group->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id) && empty($this->get('option_join'))) 
        {
            $result = $this->option_group->getByContentID($content_id);
        }
        elseif (!empty($content_id) && !empty($this->get('option_join'))) 
        {
        	$result = $this->option_group->getByContentIDAndJoinOption($content_id);
        }
        elseif (!empty($this->get('option_join')))
        {
            $result = $this->option_group->getAllAndJoinOption();
        }
        else
        {
            $result = $this->option_group->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon Grubu bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}