<?php

class Options extends MY_Controller
{
    
    public function __construct()
    {
       parent::__construct();
       $this->load->model('product/option_model','option');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->option->getByContentIDAndJoinOption($content_id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}