<?php

class Orders extends MY_Controller 
{
    
    public function __construct() 
    {
       parent::__construct();
       
       $this->load->model('order/order_model','order');
       $this->load->model('order/detail_model','detail');
    }

    public function index_get($id = '')
    {
        $result = null;
        $user_id = $this->get('user_id');

        if (!empty($id)) 
        {
            if (!empty($user_id)) {
                $this->verify_request('user');
                $result = $this->order->getByIDAndUserID($id, $user_id);
            } else {
                $iyzico_token = $this->get('iyzico_token');
                if (!empty($iyzico_token)) {
                    $this->load->model('user/user_model','user');
                    $this->load->model('db/currency_model','currency');

                    $order = $this->order->getByID($id);
                    $row = $this->user->getByID($order->OrderUserID);
                    $currency = $this->currency->getByID(1);

                    $issued_at = time();
                    $not_before = $issued_at + 10;
                    $expire = $not_before + 60; 

                    $token = AUTHORIZATION::generateToken(
                        array(
                            "iss" => 'emreeeaykut',
                            'scopes' => ['user'],
                            'jti' => md5(uniqid(mt_rand(), true)),
                            "iat" => $issued_at,
                            "nbf" => $not_before,
                            "exp" => $expire,
                            "data" => 
                            array(
                                'UserID' => $row->UserID,
                                'UserFirstName' => $row->UserFirstName,
                                'UserLastName' => $row->UserLastName,
                                'UserEmail' => $row->UserEmail
                            )
                        )
                    );

                    $this->response(
                        [
                            'token' => $token,
                            'user' => $row,
                            'currency' => $currency,
                        ], 
                        REST_Controller::HTTP_OK
                    );die;
                }
            }
        }
        else
        {
            $this->verify_request('user');

            $result = $this->order->getAllByUserID($user_id);
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $this->verify_request('user');

        $post = $this->post(null, true);

        $data = array(
            'OrderUserID' => $post['OrderUserID'],
            'OrderStatus' => $post['OrderStatus'],
            'OrderShippingStatus' => $post['OrderShippingStatus'],
            'OrderCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        $id = $this->order->save($data);

        if ($id)
        {
            $this->response(
                [
                    'OrderID' => $id,
                    'status' => true,
                    'message' => 'Sipariş oluşturuldu'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş oluşturulurken bir hata oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_put($id)
    {
        $this->verify_request('user');

        $put = $this->put(null, true);

        if ($put['UpdateWhich'] == 'amount')
        {
            $this->amount_update($id, $put);
        }
        elseif ($put['UpdateWhich'] == 'address_and_tracking_number')
        {
            $this->address_and_tracking_number_update($id, $put);
        }
        elseif ($put['UpdateWhich'] == 'status')
        {
            $this->status_update($id, $put);
        }
        elseif ($put['UpdateWhich'] == 'address')
        {
            $this->address_update($id, $put);
        }
    }

    private function amount_update($id, $put)
    {
        $data = array(
            'OrderAmount' => $put['OrderAmount'],
            'OrderVatAmount' => $put['OrderVatAmount'],
            'OrderVatInclusiveAmount' => $put['OrderVatInclusiveAmount'],
            'OrderDiscountRate' => $put['OrderDiscountRate'],
            'OrderDiscountAmount' => $put['OrderDiscountAmount'],
            'OrderDiscountInclusiveVatAmount' => $put['OrderDiscountInclusiveVatAmount'],
            'OrderDiscountInclusiveAmountAndWithoutVat' => $put['OrderDiscountInclusiveAmountAndWithoutVat'],
            'OrderDiscountInclusiveAmount' => $put['OrderDiscountInclusiveAmount'],
            'OrderCargoAmount' => $put['OrderCargoAmount'],
            'OrderLatestAmount' => $put['OrderLatestAmount'],
            'OrderCurrencyCode' => $put['OrderCurrencyCode'],
            'OrderCurrencyShortName' => $put['OrderCurrencyShortName'],
            'OrderCouponContentID' => $put['OrderCouponContentID'],
            'OrderCouponAmount' => $put['OrderCouponAmount'],
            'OrderCouponRate' => $put['OrderCouponRate'],
            'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        $result = $this->order->update(array('OrderID' => $id, 'OrderUserID' => $put['OrderUserID']), $data);

        if ($result)
        {
            $this->response(
                [
                    'status' => true,
                    'message' => 'Sipariş fiyatların toplamları güncellendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş fiyatları güncellenirken sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function address_and_tracking_number_update($id, $put)
    {
        $data = array(
            'OrderTrackingNumber' => $put['OrderTrackingNumber'],
            'OrderPaymentType' => $put['OrderPaymentType'],
            'OrderDeliveryFirstName' => $put['OrderDeliveryFirstName'],
            'OrderDeliveryLastName' => $put['OrderDeliveryLastName'],
            'OrderDeliveryCountry' => $put['OrderDeliveryCountry'],
            'OrderDeliveryCountryCode' => $put['OrderDeliveryCountryCode'],
            'OrderDeliveryCity' => $put['OrderDeliveryCity'],
            'OrderDeliveryDistrict' => $put['OrderDeliveryDistrict'],
            'OrderDeliveryOpenAddress' => $put['OrderDeliveryOpenAddress'],
            'OrderDeliveryIdentityNumber' => $put['OrderDeliveryIdentityNumber'],
            'OrderDeliveryZipCode' => $put['OrderDeliveryZipCode'],
            'OrderDeliveryPhone' => $put['OrderDeliveryPhone'],
            'OrderDeliveryEmail' => $put['OrderDeliveryEmail'],
            'OrderBillingFirstName' => $put['OrderBillingFirstName'],
            'OrderBillingLastName' => $put['OrderBillingLastName'],
            'OrderBillingCountry' => $put['OrderBillingCountry'],
            'OrderBillingCountryCode' => $put['OrderBillingCountryCode'],
            'OrderBillingCity' => $put['OrderBillingCity'],
            'OrderBillingDistrict' => $put['OrderBillingDistrict'],
            'OrderBillingOpenAddress' => $put['OrderBillingOpenAddress'],
            'OrderBillingIdentityNumber' => $put['OrderBillingIdentityNumber'],
            'OrderBillingZipCode' => $put['OrderBillingZipCode'],
            'OrderBillingPhone' => $put['OrderBillingPhone'],
            'OrderBillingEmail' => $put['OrderBillingEmail'],
            'OrderBillingCompanyName' => $put['OrderBillingCompanyName'],
            'OrderBillingCompanyTaxNumber' => $put['OrderBillingCompanyTaxNumber'],
            'OrderBillingCompanyTaxAdministration' => $put['OrderBillingCompanyTaxAdministration'],
            'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        $result = $this->order->update(array('OrderID' => $id, 'OrderUserID' => $put['OrderUserID']), $data);

        if ($result)
        {
            if ($put['OrderPaymentType'] == 'transfer') 
            {
                $order = $this->order->getByID($id);
                $details = $this->detail->getAllByOrderIDAndUserID($id, $order->OrderUserID);

                $coupon_discount = '';
                if ($order->OrderCouponContentID)
                {
                    if ($order->OrderCouponAmount)
                    {
                        $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponAmount . $order->OrderCurrencyCode . '<br />';
                    }
                    else
                    {
                        $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponRate . '%' . '<br />';
                    }
                }

                $detail_table = '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">';
                foreach ($details as $key => $detail) 
                {
                    $product_option_json = json_decode($detail['DetailProductOptionContent'], true);
                    $product_option_text = '';

                    if ($product_option_json) {
                        foreach ($product_option_json as $key => $option) {
                            $product_option_text .= $option['group'] . ': ' . $option['option'] . '<br />';
                        }
                    }

                    $detail_photo = '';
                    if ($detail['DetailProductPhoto'])
                    {
                        $detail_photo = base_url() . $detail['DetailProductPhoto'];
                    }
                    else
                    {
                        $detail_photo = base_url() . 'upload/product/null-photo.png';
                    }

                    $discount_area = '';
                    if (floatval($detail['DetailDiscountPrice']) > 0)
                    {
                        $discount_area = 'İndirim: ' . $detail['DetailDiscountPrice'] . $detail['DetailCurrencyCode'] . '<br />
                                        İndirimli Fiyatı: ' . $detail['DetailDiscountInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />';
                    }

                    $detail_table .= 
                    '<tr>
                        <td width="30">
                        <td width="100" valign="top">
                            <img src="'. $detail_photo .'" width="100" />
                        </td>
                        <td width="20">
                        <td width="450">
                            <b>' . $detail['DetailProductName'] . ' (' . $detail['DetailSKU'] . ')</b><br />' .
                            $product_option_text .
                            'Birim Fiyatı: ' . $detail['DetailVatInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />' .
                            $discount_area .
                            'Adet: ' . $detail['DetailQuantity'] . '<br />
                            Toplam Fiyat: ' . $detail['DetailLatestPrice'] . $detail['DetailCurrencyCode'] . '<br />
                        </td>
                    </tr>
                    <tr>
                        <td width="600" colspan="4" style="padding: 0 30px 0 30px;"><hr /></td>
                    </tr>
                    ';
                }
                $detail_table .= '</table>';

                $order_table = 
                    '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">
                        <tr>
                            <td style="padding: 0 30px 0 30px;">
                                <b>Ödeme Bilgileri</b><br />
                                Sipariş No: '. $order->OrderTrackingNumber .'<br />
                                Sipariş Tutarı: ' . $order->OrderVatInclusiveAmount . $order->OrderCurrencyCode . '<br />
                                İndirim Tutarı: ' . $order->OrderDiscountAmount . $order->OrderCurrencyCode . '<br />'.
                                $coupon_discount .
                                'Kargo Ücreti: ' . $order->OrderCargoAmount . $order->OrderCurrencyCode . '<br />
                                Toplam Tutar: ' . $order->OrderLatestAmount . $order->OrderCurrencyCode .'<br /><br />
                                <hr />
                            </td>
                        </tr>
                    </table>'
                ;

                $tos = array($order->OrderBillingEmail);
                $ccs = array();
                $bccs = array();
                $link = WEB_URL . 'profile/orders?tracking_number=' . $order->OrderTrackingNumber;
                $link_text = 'Sipariş Numarası: ' . $order->OrderTrackingNumber;
                $mail_content = 
                    '<b>Merhaba '. $order->OrderBillingFirstName . ' ' . $order->OrderBillingLastName .',</b><br />
                    Teşekkür ederiz, siparişiniz başarıyla oluşturuldu.<br />
                    Lütfen aşağıdaki hesap numarasına ödeme yaparak siparişinizi tamamlayınız.<br />
                    Açıklama bölümüne SİPARİŞ NUMARANIZI yazmayı unutmayınız.<br /><br />
                    <b>ÖDENECEK TUTAR: '. $order->OrderLatestAmount . $order->OrderCurrencyCode .'</b><br />
                    <b>Garanti Bankası</b><br />
                    Hesap: 287 - 6297377 GÖLCÜK<br />
                    IBAN: TR12 0006 2000 2870 0006 2973 77<br />
                    <small>(Eğer ödeme yaptıysanız bekleyiniz. Site yöneticisi siparişinizi onaylandığında siparişiniz işleme alınacaktır.)</small>
                    <br /><br /><hr /><br />' . 
                    $order_table .
                    $detail_table
                ;
                $mail_title = 'Siparişiniz Oluşturuldu';

                $html_message = $this->mail_template($mail_content, $link, $link_text, '6.png');
                $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', $mail_title, $html_message);
            }

            $tos = array('info@petshopevinde.com');
            $ccs = array('');
            $bccs = array();
            $link = PANEL_URL . 'order/main?tracking_number=' . $put['OrderTrackingNumber'];
            $link_text = 'Sipariş Numarası: ' . $put['OrderTrackingNumber'];
            $mail_content = 
                '<b>Merhaba Admin,</b><br />
                Bir kullanıcıdan sipariş geldi. Aşağıdaki linkten siparişi görüntüleyip durumunu kontrol edebilirsin.';
            $mail_title = 'Sipariş Geldi';

            $html_message = $this->mail_template($mail_content, $link, $link_text, '5.png');
            $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', $mail_title, $html_message);

            $this->response(
                [
                    'status' => true,
                    'message' => 'Sipariş adresi güncellendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş adresi güncellenirken sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function address_update($id, $put)
    {
        $validate = $this->_address_validate($put);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Gerekli alanları doldurunuz.',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'OrderDeliveryFirstName' => $put['OrderDeliveryFirstName'],
                'OrderDeliveryLastName' => $put['OrderDeliveryLastName'],
                'OrderDeliveryIdentityNumber' => $put['OrderDeliveryIdentityNumber'],
                'OrderDeliveryZipCode' => $put['OrderDeliveryZipCode'],
                'OrderDeliveryEmail' => $put['OrderDeliveryEmail'],
                'OrderDeliveryPhone' => $put['OrderDeliveryPhone'],
                'OrderDeliveryCountry' => $put['OrderDeliveryCountry'],
                'OrderDeliveryCity' => $put['OrderDeliveryCity'],
                'OrderDeliveryDistrict' => $put['OrderDeliveryDistrict'],
                'OrderDeliveryOpenAddress' => $put['OrderDeliveryOpenAddress'],
                'OrderBillingFirstName' => $put['OrderBillingFirstName'],
                'OrderBillingLastName' => $put['OrderBillingLastName'],
                'OrderBillingIdentityNumber' => $put['OrderBillingIdentityNumber'],
                'OrderBillingZipCode' => $put['OrderBillingZipCode'],
                'OrderBillingEmail' => $put['OrderBillingEmail'],
                'OrderBillingPhone' => $put['OrderBillingPhone'],
                'OrderBillingCountry' => $put['OrderBillingCountry'],
                'OrderBillingCity' => $put['OrderBillingCity'],
                'OrderBillingDistrict' => $put['OrderBillingDistrict'],
                'OrderBillingOpenAddress' => $put['OrderBillingOpenAddress'],
                'OrderBillingCompanyName' => $put['OrderBillingCompanyName'],
                'OrderBillingCompanyTaxNumber' => $put['OrderBillingCompanyTaxNumber'],
                'OrderBillingCompanyTaxAdministration' => $put['OrderBillingCompanyTaxAdministration'],
                'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            $result = $this->order->update(array('OrderID' => $id, 'OrderUserID' => $put['OrderUserID']), $data);

            if ($result)
            {
                $this->response(
                    [
                        'status' => true,
                        'message' => 'Sipariş adresi güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Sipariş adresi güncellenirken sorun oluştu'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    private function status_update($id, $put)
    {
        $data = array(
            'OrderStatus' => $put['OrderStatus'],
            'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        if (isset($put['OrderShippingStatus'])) 
        {
            if ($put['OrderShippingStatus'] == 'pending')
            {
                $result = $this->order->update(array('OrderID' => $id, 'OrderUserID' => $put['OrderUserID'], 'OrderShippingStatus' => $put['OrderShippingStatus']), $data);
            }
        }
        else
        {
            $result = $this->order->update(array('OrderID' => $id, 'OrderUserID' => $put['OrderUserID']), $data);
        }

        if ($result)
        {
            if (isset($put['OrderShippingStatus'])) 
            {
                if ($put['OrderShippingStatus'] == 'pending')
                {
                    $order = $this->order->getByID($id);
                    $tos = array('info@petshopevinde.com');
                    $ccs = array('');
                    $bccs = array();
                    $link = PANEL_URL . 'order/main?tracking_number=' . $order->OrderTrackingNumber;
                    $link_text = 'Sipariş Numarası: ' . $order->OrderTrackingNumber;
                    $mail_content = 
                        '<b>Merhaba Admin,</b><br />
                        Bir kullanıcı siparişini iptal etti. Aşağıdaki linkten siparişi görüntüleyip para iadesini gerçekleştirebilirsin';
                    $mail_title = 'Sipariş İptal Edildi';

                    $html_message = $this->mail_template($mail_content, $link, $link_text, '5.png');
                    $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', $mail_title, $html_message);
                }
            }
            else
            {
                $order = $this->order->getByID($id);
                $details = $this->detail->getAllByOrderIDAndUserID($id, $order->OrderUserID);

                $tos = array($order->OrderBillingEmail);
                $ccs = array();
                $bccs = array();
                $link = WEB_URL . 'profile/orders?tracking_number=' . $order->OrderTrackingNumber;
                $link_text = 'Sipariş Numarası: ' . $order->OrderTrackingNumber;

                $coupon_discount = '';
                if ($order->OrderCouponContentID)
                {
                    if ($order->OrderCouponAmount)
                    {
                        $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponAmount . $order->OrderCurrencyCode . '<br />';
                    }
                    else
                    {
                        $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponRate . '%' . '<br />';
                    }
                }

                $detail_table = '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">';
                foreach ($details as $key => $detail) 
                {
                    $product_option_json = json_decode($detail['DetailProductOptionContent'], true);
                    $product_option_text = '';

                    if ($product_option_json) {
                        foreach ($product_option_json as $key => $option) {
                            $product_option_text .= $option['group'] . ': ' . $option['option'] . '<br />';
                        }
                    }

                    $detail_photo = '';
                    if ($detail['DetailProductPhoto'])
                    {
                        $detail_photo = base_url() . $detail['DetailProductPhoto'];
                    }
                    else
                    {
                        $detail_photo = base_url() . 'upload/product/null-photo.png';
                    }

                    $discount_area = '';
                    if (floatval($detail['DetailDiscountPrice']) > 0)
                    {
                        $discount_area = 'İndirim: ' . $detail['DetailDiscountPrice'] . $detail['DetailCurrencyCode'] . '<br />
                                        İndirimli Fiyatı: ' . $detail['DetailDiscountInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />';
                    }

                    $detail_table .= 
                    '<tr>
                        <td width="30">
                        <td width="100" valign="top">
                            <img src="'. $detail_photo .'" width="100" />
                        </td>
                        <td width="20">
                        <td width="450">
                            <b>' . $detail['DetailProductName'] . ' (' . $detail['DetailSKU'] . ')</b><br />' .
                            $product_option_text .
                            'Birim Fiyatı: ' . $detail['DetailVatInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />' .
                            $discount_area .
                            'Adet: ' . $detail['DetailQuantity'] . '<br />
                            Toplam Fiyat: ' . $detail['DetailLatestPrice'] . $detail['DetailCurrencyCode'] . '<br />
                        </td>
                    </tr>
                    <tr>
                        <td width="600" colspan="4" style="padding: 0 30px 0 30px;"><hr /></td>
                    </tr>
                    ';
                }
                $detail_table .= '</table>';

                $order_table = 
                    '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">
                        <tr>
                            <td style="padding: 0 30px 0 30px;">
                                <b>Ödeme Bilgileri</b><br />
                                Sipariş No: '. $order->OrderTrackingNumber .'<br />
                                Sipariş Tutarı: ' . $order->OrderVatInclusiveAmount . $order->OrderCurrencyCode . '<br />
                                İndirim Tutarı: ' . $order->OrderDiscountAmount . $order->OrderCurrencyCode . '<br />'.
                                $coupon_discount .
                                'Kargo Ücreti: ' . $order->OrderCargoAmount . $order->OrderCurrencyCode . '<br />
                                Toplam Tutar: ' . $order->OrderLatestAmount . $order->OrderCurrencyCode .'<br /><br />
                                <hr />
                            </td>
                        </tr>
                    </table>'
                ;

                $mail_content = 
                    '<b>Merhaba '. $order->OrderBillingFirstName . ' ' . $order->OrderBillingLastName .',</b><br />
                    Siparişiniz Onaylandı. En kısa zamanda hazırlanıp kargoya verilecektir.
                    <br /><br /><hr /><br />'.
                    $order_table .
                    $detail_table
                ;
                $mail_title = 'Siparişiniz Onaylandı';

                $html_message = $this->mail_template($mail_content, $link, $link_text, '6.png');
                $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', $mail_title, $html_message);
            }

            $this->response(
                [
                    'status' => true,
                    'message' => 'Siparişin ödeme durumu güncellendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Siparişin ödeme durumu güncellenirken sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function _address_validate($inputs)
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($inputs['OrderDeliveryFirstName'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryFirstName';
            $data['error_string'][] = 'Ad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryLastName'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryLastName';
            $data['error_string'][] = 'Soyad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryIdentityNumber'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryIdentityNumber';
            $data['error_string'][] = 'TC Kimlik No alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryZipCode'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryZipCode';
            $data['error_string'][] = 'Posta Kodu alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryEmail'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryEmail';
            $data['error_string'][] = 'Email alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryPhone'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryPhone';
            $data['error_string'][] = 'Telefon alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryCountry'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryCountry';
            $data['error_string'][] = 'Ülke alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryCity'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryCity';
            $data['error_string'][] = 'Şehir alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryDistrict'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryDistrict';
            $data['error_string'][] = 'İlçe alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderDeliveryOpenAddress'] == '')
        {
            $data['inputerror'][] = 'OrderDeliveryOpenAddress';
            $data['error_string'][] = 'Açık Adres alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingFirstName'] == '')
        {
            $data['inputerror'][] = 'OrderBillingFirstName';
            $data['error_string'][] = 'Ad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingLastName'] == '')
        {
            $data['inputerror'][] = 'OrderBillingLastName';
            $data['error_string'][] = 'Soyad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingIdentityNumber'] == '')
        {
            $data['inputerror'][] = 'OrderBillingIdentityNumber';
            $data['error_string'][] = 'TC Kimlik No alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingZipCode'] == '')
        {
            $data['inputerror'][] = 'OrderBillingZipCode';
            $data['error_string'][] = 'Posta Kodu alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingEmail'] == '')
        {
            $data['inputerror'][] = 'OrderBillingEmail';
            $data['error_string'][] = 'Email alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingPhone'] == '')
        {
            $data['inputerror'][] = 'OrderBillingPhone';
            $data['error_string'][] = 'Telefon alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingCountry'] == '')
        {
            $data['inputerror'][] = 'OrderBillingCountry';
            $data['error_string'][] = 'Ülke alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingCity'] == '')
        {
            $data['inputerror'][] = 'OrderBillingCity';
            $data['error_string'][] = 'Şehir alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingDistrict'] == '')
        {
            $data['inputerror'][] = 'OrderBillingDistrict';
            $data['error_string'][] = 'İlçe alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['OrderBillingOpenAddress'] == '')
        {
            $data['inputerror'][] = 'OrderBillingOpenAddress';
            $data['error_string'][] = 'Açık Adres alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

    public function details_get($id)
    {
        $this->verify_request('user');

        if (!empty($id)) 
        {
            $user_id = !empty($this->get('user_id')) ? $this->get('user_id') : 0;
            $result['details'] = $this->detail->getAllByOrderIDAndUserID($id, $user_id);
            $result['order'] = $this->order->getByIDAndUserID($id, $user_id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş detayı bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function details_post($id)
    {
        $this->verify_request('user');

        $this->load->model('product/product_option_model','product_option');

        $post = $this->post(null, true);

        $data = array(
            'DetailOrderID' => $id,
            'DetailUserID' => $post['DetailUserID'],
            'DetailProductContentID' => $post['DetailProductContentID'],
            'DetailProductOptionContentID' => $post['DetailProductOptionContentID'],
            'DetailProductOptionContent' => $post['DetailProductOptionContent'],
            'DetailProductName' => $post['DetailProductName'],
            'DetailCategoryName' => $post['DetailCategoryName'],
            'DetailProductPhoto' => $post['DetailProductPhoto'],
            'DetailPrice' => $post['DetailPrice'],
            'DetailVatRate' => $post['DetailVatRate'],
            'DetailVatPrice' => $post['DetailVatPrice'],
            'DetailVatInclusivePrice' => $post['DetailVatInclusivePrice'],
            'DetailDiscountRate' => $post['DetailDiscountRate'],
            'DetailDiscountPrice' => $post['DetailDiscountPrice'],
            'DetailDiscountInclusiveVatPrice' => $post['DetailDiscountInclusiveVatPrice'],
            'DetailDiscountInclusivePriceAndWithoutVat' => $post['DetailDiscountInclusivePriceAndWithoutVat'],
            'DetailDiscountInclusivePrice' => $post['DetailDiscountInclusivePrice'],
            'DetailQuantity' => $post['DetailQuantity'],
            'DetailLatestPrice' => $post['DetailLatestPrice'],
            'DetailCurrencyCode' => $post['DetailCurrencyCode'],
            'DetailSKU' => $post['DetailSKU'],
            'DetailCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        $order_detail_id = $this->detail->save($data);

        $this->product_option->quantity_update($data['DetailProductOptionContentID'], $data['DetailQuantity']);

        if ($order_detail_id)
        {
            $this->product_option->update(
                array(
                    'ProductOptionContentID' => $data['DetailProductOptionContentID'],
                    'ProductOptionUnlimited' => 0,
                    'ProductOptionQuantity' => 0,
                ), 
                array('ProductOptionStockStatus' => 0)
            );

            $this->response(
                [
                    'status' => true,
                    'message' => 'Sipariş detayı oluşturuldu'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş detayı oluşturulurken bir hata oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}