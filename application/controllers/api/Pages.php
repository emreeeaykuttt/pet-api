<?php

class Pages extends MY_Controller 
{
    
    public function __construct() 
    {
       parent::__construct();
       $this->load->model('page/page_model','page');
    }

    public function index_get()
    {
        $this->page->lang_id = $this->get('lang_id');
        
        $result = $this->page->getAll();

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sayfalar bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    // public function imagecontrol_get($start = 0, $end = 1000)
    // {
    //     $this->load->library('image_lib');

    //     $files = glob('./upload/slider/*');
    //     $counter = 0;

    //     foreach ($files as $key => $file) 
    //     {
    //         if ($file != './upload/product/default/new' && $file != './upload/product/default/photo') 
    //         {
    //             if ($key >= $start && $key < $end) 
    //             {
    //                 // $filename = ltrim($file, './upload/slider/');
    //                 $f_arr = explode('/', $file);
    //                 $filename = $f_arr[3];
    //                 $path = $file;
    //                 $config['image_library'] = 'GD2';
    //                 $config['source_image'] = $path;
    //                 $config['maintain_ratio'] = TRUE;
    //                 $config['master_dim'] = 'auto';
    //                 $config['quality'] = '90%';
    //                 // $config['create_thumb'] = TRUE;
    //                 // $config['thumb_marker'] = '_1024';
    //                 $config['width'] = 1024;
    //                 $config['new_image'] = './resize/lg/upload/slider/' . $filename;
    //                 $this->image_lib->initialize($config);
    //                 $this->image_lib->resize();
    //                 $this->image_lib->clear();

    //                 $config['image_library'] = 'GD2';
    //                 $config['source_image'] = $path;
    //                 $config['maintain_ratio'] = TRUE;
    //                 $config['master_dim'] = 'auto';
    //                 $config['quality'] = '90%';
    //                 $config['width'] = 600;
    //                 $config['new_image'] = './resize/md/upload/slider/' . $filename;
    //                 $this->image_lib->initialize($config);
    //                 $this->image_lib->resize();
    //                 $this->image_lib->clear();

    //                 $config['image_library'] = 'GD2';
    //                 $config['source_image'] = $path;
    //                 $config['maintain_ratio'] = TRUE;
    //                 $config['master_dim'] = 'auto';
    //                 $config['quality'] = '90%';
    //                 $config['width'] = 300;
    //                 $config['new_image'] = './resize/sm/upload/slider/' . $filename;
    //                 $this->image_lib->initialize($config);
    //                 $this->image_lib->resize();
    //                 $this->image_lib->clear();

    //                 $config['image_library'] = 'GD2';
    //                 $config['source_image'] = $path;
    //                 $config['maintain_ratio'] = TRUE;
    //                 $config['master_dim'] = 'auto';
    //                 $config['quality'] = '90%';
    //                 $config['width'] = 150;
    //                 $config['new_image'] = './resize/xs/upload/slider/' . $filename;
    //                 $this->image_lib->initialize($config);
    //                 $this->image_lib->resize();
    //                 $this->image_lib->clear();

    //                 $counter = $key;
    //             }
    //         }
    //     }

    //     echo $counter . " success";
    // }

}