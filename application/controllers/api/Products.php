<?php

class Products extends MY_Controller
{
    
    public function __construct() 
    {
       parent::__construct();
       $this->load->model('product/product_model','product');
       $this->load->model('product/category_model','category');
       $this->load->model('product/product_relation_model','product_relation');
    }

    public function index_get($content_id = '')
    {
        $this->load->model('product/option_group_model','option_group');

        if (!empty($this->get('lang_id'))) 
        {
            $this->product->lang_id = $this->get('lang_id');
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $this->load->model('product/product_option_photo_model', 'product_option_photo');
            $this->load->model('product/tag_model', 'tag');

            $result['product'] = $this->product->getByContentIDAndFirstOption($content_id);
            $result['gallery'] = $this->product_option_photo->getAllByProductOptionID($result['product']->ProductOptionID);
            $tags = $result['product']->ProductTags;
            $tag_arr = array();
            if (!empty($tags)) {
                foreach (json_decode($tags) as $key => $tag) {
                    $this->tag->lang_id = $this->get('lang_id');
                    $tag_row = $this->tag->getByContentID($tag);
                    $tag_arr[] = $tag_row->TagTitle;
                }
            }
            $result['tags'] = $tag_arr;
        }
        elseif (!empty($this->get('category_content_id'))) 
        {
            $result = $this->list_by_category_content_id($this->get('category_content_id'));
        }
        elseif (!empty($this->get('best_selling'))) 
        {
            $this->load->model('order/detail_model','detail');

            $this->detail->limit = $this->get('limit');
            $products = $this->detail->bestSellingProducts();

            $result = $this->list_by_product_content_ids($products);
        }
        else
        {
            $result = $this->list_all();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function list_by_category_content_id($category_content_id)
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($this->get('limit'))) 
        {
            $this->product->limit = $this->get('limit');
            $this->product->offset = $this->get('offset') ? $this->get('offset') : 0;
        }
        elseif (!empty($this->get('per_page'))) 
        {
            $this->product->per_page = $this->get('per_page');
            $this->product->page = $this->get('page');
        }

        if (!empty($this->get('filter'))) 
        {
            $this->product->filter = $this->get('filter');
        }

        if (!empty($this->get('min')) && !empty($this->get('min'))) 
        {
            $this->product->min = $this->get('min');
            $this->product->max = $this->get('max');
        }

        if (!empty($this->get('tag'))) 
        {
            $this->load->model('product/tag_model', 'tag');
            
            $tag_ids = array();
            $tags = explode('-', $this->get('tag'));
            foreach ($tags as $key => $value)
            {
                $tag = $this->tag->getByTitle($value);

                if (!empty($tag)) 
                {
                    $tag_ids[] = $tag->TagContentID;
                }
            }

            if (empty($tag_ids))
            {
                $tag_ids[] = 0;
            }
            
            $this->product->tag = $tag_ids;
        }

        if (!empty($this->get('search'))) 
        {
            $this->product->search = $this->get('search');
        }

        if (!empty($this->get('sort'))) 
        {
            $this->product->sort = $this->get('sort');
        }

        $data['products'] = $this->product->getAllByCategoryID($category_content_id);
        $data['products_total'] = $this->product->getCountAllByCategoryID($category_content_id);
        $category = $this->category->getByContentID($category_content_id);
        $parent_category = $this->category->getByContentID($category->CategoryParentID);
        $data['parent_category_slug'] = $parent_category->CategorySlug;
        $data['category_name'] = $category->CategoryName;
        $product_all = $this->product->getAllByCategoryID($category_content_id, false);
        $data['options_group'] = $this->option_group->getAllAndIsProducts($product_all);

        return $data;
    }

    private function list_all()
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($this->get('limit'))) 
        {
            $this->product->limit = $this->get('limit');
            $this->product->offset = $this->get('offset') ? $this->get('offset') : 0;
        }
        elseif (!empty($this->get('per_page'))) 
        {
            $this->product->per_page = $this->get('per_page');
            $this->product->page = $this->get('page');
        }

        if (!empty($this->get('filter'))) 
        {
            $this->product->filter = $this->get('filter');
        }

        if (!empty($this->get('min')) && !empty($this->get('min'))) 
        {
            $this->product->min = $this->get('min');
            $this->product->max = $this->get('max');
        }

        if (!empty($this->get('tag'))) 
        {
            $this->load->model('product/tag_model', 'tag');
            
            $tag_ids = array();
            $tags = explode('-', $this->get('tag'));
            foreach ($tags as $key => $value)
            {
                $tag = $this->tag->getByTitle($value);

                if (!empty($tag)) 
                {
                    $tag_ids[] = $tag->TagContentID;
                }
            }

            if (empty($tag_ids))
            {
                $tag_ids[] = 0;
            }
            
            $this->product->tag = $tag_ids;
        }

        if (!empty($this->get('search'))) 
        {
            $this->product->search = $this->get('search');
        }

        if (!empty($this->get('sort'))) 
        {
            $this->product->sort = $this->get('sort');
        }

        $data['products'] = $this->product->getAllByStatus(1);
        $data['products_total'] = $this->product->getCountAllByStatus(1);
        $data['parent_category_slug'] = '';
        $data['category_name'] = '';
        $product_all = $this->product->getAllByStatus(1, false);
        $data['options_group'] = $this->option_group->getAllAndIsProducts($product_all);

        return $data;
    }

    private function list_by_product_content_ids($products)
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($this->get('limit'))) 
        {
            $this->product->limit = $this->get('limit');
            $this->product->offset = $this->get('offset') ? $this->get('offset') : 0;
        }
        elseif (!empty($this->get('per_page'))) 
        {
            $this->product->per_page = $this->get('per_page');
            $this->product->page = $this->get('page');
        }

        if (!empty($this->get('filter'))) 
        {
            $this->product->filter = $this->get('filter');
        }

        if (!empty($this->get('min')) && !empty($this->get('min'))) 
        {
            $this->product->min = $this->get('min');
            $this->product->max = $this->get('max');
        }

        if (!empty($this->get('tag'))) 
        {
            $this->load->model('product/tag_model', 'tag');
            
            $tag_ids = array();
            $tags = explode('-', $this->get('tag'));
            foreach ($tags as $key => $value)
            {
                $tag = $this->tag->getByTitle($value);

                if (!empty($tag)) 
                {
                    $tag_ids[] = $tag->TagContentID;
                }
            }

            if (empty($tag_ids))
            {
                $tag_ids[] = 0;
            }
            
            $this->product->tag = $tag_ids;
        }

        if (!empty($this->get('search'))) 
        {
            $this->product->search = $this->get('search');
        }

        if (!empty($this->get('sort'))) 
        {
            $this->product->sort = $this->get('sort');
        }

        $data['products'] = $this->product->getAllByProductContentIDs($products);
        $data['products_total'] = $this->product->getCountAllProductContentIDs($products);
        $data['parent_category_slug'] = '';
        $data['category_name'] = '';

        return $data;
    }

    public function options_get($content_id, $option_content_id = '')
    {
        $this->load->model('product/product_option_model','product_option');

        if (!empty($this->get('lang_id'))) 
        {
            $this->product_option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id) && !empty($option_content_id)) 
        {
            $result = $this->product_option->getByProductContentIDAndContentID($content_id, $option_content_id);
        }
        elseif (!empty($content_id))
        {
            if (!empty($this->get('option_content_ids'))) 
            {
                $this->load->model('product/product_option_photo_model', 'product_option_photo');

                $result = array();
                $counter = 0;
                $option_val = '';
                foreach (explode(',', $this->get('option_content_ids')) as $key => $value) 
                {
                    $counter++;
                    $option_val .= '"'.$counter.'":"'.$value.'",';
                }

                $option_val = rtrim($option_val, ',');

                $result['product'] = $this->product_option->getByProductContentIDAndOptionContentIDJson($content_id, $option_val);

                if (!empty($result)) 
                {
                    $result['gallery'] = $this->product_option_photo->getAllByProductOptionID($result['product']->ProductOptionID);
                }
            }
            else
            {
                $result = $this->product_option->getAllByProductContentID($content_id);
            }
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün opsiyonu bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function categories_get($content_id, $category_content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->category->getByContentID($content_id);
        }
        elseif (!empty($this->get('parent_id'))) 
        {
            $result = $this->category->getAllByParentID($this->get('parent_id'));
        }
        else
        {
            $parentArr = array();
            $categories = $this->category->getAll();

            foreach($categories as $key => $val){
                $parentArr[$val['CategoryParentID']][$val['CategoryContentID']] = array(
                    'CategoryName' => $val['CategoryName'],
                    'CategoryParentID' => $val['CategoryParentID'],
                    'CategorySlug' => $val['CategorySlug'],
                    'CategoryContentID' => $val['CategoryContentID'],
                    'CategoryLangID' => $val['CategoryLangID'],
                );
            }

            $result = $parentArr;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kategori bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }
}