<?php

class Sliders extends MY_Controller {
    
    public function __construct() 
    {
       parent::__construct();
       $this->load->model('slider/slider_model','slider');
    }

    public function index_get($content_id = '')
    {
        $this->slider->lang_id = $this->get('lang_id');

        if (!empty($content_id)) 
        {
            $result = NULL;
        }
        else
        {
            $result = $this->slider->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Slider fotoğrafları bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}