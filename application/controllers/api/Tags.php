<?php

class Tags extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('product/tag_model','tag');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->tag->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->tag->getByContentID($content_id);
        }
        else
        {
            $result = $this->tag->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Etiket bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}