<?php

class Users extends MY_Controller {
    
    public function __construct()
    {
        parent::__construct();
    
        $this->load->model('user/user_model','user');
        $this->load->model('user/address_model','address');
    }

    public function index_get($id = '')
    {
        $this->verify_request('user');

        if (!empty($id))
        {
            $row = $this->user->getByID($id);

            if ($row)
            {
                $this->response($row, REST_Controller::HTTP_OK);   
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Kullanıcı bulunamadı'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function index_put($id)
    {
        $this->verify_request('user');

        $this->load->library('form_validation');
        $put = $this->put(null, true);

        if($put['UserEmail'] != $put['UserEmailOld']) 
        {
           $is_unique =  '|is_unique[Users.UserEmail]';
        } 
        else 
        {
           $is_unique =  '';
        }

        $password_rules = '';
        $password_again_rules = '';
        $password_old_error_message = '';

        if (!empty($put['UserPasswordOld'])) 
        {
            $is_password = $this->user->passwordValidate($put['UserEmailOld'], $put['UserPasswordOld']);
            
            if ($is_password) 
            {
                $password_rules = ['field' => 'UserPassword', 'label' => 'Yeni Şifre', 'rules' => 'trim|required|min_length[6]'];
                $password_again_rules = ['field' => 'UserPasswordAgain', 'label' => 'Yeni Şifre Tekrar', 'rules' => 'trim|matches[UserPassword]'];
            }
            else
            {
                $password_old_error_message = 'Şifre yanlış';
            }
        }

        $config = [
            [
                    'field' => 'UserFirstName',
                    'label' => 'İsim',
                    'rules' => 'trim|required|min_length[3]',
            ],
            [
                    'field' => 'UserLastName',
                    'label' => 'Soyisim',
                    'rules' => 'trim|required|min_length[3]',
            ],
            [
                    'field' => 'UserEmail',
                    'label' => 'E-Posta',
                    'rules' => 'trim|required|valid_email' . $is_unique,
            ],
            [
                    'field' => 'UserPhone',
                    'label' => 'Telefon',
                    'rules' => 'trim|required|regex_match[/^[0-9]{10}$/]',
            ],
            [
                    'field' => 'UserDateBirth',
                    'label' => 'Doğum Tarihi',
                    'rules' => 'trim|required|min_length[10]',
            ],
            [
                    'field' => 'UserGender',
                    'label' => 'Cinsiyet',
                    'rules' => 'trim|required|min_length[1]',
            ],
            $password_rules,
            $password_again_rules,
        ];

        $this->form_validation->set_data($put);
        $this->form_validation->set_rules($config);

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('max_length', '{field} alanı en fazla {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        $this->form_validation->set_message('is_unique', '{field} başka bir kullanıcı tarafından kullanılıyor.');
        $this->form_validation->set_message('regex_match', '{field} alanı 10 rakamdan oluşmaktadır. Örn: 5300000000');
        $this->form_validation->set_message('matches', '{field} alanı {param} alanı ile eşleşmiyor.');

        if ($this->form_validation->run() == FALSE || !empty($password_old_error_message)) 
        {
            if ($this->form_validation->run() == FALSE) 
            {
                $errors = $this->form_validation->error_array();
            }

            if (!empty($password_old_error_message)) 
            {
                $errors['UserPasswordOld'] = $password_old_error_message;
            }

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Profil güncellenirken bir sorun oluştu.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'UserFirstName' => $put['UserFirstName'],
                'UserLastName' => $put['UserLastName'],
                'UserEmail' => $put['UserEmail'],
                'UserPhone' => $put['UserPhone'],
                'UserDateBirth' => $put['UserDateBirth'],
                'UserGender' => $put['UserGender'],
                'UserUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            if (!empty($put['UserPasswordOld'])) 
            {
                $data['UserPassword'] = password_hash($put['UserPassword'], PASSWORD_DEFAULT, ['cost' => 12]);
            }
            
            $updated = $this->user->update(array('UserID' => $id, 'UserEmail' => $put['UserEmailOld']), $data);

            $this->response(
                [
                    'status' => true,
                    'message' => 'Profil güncellendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }

    public function coupons_get($id = '', $coupon_content_id = '')
    {
        $this->verify_request('user');

        $this->load->model('product/coupon_model','coupon');

        if (!empty($this->get('lang_id'))) 
        {
            $this->coupon->lang_id = $this->get('lang_id');
        }

        if (!empty($id) && !empty($coupon_content_id)) 
        {
            $result = $this->coupon->getByContentIDAndUserID($coupon_content_id, $id);
        }
        elseif (!empty($id))
        {
            $code = $this->get('code');

            if (!empty($code)) 
            {
                $coupon = $this->coupon->getByCode($code);

                $result = NULL;

                if ($coupon) 
                {
                    $coupon_by_user = $this->coupon->getByCodeAndUserID($code, $id);
                    if ($coupon_by_user)
                    {
                        if ($coupon_by_user->UserCouponIsUsed == 0) 
                        {
                            $result = $coupon_by_user;
                        }
                    }
                    else
                    {
                        if ($coupon->CouponPublic == 1) 
                        {
                            if ($coupon->CuoponUnLimited == 1 || $coupon->CuoponQuantity > 0) 
                            {
                                $result = $coupon;
                            }
                        }
                    }
                }
            }
            else
            {
                $result = $this->coupon->getAllByUserID($id);
            }
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kupon bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function coupons_post($id = '')
    {
        $this->verify_request('user');

        $this->load->model('user/user_coupon_model','user_coupon');
        $this->load->model('product/coupon_model','coupon');

        $post = $this->post(null, true);

        $data = array(
            'UserCouponIsUsed' => $post['UserCouponIsUsed'],
            'UserCouponCouponContentID' => $post['UserCouponCouponContentID'],
            'UserCouponUserID' => $id,
            'UserCouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );
        
        $insert_id = $this->user_coupon->save($data);

        if ($insert_id)
        {
            $this->coupon->quantity_change($post['UserCouponCouponContentID'], 'minus');

            $this->response(
                [
                    'status' => true,
                    'message' => 'Kupon başarılı bir şekilde kullanıldı'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kupon kullanılırken bir sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function coupons_put($id = '', $user_coupon_id = '')
    {
        $this->verify_request('user');

        $this->load->model('user/user_coupon_model','user_coupon');

        $put = $this->put(null, true);

        $data = array(
            'UserCouponIsUsed' => $put['UserCouponIsUsed'],
            'UserCouponUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );
        
        $updated = $this->user_coupon->update(
                        array('UserCouponUserID' => $id, 'UserCouponID' => $user_coupon_id), 
                        $data
                    );

        if ($updated)
        {
            $this->response(
                [
                    'status' => true,
                    'message' => 'Kupon başarılı bir şekilde kullanıldı'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kupon kullanılırken bir sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function addresses_get($id = '', $address_id = '')
    {
        $this->verify_request('user');

        if (!empty($this->get('lang_id'))) 
        {
            $this->address->lang_id = $this->get('lang_id');
        }

        if (!empty($id) && !empty($address_id)) 
        {
            $result = $this->address->getByIDAndUserID($address_id, $id);
        }
        elseif (!empty($id))
        {
            $result = $this->address->getAllByUserID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kullanıya ait adres bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function addresses_post($id)
    {
        $this->verify_request('user');

        $post = $this->post(null, true);

        $validate = $this->_address_validate($post);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Adres oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'AddressUserID' => $id,
                'AddressTitle' => $post['AddressTitle'],
                'AddressType' => $post['AddressType'],
                'AddressFirstName' => $post['AddressFirstName'],
                'AddressLastName' => $post['AddressLastName'],
                'AddressCountryID' => $post['AddressCountryID'],
                'AddressCityID' => $post['AddressCityID'],
                'AddressDistrictID' => $post['AddressDistrictID'],
                'AddressAlternativeCity' => $post['AddressAlternativeCity'],
                'AddressOpenAddress' => $post['AddressOpenAddress'],
                'AddressIdentityNumber' => replace_space($post['AddressIdentityNumber']),
                'AddressZipCode' => replace_space($post['AddressZipCode']),
                'AddressPhone' => replace_space($post['AddressPhone']),
                'AddressEmail' => replace_space($post['AddressEmail']),
                'AddressBillType' => $post['AddressBillType'],
                'AddressCompanyName' => $post['AddressCompanyName'],
                'AddressCompanyTaxNumber' => $post['AddressCompanyTaxNumber'],
                'AddressCompanyTaxAdministration' => $post['AddressCompanyTaxAdministration'],
            );

            $address_id = $this->address->save($data);
         
            $this->response(
                [
                    'AddressID' => $address_id,
                    'status' => true,
                    'message' => 'Adres başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function addresses_put($id, $address_id)
    {
        $this->verify_request('user');

        $put = $this->put(null, true);

        $validate = $this->_address_validate($put);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Adres güncellerken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'AddressTitle' => $put['AddressTitle'],
                'AddressType' => $put['AddressType'],
                'AddressFirstName' => $put['AddressFirstName'],
                'AddressLastName' => $put['AddressLastName'],
                'AddressCountryID' => $put['AddressCountryID'],
                'AddressCityID' => $put['AddressCityID'],
                'AddressDistrictID' => $put['AddressDistrictID'],
                'AddressAlternativeCity' => $put['AddressAlternativeCity'],
                'AddressOpenAddress' => $put['AddressOpenAddress'],
                'AddressIdentityNumber' => replace_space($put['AddressIdentityNumber']),
                'AddressZipCode' => replace_space($put['AddressZipCode']),
                'AddressPhone' => replace_space($put['AddressPhone']),
                'AddressEmail' => replace_space($put['AddressEmail']),
                'AddressBillType' => $put['AddressBillType'],
                'AddressCompanyName' => $put['AddressCompanyName'],
                'AddressCompanyTaxNumber' => $put['AddressCompanyTaxNumber'],
                'AddressCompanyTaxAdministration' => $put['AddressCompanyTaxAdministration'],
            );

            $this->address->update(
                array('AddressID' => $address_id),
                $data
            );
            
            $this->response(
                [
                    'AddressID' => $address_id,
                    'status' => true,
                    'message' => 'Adres başarılı bir şekilde güncellendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }

    public function addresses_delete($id, $address_id)
    {
        $this->verify_request('user');

        $this->address->delete(array('AddressID' => $address_id, 'AddressUserID' => $id));
       
        $this->response(
            [
                'status' => true,
                'message' => 'Adres silindi'
            ],
            REST_Controller::HTTP_OK
        );
    }

    private function _address_validate($inputs)
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($inputs['AddressType'] != 'delivery' && $inputs['AddressType'] != 'billing')
        {
            $data['inputerror'][] = 'AddressType';
            $data['error_string'][] = 'Adres Tipi seçiniz';
            $data['status'] = FALSE;
        }
        elseif ($inputs['AddressType'] == 'billing') 
        {
            if($inputs['AddressBillType'] != 'individual' && $inputs['AddressBillType'] != 'corporate')
            {
                $data['inputerror'][] = 'AddressBillType';
                $data['error_string'][] = 'Fatura Tipi seçiniz';
                $data['status'] = FALSE;
            }
            elseif($inputs['AddressBillType'] == 'corporate')
            {
                if($inputs['AddressCompanyName'] == '')
                {
                    $data['inputerror'][] = 'AddressCompanyName';
                    $data['error_string'][] = 'Firma Adı alanı boş bırakılamaz';
                    $data['status'] = FALSE;
                }

                if($inputs['AddressCompanyTaxNumber'] == '')
                {
                    $data['inputerror'][] = 'AddressCompanyTaxNumber';
                    $data['error_string'][] = 'Vergi Numarası alanı boş bırakılamaz';
                    $data['status'] = FALSE;
                }

                if($inputs['AddressCompanyTaxAdministration'] == '')
                {
                    $data['inputerror'][] = 'AddressCompanyTaxAdministration';
                    $data['error_string'][] = 'Vergi Dairesi alanı boş bırakılamaz';
                    $data['status'] = FALSE;
                }
            }
        }

        if($inputs['AddressTitle'] == '')
        {
            $data['inputerror'][] = 'AddressTitle';
            $data['error_string'][] = 'Adres Adı alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressFirstName'] == '')
        {
            $data['inputerror'][] = 'AddressFirstName';
            $data['error_string'][] = 'Ad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressLastName'] == '')
        {
            $data['inputerror'][] = 'AddressLastName';
            $data['error_string'][] = 'Soyad alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressCountryID'] == '0')
        {
            $data['inputerror'][] = 'AddressCountryID';
            $data['error_string'][] = 'Ülke seçiniz';
            $data['status'] = FALSE;
        }

        if ($inputs['AddressAlternativeCity'] == '0')
        {   
            if($inputs['AddressCityID'] == '0')
            {
                $data['inputerror'][] = 'AddressCityID';
                $data['error_string'][] = 'Şehir seçiniz';
                $data['status'] = FALSE;
            }
            
            if($inputs['AddressDistrictID'] == '0')
            {
                $data['inputerror'][] = 'AddressDistrictID';
                $data['error_string'][] = 'İlçe seçiniz';
                $data['status'] = FALSE;
            }
        }
        elseif ($inputs['AddressAlternativeCity'] == '') 
        {
            if ($inputs['AddressAlternativeCity'] == '')
            {
                $data['inputerror'][] = 'AddressAlternativeCity';
                $data['error_string'][] = 'Şehir alanı boş bırakılamaz';
                $data['status'] = FALSE;    
            }
        }

        if($inputs['AddressOpenAddress'] == '')
        {
            $data['inputerror'][] = 'AddressOpenAddress';
            $data['error_string'][] = 'Açık Adres alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressPhone'] == '')
        {
            $data['inputerror'][] = 'AddressPhone';
            $data['error_string'][] = 'Telefon alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressEmail'] == '')
        {
            $data['inputerror'][] = 'AddressEmail';
            $data['error_string'][] = 'Email alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($inputs['AddressIdentityNumber'] == '')
        {
            $data['inputerror'][] = 'AddressIdentityNumber';
            $data['error_string'][] = 'TC Kimlik No alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }
        elseif (strlen($inputs['AddressIdentityNumber']) != 11) 
        {
            $data['inputerror'][] = 'AddressIdentityNumber';
            $data['error_string'][] = 'TC Kimlik No alanı 11 karakterden oluşmaktadır';
            $data['status'] = FALSE;
        }

        if($inputs['AddressZipCode'] == '')
        {
            $data['inputerror'][] = 'AddressZipCode';
            $data['error_string'][] = 'Posta Kodu alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

    public function favorites_get($id = '', $favorite_id = '')
    {
        $this->verify_request('user');

        $this->load->model('user/favorite_model','favorite');

        if (!empty($this->get('lang_id'))) 
        {
            $this->favorite->lang_id = $this->get('lang_id');
        }

        if (!empty($id) && !empty($favorite_id)) 
        {
            $result = NULL;
        }
        elseif (!empty($id))
        {
            $result = $this->favorite->getAllByUserID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Favorilerde ürün bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function favorites_post($id = '')
    {
        $this->verify_request('user');

        $this->load->model('user/favorite_model','favorite');

        $post = $this->post(null, true);

        $data = array(
            'FavoriteUserID' => $id,
            'FavoriteProductID' => $post['FavoriteProductID'],
            'FavoriteProductContentID' => $post['FavoriteProductContentID'],
            'FavoriteCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );
        
        $insert_id = $this->favorite->save($data);

        if ($insert_id)
        {
            $this->response(
                [
                    'FavoriteID' => $insert_id,
                    'status' => true,
                    'message' => 'Ürün başarılı bir şekilde favorilerin listene eklendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün favorilere eklenirken bir sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function favorites_delete($id, $favorite_id = '')
    {
        $this->verify_request('user');

        $this->load->model('user/favorite_model','favorite');
        
        if (!empty($favorite_id)) 
        {
            $this->favorite->deleteByIDAndUserID($favorite_id, $id);
       
            $this->response(
                [
                    'status' => true,
                    'message' => 'Favorilerdeki ürün silindi'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }

    public function baskets_get($id = '')
    {
        $this->load->model('product/basket_model','basket');

        if (!empty($id))
        {
            if (!empty($this->get('product_option_content_id'))) 
            {
                $this->basket->product_option_content_id = $this->get('product_option_content_id');
                $result = $this->basket->getByUserID($id);
            }
            else
            {
                if (!empty($this->get('lang_id'))) 
                {
                    $this->basket->lang_id = $this->get('lang_id');
                    $result = $this->basket->getAllByUserIDAndJoin($id);
                }
                else
                {
                    $result = $this->basket->getAllByUserID($id);
                }
            }

            if ($result)
            {
                $this->response($result, REST_Controller::HTTP_OK);
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Sepette ürün bulunamadı'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function baskets_post($id)
    {
        $this->load->model('product/basket_model','basket');

        $post = $this->post(null, true);

        $data = array(
            'BasketProductID' => $post['BasketProductID'],
            'BasketProductContentID' => $post['BasketProductContentID'],
            'BasketProductOptionID' => $post['BasketProductOptionID'],
            'BasketProductOptionContentID' => $post['BasketProductOptionContentID'],
            'BasketProductQuantity' => $post['BasketProductQuantity'],
            'BasketUserID' => !empty($post['BasketUserID']) ? $post['BasketUserID'] : NULL,
            'BasketUnregisteredID' => !empty($post['BasketUnregisteredID']) && empty($post['BasketUserID']) ? $post['BasketUnregisteredID'] : NULL,
            'BasketCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
        );

        if (intval($post['BasketProductQuantity']) > 0) 
        {
            $basket_id = $this->basket->save($data);
        }
        else
        {
            $basket_id = false;
        }

        $basket_count = $this->basket->getCountAllByUserID($id);

        if ($basket_id)
        {
            $this->response(
                [
                    'basket_total_product' => $basket_count,
                    'status' => true,
                    'message' => 'Ürün sepete eklendi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün sepete eklenirken bir hata oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function baskets_put($id, $basket_id)
    {
        $this->load->model('product/basket_model','basket');

        $put = $this->put(null, true);

        if (isset($put['BasketProductOptionContentID'])) 
        {
            $data = array(
                'BasketProductID' => $put['BasketProductID'],
                'BasketProductContentID' => $put['BasketProductContentID'],
                'BasketProductOptionID' => $put['BasketProductOptionID'],
                'BasketProductOptionContentID' => $put['BasketProductOptionContentID'],
                'BasketProductQuantity' => $put['BasketProductQuantity'],
                'BasketUserID' => !empty($put['BasketUserID']) ? $put['BasketUserID'] : NULL,
                'BasketUnregisteredID' => !empty($put['BasketUnregisteredID']) && empty($put['BasketUserID']) ? $put['BasketUnregisteredID'] : NULL,
                'BasketUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            if (intval($put['BasketProductQuantity']) > 0) 
            {
                $this->basket->update(array('BasketID' => $basket_id), $data);

                $basket_count = $this->basket->getCountAllByUserID($id);

                $this->response(
                    [
                        'basket_total_product' => $basket_count,
                        'status' => true,
                        'message' => 'Sepetteki ürün güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Sepetteki ürün güncellenirken bir hata oluştu'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
        else
        {
            $data = array(
                'BasketUserID' => $put['BasketUserID'],
                'BasketUnregisteredID' => NULL,
                'BasketUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            if (isset($put['BasketUserID'])) 
            {
                $this->basket->update(array('BasketID' => $basket_id), $data);

                $this->response(
                    [
                        'status' => true,
                        'message' => 'Sepetteki kişi güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
    }

    public function baskets_delete($id, $basket_id = '')
    {
        $this->load->model('product/basket_model','basket');
        
        if (!empty($basket_id)) 
        {
            $this->basket->deleteByIDAndUserID($basket_id, $id);
       
            $this->response(
                [
                    'status' => true,
                    'message' => 'Sepetteki ürün silindi'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->basket->deleteByUserID($id);
       
            $this->response(
                [
                    'status' => true,
                    'message' => 'Sepetteki ürünler silindi'
                ],
                REST_Controller::HTTP_OK
            );
        }
    }
}