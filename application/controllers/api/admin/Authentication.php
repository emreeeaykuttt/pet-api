<?php
     
class Authentication extends MY_Controller
{
    
    public function __construct()
    {
       parent::__construct();

       $this->load->model('user/admin_model','admin');
    }

    public function token_post()
    {
        $post = $this->post(null, true);
        $this->load->library('form_validation');

        $this->form_validation->set_rules('UserEmail', 'E-Posta', 'trim|required|valid_email');
        $this->form_validation->set_rules('UserPassword', 'Şifre', 'trim|required|min_length[6]');

        $this->form_validation->set_message('min_length', '{field} alanı en az {param} karakterli olmalıdır.');
        $this->form_validation->set_message('required', '{field} alanı boş bırakılamaz.');
        $this->form_validation->set_message('valid_email', '{field} alanı geçerli bir e-posta adresi içermelidir.');
        
        if ($this->form_validation->run() == FALSE) 
        {
            $errors = $this->form_validation->error_array();

            $this->response(
                [
                    'errors' => $errors,
                    'status' => false,
                    'message' => 'Giriş işleminiz başarısız oldu.'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $row = $this->admin->validate($post['UserEmail'], $post['UserPassword']);

            if ($row)
            {
                $issued_at = time();
                $not_before = $issued_at + 10;
                $expire = $not_before + 60; 

                $token = AUTHORIZATION::generateToken(
                    array(
                        "iss" => 'emreeeaykut',
                        'scopes' => ['admin'],
                        'jti' => md5(uniqid(mt_rand(), true)),
                        "iat" => $issued_at,
                        "nbf" => $not_before,
                        "exp" => $expire,
                        "data" => 
                        array(
                            'UserID' => $row->UserID,
                            'UserFirstName' => $row->UserFirstName,
                            'UserLastName' => $row->UserLastName,
                            'UserEmail' => $row->UserEmail
                        )
                    )
                );

                $this->response(
                    [
                        'token' => $token,
                        'user' => $row,
                        'status' => true,
                        'message' => 'Kişi bulundu.'
                    ], 
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'E-Posta veya şifrenizi yanlış girmiş olabilirsiniz. Tekrar girininiz.'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function total_records_get()
    {
        $data['product_total'] = $this->db->where('ProductLangID', 1)->count_all_results('Products');
        $data['category_total'] = $this->db->where('CategoryLangID', 1)->where('CategoryParentID !=', 0)->count_all_results('ProductCategories');
        $data['group_total'] = $this->db->where('GroupLangID', 1)->count_all_results('OptionGroups');
        $data['tag_total'] = $this->db->where('TagLangID', 1)->count_all_results('ProductTags');
        $data['coupon_total'] = $this->db->where('CouponLangID', 1)->count_all_results('Coupons');
        $data['order_total'] = $this->db->count_all_results('Orders');
        $data['user_total'] = $this->db->where('UserVerified', 1)->count_all_results('Users');

        $this->response($data, REST_Controller::HTTP_OK);
    }
}