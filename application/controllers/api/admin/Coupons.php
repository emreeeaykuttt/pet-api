<?php

class Coupons extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');
        
        $this->load->model('product/coupon_model','coupon');
        $this->load->model('product/coupon_relation_model','coupon_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->coupon->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->coupon->getByContentID($content_id);
        }
        else
        {
            $result = $this->coupon->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kupon bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function users_get($content_id = '', $user_id = '')
    {
        $this->load->model('user/user_coupon_model','user_coupon');

        if (!empty($content_id) && !empty($user_id)) 
        {
            $result = NULL;
        }
        elseif (!empty($content_id))
        {
            $result = $this->user_coupon->getAllByCouponContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kullanıcı bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_coupon_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kupon oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->coupon_relation->save(
                array('CouponRelationModul' => 'Coupon', 'CouponRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'CouponIsActive' => !empty($post['CouponIsActive'][$i]) ? 1 : 0,
                    'CouponPublic' => !empty($post['CouponPublic'][$i]) ? 1 : 0,
                    'CouponName' => $post['CouponName'][$i],
                    'CouponCode' => $post['CouponCode'][$i],
                    'CouponDescription' => $post['CouponDescription'][$i],
                    'CouponAmount' => empty($post['CouponRate'][$i]) ? $post['CouponAmount'][$i] : NULL,
                    'CouponRate' => empty($post['CouponAmount'][$i]) ? $post['CouponRate'][$i] : NULL,
                    'CouponMinAmount' => $post['CouponMinAmount'][$i],
                    'CuoponQuantity' => !empty($post['CuoponQuantity'][$i]) || $post['CuoponQuantity'][$i] === '0' ? $post['CuoponQuantity'][$i] : null,
                    'CuoponUnlimited' => empty($post['CuoponQuantity'][$i]) && $post['CuoponQuantity'][$i] !== '0'  ? 1 : 0,
                    'CouponStartDate' => $post['CouponStartDate'][$i],
                    'CouponEndDate' => empty($post['CouponIsEndDate'][$i]) ? $post['CouponEndDate'][$i] : NULL,
                    'CouponIsEndDate' => !empty($post['CouponIsEndDate'][$i]) ? 1 : 0,
                    'CouponLangID' => $post['CouponLangID'][$i],
                    'CouponContentID' => $content_id,
                    'CouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $coupon_id = $this->coupon->save($data);
            }
         
            $this->response(
                [
                    'CouponContentID' => $content_id,
                    'status' => true,
                    'message' => 'Kupon başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function users_post($content_id)
    {
        $this->load->model('user/user_coupon_model','user_coupon');

        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_user_validate($post);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kullanıcı eklerken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $data = array(
                'UserCouponCouponID' => $post['UserCouponCouponID'],
                'UserCouponCouponContentID' => $post['UserCouponCouponContentID'],
                'UserCouponUserID' => $post['UserCouponUserID'],
                'UserCouponIsUsed' => 0,
                'UserCouponCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            $user_coupon_id = $this->user_coupon->save($data);

            $this->coupon->quantity_change($post['UserCouponCouponContentID'], 'minus');
         
            $this->response(
                [
                    'CouponID' => $post['UserCouponCouponID'],
                    'CouponContentID' => $post['UserCouponCouponContentID'],
                    'status' => true,
                    'message' => 'Kullanıcı başarılı bir şekilde eklendi'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['CouponContentID'];

        $coupon = $this->coupon_relation->getByID($content_id);

        if ($coupon) 
        {
            $validate = $this->_coupon_validate($put, $language_count);

            if ($validate) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Kupon güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->coupon_relation->update(
                    array('CouponRelationID' => $content_id), 
                    array('CouponRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'CouponIsActive' => !empty($put['CouponIsActive'][$i]) ? 1 : 0,
                        'CouponPublic' => !empty($put['CouponPublic'][$i]) ? 1 : 0,
                        'CouponName' => $put['CouponName'][$i],
                        'CouponCode' => $put['CouponCode'][$i],
                        'CouponDescription' => $put['CouponDescription'][$i],
                        'CouponAmount' => empty($put['CouponRate'][$i]) ? $put['CouponAmount'][$i] : NULL,
                        'CouponRate' => empty($put['CouponAmount'][$i]) ? $put['CouponRate'][$i] : NULL,
                        'CouponMinAmount' => $put['CouponMinAmount'][$i],
                        'CuoponQuantity' => !empty($put['CuoponQuantity'][$i]) || $put['CuoponQuantity'][$i] === '0' ? $put['CuoponQuantity'][$i] : null,
                        'CuoponUnlimited' => empty($put['CuoponQuantity'][$i]) && $put['CuoponQuantity'][$i] !== '0'  ? 1 : 0,
                        'CouponStartDate' => $put['CouponStartDate'][$i],
                        'CouponEndDate' => empty($put['CouponIsEndDate'][$i]) ? $put['CouponEndDate'][$i] : NULL,
                        'CouponIsEndDate' => !empty($put['CouponIsEndDate'][$i]) ? 1 : 0,
                        'CouponLangID' => $put['CouponLangID'][$i],
                        'CouponContentID' => $content_id,
                        'CouponUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $this->coupon->update(
                        array('CouponID' => $put['CouponID'][$i]),
                        $data
                    );
                }
                
                $this->response(
                    [
                        'CouponContentID' => $content_id,
                        'status' => true,
                        'message' => 'Kupon başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir kupon yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $this->load->model('user/user_coupon_model','user_coupon');

        $coupon = $this->coupon_relation->getByID($content_id);

        if ($coupon) 
        {
            $this->coupon->deleteByContentID($content_id);
            $this->coupon_relation->deleteByID($content_id);

            $this->user_coupon->deleteByCouponContentID($content_id);

            $this->response(
                [
                    'status' => true,
                    'message' => 'Kupon silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir kupon yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function users_delete($content_id, $user_coupon_id)
    {
        $this->load->model('user/user_coupon_model','user_coupon');

        $this->user_coupon->deleteByIDAndCouponContentID($user_coupon_id, $content_id);
        $this->coupon->quantity_change($content_id, 'plus');

        $this->response(
            [
                'status' => true,
                'message' => 'Kullanıcı silindi.'
            ],
            REST_Controller::HTTP_OK
        );
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->coupon->get_data = $get_data;
        $this->coupon->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->coupon->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->coupon->getCountAll();
    }

    private function count_filtered()
    {
        return $this->coupon->getCountFiltered();
    }

    private function _coupon_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['CouponName'][$i] == '')
            {
                $data['inputerror'][][$i] = 'CouponName';
                $data['error_string'][][$i] = 'Kupon Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['CouponCode'][$i] == '')
            {
                $data['inputerror'][][$i] = 'CouponCode';
                $data['error_string'][][$i] = 'Kupon Kodu alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['CouponRate'][$i] == '')
            {
                if($inputs['CouponAmount'][$i] == '')
                {
                    $data['inputerror'][][$i] = 'CouponAmount';
                    $data['error_string'][][$i] = 'İndirim Miktarı alanı boş bırakılamaz';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
                elseif (!is_numeric($inputs['CouponAmount'][$i])) 
                {
                    $data['inputerror'][][$i] = 'CouponAmount';
                    $data['error_string'][][$i] = 'İndirim Miktarı alanına sadece sayı girilir';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
            }
            elseif (!is_numeric($inputs['CouponRate'][$i])) 
            {
                $data['inputerror'][][$i] = 'CouponRate';
                $data['error_string'][][$i] = 'İndirim Oranı alanına sadece sayı girilir';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
            elseif (!empty($inputs['CouponAmount'][$i])) 
            {
                $data['inputerror'][][$i] = 'CouponRate';
                $data['error_string'][][$i] = 'İndirim Miktarı ile indirim alanı aynı anda veri girilmez';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            
            if (!is_numeric($inputs['CouponMinAmount'][$i]) && $inputs['CouponMinAmount'][$i] != '') 
            {
                $data['inputerror'][][$i] = 'CouponMinAmount';
                $data['error_string'][][$i] = 'Minumum Satış Miktarı alanına sadece sayı girilir';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if (!is_numeric($inputs['CuoponQuantity'][$i]) && $inputs['CuoponQuantity'][$i] != '') 
            {
                $data['inputerror'][][$i] = 'CuoponQuantity';
                $data['error_string'][][$i] = 'Adet alanına sadece sayı girilir';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['CouponStartDate'][$i] == '')
            {
                $data['inputerror'][][$i] = 'CouponStartDate';
                $data['error_string'][][$i] = 'Başlangıç Tarihi alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if (empty($inputs['CouponIsEndDate'][$i]))
            {
                if($inputs['CouponEndDate'][$i] == '')
                {
                    $data['inputerror'][][$i] = 'CouponEndDate';
                    $data['error_string'][][$i] = $inputs['CouponIsEndDate'][1] . ' Bitiş Tarihi alanı boş bırakılamaz';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

    private function _user_validate($inputs)
    {
        $this->load->model('user/user_coupon_model','user_coupon');

        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        if($inputs['UserCouponUserID'] == '' || $inputs['UserCouponUserID'] == '0')
        {
            $data['inputerror'][] = 'UserCouponUserID';
            $data['error_string'][] = 'Kullanıcı alanı boş bırakılamaz';
            $data['status'] = FALSE;
        }
        else
        {

            $is_user = $this->user_coupon->isThereUser($inputs['UserCouponCouponContentID'], $inputs['UserCouponUserID']);

            if ($is_user) 
            {
                $data['inputerror'][] = 'UserCouponUserID';
                $data['error_string'][] = 'Kullanıcı zaten eklenmiş';
                $data['status'] = FALSE;
            }
        } 

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}