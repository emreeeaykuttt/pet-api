<?php

class Languages extends MY_Controller 
{

	public function __construct()
	{
		parent::__construct();
        $this->verify_request('admin');
        
		$this->load->model('language/language_model', 'language');
	}

    public function index_get()
    {   
        if (!empty($this->get('code'))) 
        {
            $result = $this->row_by_code($this->get('code'));
        }
        elseif (!empty($this->get('default'))) 
        {
            $result = $this->row_by_default();
        }
        elseif (!empty($this->get('active'))) 
        {
            $result = $this->list_by_active();
        }
        elseif (!empty($this->get('count'))) 
        {
            $result = $this->count_by_status();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Dil bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

	private function row_by_code($code)
    {   
        return $this->language->getByCode($code);
    }

	private function row_by_default()
    {
        return $this->language->getByDefault();
    }

    private function list_by_active()
    {
        return $this->language->getAllByActive();
    }

    private function count_by_status()
    {
        return $this->language->getCountByStatus();
    }

}
