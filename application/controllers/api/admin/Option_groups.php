<?php

class Option_groups extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');
        
        $this->load->model('product/option_group_model','option_group');
        $this->load->model('product/option_model','option');
        $this->load->model('product/option_relation_model','option_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->option_group->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->option_group->getByContentID($content_id);
        }
        else
        {
            $result = $this->option_group->getAll();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon grubu bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_option_group_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon grubu oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->option_relation->save(
                array('OptionRelationModul' => 'OptionGroup', 'OptionRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'GroupName' => $post['GroupName'][$i],
                    'GroupSlug' => seo_url($post['GroupName'][$i]),
                    'GroupSort' => $post['GroupSort'][$i],
                    'GroupLangID' => $post['GroupLangID'][$i],
                    'GroupContentID' => $content_id,
                    'GroupCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $option_group_id = $this->option_group->save($data);
            }
         
            $this->response(
                [
                    'GroupContentID' => $content_id,
                    'status' => true,
                    'message' => 'Opsiyon grubu başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['GroupContentID'];

        $option_group = $this->option_relation->getByID($content_id);

        if ($option_group) 
        {
            $validate = $this->_option_group_validate($put, $language_count);

            if ($validate) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Opsiyon grubu güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->option_relation->update(
                    array('OptionRelationID' => $content_id), 
                    array('OptionRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'GroupName' => $put['GroupName'][$i],
                        'GroupSlug' => seo_url($put['GroupName'][$i]),
                        'GroupSort' => $put['GroupSort'][$i],
                        'GroupLangID' => $put['GroupLangID'][$i],
                        'GroupContentID' => $content_id,
                        'GroupUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $this->option_group->update(
                        array('GroupID' => $put['GroupID'][$i]),
                        $data
                    );
                }
                
                $this->response(
                    [
                        'GroupContentID' => $content_id,
                        'status' => true,
                        'message' => 'Opsiyon grubu başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir opsiyon grubu yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $option_group = $this->option_relation->getByID($content_id);

        if ($option_group) 
        {
            $this->load->model('product/product_option_model','product_option');

            $option_group_val = '":"'.$content_id.'"';
            $is_option_group = $this->product_option->isThereOptionGroupRelation($option_group_val);

            if (!$is_option_group) 
            {
                $options = $this->option->getAllByGroupContentID($content_id);
                if ($options)
                {
                    foreach ($options as $key => $value)
                    {
                        $this->option_relation->deleteByID($value['OptionContentID']);
                    }
                }
                $this->option_relation->deleteByID($content_id);

                $this->option_group->deleteByContentID($content_id);
           
                $this->response(
                    [
                        'status' => true,
                        'message' => 'Opsiyon grubu silindi.'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Ürün ile ilişkili olduğu için silinemiyor'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );  
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir opsiyon grubu yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function options_get($content_id = '', $option_content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id) && !empty($option_content_id)) 
        {
            $result = NULL;
        }
        elseif (!empty($content_id))
        {
            $result = $this->option->getAllByGroupContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->option_group->get_data = $get_data;
        $this->option_group->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->option_group->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->option_group->getCountAll();
    }

    private function count_filtered()
    {
        return $this->option_group->getCountFiltered();
    }

    private function _option_group_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['GroupName'][$i] == '')
            {
                $data['inputerror'][][$i] = 'GroupName';
                $data['error_string'][][$i] = 'Opsion Grup Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}