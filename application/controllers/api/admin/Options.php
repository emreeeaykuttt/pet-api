<?php

class Options extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');
        
        $this->load->model('product/option_model','option');
        $this->load->model('product/option_relation_model','option_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->option->getByContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if (!empty($result))
        {
            $this->response($result, REST_Controller::HTTP_OK);   
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_option_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Opsiyon oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->option_relation->save(
                array('OptionRelationModul' => 'Option', 'OptionRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'OptionName' => $post['OptionName'][$i],
                    'OptionColor' => $post['OptionColor'][$i],
                    'OptionSort' => !empty($post['OptionSort'][$i]) ? $post['OptionSort'][$i] : NULL,
                    'OptionGroupID' => $post['OptionGroupID'],
                    'OptionGroupContentID' => $post['OptionGroupContentID'],
                    'OptionLangID' => $post['OptionLangID'][$i],
                    'OptionContentID' => $content_id,
                    'OptionCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $option_id = $this->option->save($data);
            }
         
            $this->response(
                [
                    'OptionContentID' => $content_id,
                    'OptionGroupID' => $post['OptionGroupID'],
                    'OptionGroupContentID' => $post['OptionGroupContentID'],
                    'status' => true,
                    'message' => 'Opsiyon başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['OptionContentID'];

        $option = $this->option_relation->getByID($content_id);

        if ($option) 
        {
            $validate = $this->_option_validate($put, $language_count);

            if ($validate) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Opsiyon güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->option_relation->update(
                    array('OptionRelationID' => $content_id), 
                    array('OptionRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'OptionName' => $put['OptionName'][$i],
                        'OptionColor' => $put['OptionColor'][$i],
                        'OptionSort' => !empty($put['OptionSort'][$i]) ? $put['OptionSort'][$i] : NULL,
                        'OptionGroupID' => $put['OptionGroupID'],
                        'OptionGroupContentID' => $put['OptionGroupContentID'],
                        'OptionLangID' => $put['OptionLangID'][$i],
                        'OptionContentID' => $content_id,
                        'OptionUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $this->option->update(
                        array('OptionID' => $put['OptionID'][$i]),
                        $data
                    );
                }
                
                $this->response(
                    [
                        'OptionContentID' => $content_id,
                        'OptionGroupID' => $put['OptionGroupID'],
                        'OptionGroupContentID' => $put['OptionGroupContentID'],
                        'status' => true,
                        'message' => 'Opsiyon başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir opsiyon yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $option = $this->option_relation->getByID($content_id);

        if ($option) 
        {
            $this->load->model('product/product_option_model','product_option');

            $option_val = '":"'.$content_id.'"';

            $is_option = $this->product_option->isThereOptionRelation($option_val);

            if (!$is_option) 
            {
                $this->option->deleteByContentID($content_id);
                $this->option_relation->deleteByID($content_id);
               
                $this->response(
                    [
                        'status' => true,
                        'message' => 'Opsiyon silindi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Ürün ile ilişkili olduğu için silinemiyor'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );   
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir opsiyon yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function _option_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['OptionName'][$i] == '')
            {
                $data['inputerror'][][$i] = 'OptionName';
                $data['error_string'][][$i] = 'Opsion Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}