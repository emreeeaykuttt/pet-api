<?php

class Orders extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');
        
        $this->load->model('order/order_model','order');
        $this->load->model('order/detail_model','detail');
    }

    public function index_get($id = '')
    {
        if (!empty($id))
        {
            $row = $this->order->getByID($id);

            if ($row)
            {
                $this->response($row, REST_Controller::HTTP_OK);   
            }
            else
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Sipariş bulunamadı'
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
        }
    }

    public function index_put($id)
    {
        $status = true;
        $message = '';
        $rest_code = REST_Controller::HTTP_OK;
        $put = $this->put(null, true);

        if ($put['update_type'] == 'status') 
        {
            $data = array(
                'OrderStatus' => $put['OrderStatus'],
                'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            $updated = $this->order->update(array('OrderID' => $id), $data);

            if ($updated) 
            {
                $status = true;
                $message = 'Siparişin ödeme durumu güncellendi';
                $rest_code = REST_Controller::HTTP_OK;
            }
            else
            {
                $status = false;
                $message = 'Siparişin ödeme durumu güncellenirken bir hata oluştu';
                $rest_code = REST_Controller::HTTP_BAD_REQUEST;
            }
        }
        elseif ($put['update_type'] == 'shipping_status') 
        {
            $data = array(
                'OrderShippingStatus' => $put['OrderShippingStatus'],
                'OrderCargoCompany' => $put['OrderCargoCompany'],
                'OrderCargoNumber' => $put['OrderCargoNumber'],
                'OrderUptatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
            );

            $updated = $this->order->update(array('OrderID' => $id), $data);

            if ($updated) 
            {
                if ($put['OrderShippingStatus'] != 'pending') 
                {
                    $order = $this->order->getByID($id);
                    $details = $this->detail->getAllByOrderIDAndUserID($id, $order->OrderUserID);

                    $tos = array($order->OrderBillingEmail);
                    $ccs = array();
                    $bccs = array();
                    $link = WEB_URL . 'profile/orders?tracking_number=' . $order->OrderTrackingNumber;
                    $link_text = 'Sipariş Numarası: ' . $order->OrderTrackingNumber;

                    $coupon_discount = '';
                    if ($order->OrderCouponContentID)
                    {
                        if ($order->OrderCouponAmount)
                        {
                            $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponAmount . $order->OrderCurrencyCode . '<br />';
                        }
                        else
                        {
                            $coupon_discount = 'Kupon İndirimi: ' . $order->OrderCouponRate . '%' . '<br />';
                        }
                    }

                    $detail_table = '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">';
                    foreach ($details as $key => $detail) 
                    {
                        $product_option_json = json_decode($detail['DetailProductOptionContent'], true);
                        $product_option_text = '';

                        if ($product_option_json) {
                            foreach ($product_option_json as $key => $option) {
                                $product_option_text .= $option['group'] . ': ' . $option['option'] . '<br />';
                            }
                        }

                        $detail_photo = '';
                        if ($detail['DetailProductPhoto'])
                        {
                            $detail_photo = base_url() . $detail['DetailProductPhoto'];
                        }
                        else
                        {
                            $detail_photo = base_url() . 'upload/product/null-photo.png';
                        }

                        $discount_area = '';
                        if (floatval($detail['DetailDiscountPrice']) > 0)
                        {
                            $discount_area = 'İndirim: ' . $detail['DetailDiscountPrice'] . $detail['DetailCurrencyCode'] . '<br />
                                            İndirimli Fiyatı: ' . $detail['DetailDiscountInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />';
                        }

                        $detail_table .= 
                        '<tr>
                            <td width="30">
                            <td width="100" valign="top">
                                <img src="'. $detail_photo .'" width="100" />
                            </td>
                            <td width="20">
                            <td width="450">
                                <b>' . $detail['DetailProductName'] . ' (' . $detail['DetailSKU'] . ')</b><br />' .
                                $product_option_text .
                                'Birim Fiyatı: ' . $detail['DetailVatInclusivePrice'] . $detail['DetailCurrencyCode'] . '<br />' .
                                $discount_area .
                                'Adet: ' . $detail['DetailQuantity'] . '<br />
                                Toplam Fiyat: ' . $detail['DetailLatestPrice'] . $detail['DetailCurrencyCode'] . '<br />
                            </td>
                        </tr>
                        <tr>
                            <td width="600" colspan="4" style="padding: 0 30px 0 30px;"><hr /></td>
                        </tr>
                        ';
                    }
                    $detail_table .= '</table>';

                    $order_table = 
                        '<table align="left" border="0" cellpadding="0" cellspacing="0" width="600">
                            <tr>
                                <td style="padding: 0 30px 0 30px;">
                                    <b>Ödeme Bilgileri</b><br />
                                    Sipariş No: '. $order->OrderTrackingNumber .'<br />
                                    Sipariş Tutarı: ' . $order->OrderVatInclusiveAmount . $order->OrderCurrencyCode . '<br />
                                    İndirim Tutarı: ' . $order->OrderDiscountAmount . $order->OrderCurrencyCode . '<br />'.
                                    $coupon_discount .
                                    'Kargo Ücreti: ' . $order->OrderCargoAmount . $order->OrderCurrencyCode . '<br />
                                    Toplam Tutar: ' . $order->OrderLatestAmount . $order->OrderCurrencyCode .'<br /><br />
                                    <hr />
                                </td>
                            </tr>
                        </table>'
                    ;

                    if ($put['OrderShippingStatus'] == 'preparing') 
                    {
                        $mail_content = 
                            '<b>Merhaba '. $order->OrderBillingFirstName . ' ' . $order->OrderBillingLastName .',</b><br />
                            Siparişiniz hazırlanıyor. En kısa zamanda kargoya verilecektir.
                            <br /><br /><hr /><br />' .
                            $order_table .
                            $detail_table
                        ;
                        $mail_title = 'Siparişiniz Hazırlanıyor';
                    }
                    elseif ($put['OrderShippingStatus'] == 'shipped') 
                    {
                        $mail_content = 
                            '<b>Merhaba '. $order->OrderBillingFirstName . ' ' . $order->OrderBillingLastName .',</b><br />
                            Siparişiniz kargoya verildi.
                            <br />
                            <b>Kargo Firması: '. $order->OrderCargoCompany .'</b><br />
                            <b>Kargo Takip Numarası: '. $order->OrderCargoNumber .'</b><br /><br />
                            <b>Teslimat Adresi:</b><br />
                            ' . $order->OrderDeliveryOpenAddress .'<br />
                            ' . $order->OrderDeliveryDistrict . ' / ' . $order->OrderDeliveryCity .'<br />
                            <br /><br /><hr /><br />' .
                            $order_table .
                            $detail_table
                        ;
                        $mail_title = 'Siparişiniz Kargoya Verildi';
                    }
                    elseif ($put['OrderShippingStatus'] == 'delivered') 
                    {
                        $mail_content = 
                            '<b>Merhaba '. $order->OrderBillingFirstName . ' ' . $order->OrderBillingLastName .',</b><br />
                            Siparişiniz teslim edildi.
                            <br /><br /><hr /><br />' .
                            $order_table .
                            $detail_table
                        ;
                        $mail_title = 'Siparişiniz Teslim Edildi';
                    }

                    $html_message = $this->mail_template($mail_content, $link, $link_text, '7.png');
                    $this->send_email($tos, $ccs, $bccs, 'petshopevinde.com', $mail_title, $html_message);
                }

                $status = true;
                $message = 'Siparişin gönderim durumu güncellendi';
                $rest_code = REST_Controller::HTTP_OK;
            }
            else
            {
                $status = false;
                $message = 'Siparişin gönderim durumu güncellenirken bir hata oluştu';
                $rest_code = REST_Controller::HTTP_BAD_REQUEST;
            }
        }

        $this->response(
            [
                'status' => $status,
                'message' => $message
            ],
            $rest_code
        );        
    }

    public function details_get($id = '', $detail_id = '')
    {
        $this->load->model('order/detail_model','detail');

        if (!empty($id) && !empty($detail_id)) 
        {
            $result = $this->detail->getByIDAndOrderID($detail_id, $id);
        }
        elseif (!empty($id))
        {
            $result = $this->detail->getAllByOrderID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Sipariş Detayı Bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->order->get_data = $get_data;

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->order->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->order->getCountAll();
    }
    
    private function count_filtered()
    {
        return $this->order->getCountFiltered();
    }

}