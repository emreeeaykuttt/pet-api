<?php

class Product_categories extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');

        $this->load->model('product/category_model','category');
        $this->load->model('product/category_relation_model','category_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->category->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->category->getByContentID($content_id);
        }
        elseif (!empty($this->get('parent_id'))) 
        {
            $result = $this->category->getAllByParentID($this->get('parent_id'));
        }
        else
        {
            $parentArr = array();
            $categories = $this->category->getAll();

            foreach($categories as $key => $val){
                $parentArr[$val['CategoryParentID']][$val['CategoryContentID']] = array(
                    'CategoryName' => $val['CategoryName'],
                    'CategoryParentID' => $val['CategoryParentID'],
                    'CategorySlug' => $val['CategorySlug'],
                    'CategoryContentID' => $val['CategoryContentID'],
                    'CategoryLangID' => $val['CategoryLangID'],
                );
            }

            $result = $parentArr;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kategori bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_category_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kategori oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->category_relation->save(
                array('CategoryRelationModul' => 'ProductCategory', 'CategoryRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'CategoryName' => $post['CategoryName'][$i],
                    'CategorySlug' => seo_url($post['CategoryName'][$i]) . '-' . $content_id,
                    'CategoryParentID' => $post['CategoryParentID'][$i],
                    'CategoryLangID' => $post['CategoryLangID'][$i],
                    'CategoryContentID' => $content_id,
                    'CategoryCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $category_id = $this->category->save($data);
            }
         
            $this->response(
                [
                    'CategoryContentID' => $content_id,
                    'status' => true,
                    'message' => 'Kategori başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['CategoryContentID'];

        $category = $this->category_relation->getByID($content_id);

        if ($category) 
        {
            $validate = $this->_category_validate($put, $language_count);

            if ($validate) 
            {
                
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Kategori güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->category_relation->update(
                    array('CategoryRelationID' => $content_id), 
                    array('CategoryRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'CategoryName' => $put['CategoryName'][$i],
                        'CategorySlug' => seo_url($put['CategoryName'][$i]) . '-' . $content_id,
                        'CategoryParentID' => $put['CategoryParentID'][$i],
                        'CategoryLangID' => $put['CategoryLangID'][$i],
                        'CategoryContentID' => $content_id,
                        'CategoryUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $this->category->update(
                        array('CategoryID' => $put['CategoryID'][$i]),
                        $data
                    );
                }

                $this->response(
                    [
                        'CategoryContentID' => $content_id,
                        'status' => true,
                        'message' => 'Kategori başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }

        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir kategori yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $category = $this->category_relation->getByID($content_id);

        if ($category) 
        {
            $this->category->deleteByContentID($content_id);
            $this->category_relation->deleteByID($content_id);
           
            $this->response(
                [
                    'status' => true,
                    'message' => 'Kategori silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir kategori yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->category->get_data = $get_data;
        $this->category->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->category->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->category->getCountAll();
    }
    
    private function count_filtered()
    {
        return $this->category->getCountFiltered();
    }

    private function _category_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['CategoryName'][$i] == '')
            {
                $data['inputerror'][][$i] = 'CategoryName';
                $data['error_string'][][$i] = 'Kategori Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if(!isset($inputs['CategoryParentID'][$i]))
            {
                $data['inputerror'][][$i] = 'CategoryParentID';
                $data['error_string'][][$i] = 'Bağlı olduğu kategoriyi seçiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
            else
            {
                if (isset($inputs['CategoryContentID'])) 
                {
                    if($inputs['CategoryParentID'][$i] == $inputs['CategoryContentID'])
                    {
                        $data['inputerror'][][$i] = 'CategoryParentID';
                        $data['error_string'][][$i] = 'Bağlı olduğu kategoriyi kendisini seçemezsiniz';
                        $data['lang'][] = $i;
                        $data['status'] = FALSE;
                    }
                }
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}