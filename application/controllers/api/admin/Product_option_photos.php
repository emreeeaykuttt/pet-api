<?php

class Product_option_photos extends MY_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');

        $this->load->model('product/product_option_photo_model', 'product_option_photo');
    }

    public function index_post()
    {
        $post_data = $this->post(null, true);
        $file = json_decode($post_data['files'], true);
        $file_total = $post_data['total'];

        $filename = '';

        for ($i = 0; $i < $file_total; $i++) 
        {
            if (!empty($file[$i]['image'])) 
            {
                $image = $file[$i]['image'];
                list($type, $image) = explode(';', $image);
                list(, $image) = explode(',', $image);
                $image = base64_decode($image);
                $encryption = crc32(date('Y-m-d H:i:s') . uniqid(mt_rand(), true));
                $filename = 'upload/product/gallery/' . $encryption . '-' . image_url($file[$i]['name']);
                $image_upload = file_put_contents('./' . $filename, $image);

                if (isset($image_upload)) 
                {
                    $data[] = $filename;
                    $this->image_resize($filename, $filename);
                }
            }
        }

        if (count($data) > 0) 
        {
        	$this->response($data, REST_Controller::HTTP_OK);
        }
        else
        {
        	$this->response(
                [
                    'status' => false,
                    'message' => 'Galeri oluştururken bir sorun oluştu'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

}