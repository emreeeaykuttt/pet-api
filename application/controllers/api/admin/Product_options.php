<?php

class Product_options extends MY_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');

        $this->load->model('product/product_option_model','product_option');
        $this->load->model('product/product_option_relation_model','product_option_relation');
        $this->load->model('product/product_option_photo_model', 'product_option_photo');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->product_option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->product_option->getByContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if (!empty($result))
        {
            $this->response($result, REST_Controller::HTTP_OK);   
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün opsiyonu bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $this->load->model('product/option_group_model','option_group');
        $this->load->model('product/option_model','option');

        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_product_option_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün opsiyonu oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->product_option_relation->save(
                array('ProductOptionRelationModul' => 'ProductOption', 'ProductOptionRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'ProductOptionSKU' => $post['ProductOptionSKU'][$i],
                    'ProductOptionStockStatus' => !empty($post['ProductOptionQuantity'][$i]) && $post['ProductOptionQuantity'][$i] !== '0'  ? 1 : 0,
                    'ProductOptionQuantity' => !empty($post['ProductOptionQuantity'][$i]) || $post['ProductOptionQuantity'][$i] === '0' ? $post['ProductOptionQuantity'][$i] : null,
                    'ProductOptionUnlimited' => empty($post['ProductOptionQuantity'][$i]) && $post['ProductOptionQuantity'][$i] !== '0'  ? 1 : 0,
                    'ProductOptionPrice' => $post['ProductOptionPrice'][$i],
                    'ProductOptionVatRate' => $post['ProductOptionVatRate'][$i],
                    'ProductOptionVatPrice' => $post['ProductOptionVatPrice'][$i],
                    'ProductOptionVatInclusivePrice' => $post['ProductOptionVatInclusivePrice'][$i],
                    'ProductOptionDiscountRate' => $post['ProductOptionDiscountRate'][$i],
                    'ProductOptionDiscountPrice' => $post['ProductOptionDiscountPrice'][$i],
                    'ProductOptionLatestPrice' => $post['ProductOptionLatestPrice'][$i],
                    'ProductOptionSecondPrice' => $post['ProductOptionSecondPrice'][$i],
                    'ProductOptionSecondVatRate' => $post['ProductOptionSecondVatRate'][$i],
                    'ProductOptionSecondVatPrice' => $post['ProductOptionSecondVatPrice'][$i],
                    'ProductOptionSecondVatInclusivePrice' => $post['ProductOptionSecondVatInclusivePrice'][$i],
                    'ProductOptionSecondDiscountRate' => $post['ProductOptionSecondDiscountRate'][$i],
                    'ProductOptionSecondDiscountPrice' => $post['ProductOptionSecondDiscountPrice'][$i],
                    'ProductOptionSecondLatestPrice' => $post['ProductOptionSecondLatestPrice'][$i],
                    'ProductOptionGroupContentIDs' => json_encode($post['ProductOptionGroupContentIDs'][$i]),
                    'ProductOptionOptionContentIDs' => json_encode($post['ProductOptionOptionContentIDs'][$i]),
                    'ProductOptionProductID' => $post['ProductOptionProductID'],
                    'ProductOptionProductContentID' => $post['ProductOptionProductContentID'],
                    'ProductOptionLangID' => $post['ProductOptionLangID'][$i],
                    'ProductOptionContentID' => $content_id,
                    'ProductOptionCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $price = $data['ProductOptionLatestPrice'] / (1 + ($data['ProductOptionVatRate'] / 100));
                $vat_price = $data['ProductOptionLatestPrice'] - $price;
                $data['ProductOptionDiscountInclusiveVatPrice'] = $vat_price;
                $data['ProductOptionDiscountInclusivePriceAndWithoutVat'] = $price;

                $data['ProductOptionFirstOption'] = !isset($post['ProductOptionFirstOption']) ? 0 : 1;
                $data['ProductOptionCommonGallery'] = !isset($post['ProductOptionCommonGallery']) ? 0 : 1;

                $details = [];
                foreach ($post['ProductOptionGroupContentIDs'][1] as $key => $group_id) 
                {
                    $this->option_group->lang_id = $post['ProductOptionLangID'][1];
                    $this->option->lang_id = $post['ProductOptionLangID'][1];
                    $group = $this->option_group->getByContentID($group_id);
                    $option = $this->option->getByContentID($post['ProductOptionOptionContentIDs'][1][$key]);

                    $details[$group->GroupSlug]['group'] = $group->GroupName;
                    $details[$group->GroupSlug]['option'] = $option->OptionName;
                    $details[$group->GroupSlug]['option_id'] = $option->OptionContentID;
                }

                $data['ProductOptionDetails'] = json_encode($details);

                $product_option_id = $this->product_option->save($data);

                if ($i == 1 && isset($post['PhotoSrc'][$i])) 
                {
                    foreach ($post['PhotoSrc'][$i] as $key => $value) {
                        $data_img = array(
                            'PhotoName' => $value,
                            'PhotoTitle' => $post['PhotoTitle'][$i][$key],
                            'PhotoSort' => $post['PhotoSort'][$i][$key],
                            'PhotoProductOptionID' => $product_option_id,
                            'PhotoProductOptionContentID' => $content_id,
                            'PhotoLangID' => $post['ProductOptionLangID'][$i],
                            'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                        );
                        $this->product_option_photo->save($data_img);
                    }
                }
                elseif ($i > 1 && isset($post['PhotoSrc'][$i]) && !isset($post['ProductOptionCommonGallery'])) 
                {
                    foreach ($post['PhotoSrc'][$i] as $key => $value) {
                        $data_img = array(
                            'PhotoName' => $value,
                            'PhotoTitle' => $post['PhotoTitle'][$i][$key],
                            'PhotoSort' => $post['PhotoSort'][$i][$key],
                            'PhotoProductOptionID' => $product_option_id,
                            'PhotoProductOptionContentID' => $content_id,
                            'PhotoLangID' => $post['ProductOptionLangID'][$i],
                            'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                        );
                        $this->product_option_photo->save($data_img);
                    }
                }
                elseif ($i > 1 && isset($post['PhotoSrc'][1]) && isset($post['ProductOptionCommonGallery'])) 
                {
                    foreach ($post['PhotoSrc'][1] as $key => $value) {
                        $data_img = array(
                            'PhotoName' => $value,
                            'PhotoTitle' => $post['PhotoTitle'][1][$key],
                            'PhotoSort' => $post['PhotoSort'][1][$key],
                            'PhotoProductOptionID' => $product_option_id,
                            'PhotoProductOptionContentID' => $content_id,
                            'PhotoLangID' => $post['ProductOptionLangID'][$i],
                            'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                        );
                        $this->product_option_photo->save($data_img);
                    }
                }
            }

            if (isset($post['ProductOptionFirstOption'])) 
            {
                $this->product_option->firstOptionUpdate($content_id, $post['ProductOptionProductContentID']);
            }
         
            $this->response(
                [
                    'ProductOptionContentID' => $content_id,
                    'ProductOptionProductID' => $post['ProductOptionProductID'],
                    'ProductOptionProductContentID' => $post['ProductOptionProductContentID'],
                    'status' => true,
                    'message' => 'Ürün opsiyonu başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $this->load->model('product/option_group_model','option_group');
        $this->load->model('product/option_model','option');

        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['ProductOptionContentID'];

        $product_option = $this->product_option_relation->getByID($content_id);

        if ($product_option) 
        {
            $validate = $this->_product_option_validate($put, $language_count);

            if ($validate) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Ürün opsiyonu güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->product_option_relation->update(
                    array('ProductOptionRelationID' => $content_id), 
                    array('ProductOptionRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'ProductOptionSKU' => $put['ProductOptionSKU'][$i],
                        'ProductOptionStockStatus' => !empty($put['ProductOptionQuantity'][$i]) && $put['ProductOptionQuantity'][$i] !== '0'  ? 1 : 0,
                        'ProductOptionQuantity' => !empty($put['ProductOptionQuantity'][$i]) || $put['ProductOptionQuantity'][$i] === '0' ? $put['ProductOptionQuantity'][$i] : null,
                        'ProductOptionUnlimited' => empty($put['ProductOptionQuantity'][$i]) && $put['ProductOptionQuantity'][$i] !== '0'  ? 1 : 0,
                        'ProductOptionPrice' => $put['ProductOptionPrice'][$i],
                        'ProductOptionVatRate' => $put['ProductOptionVatRate'][$i],
                        'ProductOptionVatPrice' => $put['ProductOptionVatPrice'][$i],
                        'ProductOptionVatInclusivePrice' => $put['ProductOptionVatInclusivePrice'][$i],
                        'ProductOptionDiscountRate' => $put['ProductOptionDiscountRate'][$i],
                        'ProductOptionDiscountPrice' => $put['ProductOptionDiscountPrice'][$i],
                        'ProductOptionLatestPrice' => $put['ProductOptionLatestPrice'][$i],
                        'ProductOptionSecondPrice' => $put['ProductOptionSecondPrice'][$i],
                        'ProductOptionSecondVatRate' => $put['ProductOptionSecondVatRate'][$i],
                        'ProductOptionSecondVatPrice' => $put['ProductOptionSecondVatPrice'][$i],
                        'ProductOptionSecondVatInclusivePrice' => $put['ProductOptionSecondVatInclusivePrice'][$i],
                        'ProductOptionSecondDiscountRate' => $put['ProductOptionSecondDiscountRate'][$i],
                        'ProductOptionSecondDiscountPrice' => $put['ProductOptionSecondDiscountPrice'][$i],
                        'ProductOptionSecondLatestPrice' => $put['ProductOptionSecondLatestPrice'][$i],
                        'ProductOptionGroupContentIDs' => json_encode($put['ProductOptionGroupContentIDs'][$i]),
                        'ProductOptionOptionContentIDs' => json_encode($put['ProductOptionOptionContentIDs'][$i]),
                        'ProductOptionProductID' => $put['ProductOptionProductID'],
                        'ProductOptionProductContentID' => $put['ProductOptionProductContentID'],
                        'ProductOptionLangID' => $put['ProductOptionLangID'][$i],
                        'ProductOptionContentID' => $content_id,
                        'ProductOptionUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $price = $data['ProductOptionLatestPrice'] / (1 + ($data['ProductOptionVatRate'] / 100));
                    $vat_price = $data['ProductOptionLatestPrice'] - $price;
                    $data['ProductOptionDiscountInclusiveVatPrice'] = $vat_price;
                    $data['ProductOptionDiscountInclusivePriceAndWithoutVat'] = $price;

                    $data['ProductOptionFirstOption'] = !isset($put['ProductOptionFirstOption']) ? 0 : 1;
                    $data['ProductOptionCommonGallery'] = !isset($put['ProductOptionCommonGallery']) ? 0 : 1;

                    $details = [];
                    foreach ($put['ProductOptionGroupContentIDs'][1] as $key => $group_id) 
                    {
                        $this->option_group->lang_id = $put['ProductOptionLangID'][1];
                        $this->option->lang_id = $put['ProductOptionLangID'][1];
                        $group = $this->option_group->getByContentID($group_id);
                        $option = $this->option->getByContentID($put['ProductOptionOptionContentIDs'][1][$key]);

                        $details[$group->GroupSlug]['group'] = $group->GroupName;
                        $details[$group->GroupSlug]['option'] = $option->OptionName;
                        $details[$group->GroupSlug]['option_id'] = $option->OptionContentID;
                    }

                    $data['ProductOptionDetails'] = json_encode($details);

                    $this->product_option->update(
                        array('ProductOptionID' => $put['ProductOptionID'][$i]),
                        $data
                    );

                    $this->product_option_photo->deleteByProductOptionID($put['ProductOptionID'][$i]);

                    if ($i == 1 && isset($put['PhotoSrc'][$i])) 
                    {
                        foreach ($put['PhotoSrc'][$i] as $key => $value) {
                            $data_img = array(
                                'PhotoName' => $value,
                                'PhotoTitle' => $put['PhotoTitle'][$i][$key],
                                'PhotoSort' => $put['PhotoSort'][$i][$key],
                                'PhotoProductOptionID' => $put['ProductOptionID'][$i],
                                'PhotoProductOptionContentID' => $content_id,
                                'PhotoLangID' => $put['ProductOptionLangID'][$i],
                                'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                            );
                            $this->product_option_photo->save($data_img);
                        }
                    }
                    elseif ($i > 1 && isset($put['PhotoSrc'][$i]) && !isset($put['ProductOptionCommonGallery'])) 
                    {
                        foreach ($put['PhotoSrc'][$i] as $key => $value) {
                            $data_img = array(
                                'PhotoName' => $value,
                                'PhotoTitle' => $put['PhotoTitle'][$i][$key],
                                'PhotoSort' => $put['PhotoSort'][$i][$key],
                                'PhotoProductOptionID' => $put['ProductOptionID'][$i],
                                'PhotoProductOptionContentID' => $content_id,
                                'PhotoLangID' => $put['ProductOptionLangID'][$i],
                                'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                            );
                            $this->product_option_photo->save($data_img);
                        }
                    }
                    elseif ($i > 1 && isset($put['PhotoSrc'][1]) && isset($put['ProductOptionCommonGallery'])) 
                    {
                        foreach ($put['PhotoSrc'][1] as $key => $value) {
                            $data_img = array(
                                'PhotoName' => $value,
                                'PhotoTitle' => $put['PhotoTitle'][1][$key],
                                'PhotoSort' => $put['PhotoSort'][1][$key],
                                'PhotoProductOptionID' => $put['ProductOptionID'][$i],
                                'PhotoProductOptionContentID' => $content_id,
                                'PhotoLangID' => $put['ProductOptionLangID'][$i],
                                'PhotoCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME)
                            );
                            $this->product_option_photo->save($data_img);
                        }
                    }
                }

                if (isset($put['ProductOptionFirstOption'])) 
                {
                    $this->product_option->firstOptionUpdate($content_id, $put['ProductOptionProductContentID']);
                }
                
                $this->response(
                    [
                        'ProductOptionContentID' => $content_id,
                        'ProductOptionProductID' => $put['ProductOptionProductID'],
                        'ProductOptionProductContentID' => $put['ProductOptionProductContentID'],
                        'status' => true,
                        'message' => 'Ürün opsiyonu başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir ürün opsiyonu yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $product_option = $this->product_option_relation->getByID($content_id);

        if ($product_option) 
        {
            $photos = $this->product_option_photo->getAllByProductOptionContentID($content_id);
            if (!empty($photos)) 
            {
                foreach ($photos as $key => $value) 
                {
                    if(file_exists($value['PhotoName']) && $value['PhotoName'])
                    {
                        unlink($value['PhotoName']);  
                    }
                }
            }

            $this->product_option->deleteByContentID($content_id);
            $this->product_option_relation->deleteByID($content_id);
           
            $this->response(
                [
                    'status' => true,
                    'message' => 'Ürün opsiyonu silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir ürün opsiyonu yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function photos_get($id = '')
    {
        if (!empty($id))
        {
            $result = $this->product_option_photo->getAllByProductOptionID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün galerisi bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    private function _product_option_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;
        
        for ($i=1; $i <= $language_count; $i++) 
        {
            $option_val = '';
            $counter = 0;
            foreach ($inputs['ProductOptionOptionContentIDs'][$i] as $key => $value) 
            {
                $counter++;
                $option_val .= '"'.$counter.'":"'.$inputs['ProductOptionOptionContentIDs'][$i][$counter].'",';

                if($inputs['ProductOptionOptionContentIDs'][$i][$counter] == '0')
                {
                    $data['inputerror'][][$i] = 'ProductOptionOptionContentIDs';
                    $data['inputerror_counter'][][$i] = $counter;
                    $data['error_string'][][$i] = 'Opsiyon seçiniz';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
            }

            if ($option_val != '') 
            {
                $option_val = rtrim($option_val, ',');
            
                if (isset($inputs['ProductOptionID'][$i])) 
                {
                    $is_option = $this->product_option->isThereOptionWithProductRelation(
                        $option_val,
                        $inputs['ProductOptionProductContentID'],
                        $inputs['ProductOptionContentID']
                    );
                }
                else
                {
                    $is_option = $this->product_option->isThereOptionWithProductRelation(
                        $option_val,
                        $counter, 
                        $inputs['ProductOptionProductContentID']
                    );
                }

                if ($is_option) 
                {
                    $data['inputerror'][][$i] = 'ProductOptionOptionContentIDs-popup';
                    $data['inputerror_counter'][][$i] = $counter;
                    $data['error_string'][][$i] = 'Bu varyasyondan zaten var!';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
            }

            if($inputs['ProductOptionVatPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionVatPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'Kdv Miktarı giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionDiscountPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionDiscountPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İndirim Miktarı giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionVatInclusivePrice'][$i] == '' || $inputs['ProductOptionVatInclusivePrice'][$i] == '0' || $inputs['ProductOptionVatInclusivePrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionVatInclusivePrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'Kdvli Fiyat giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionLatestPrice'][$i] == '' || $inputs['ProductOptionLatestPrice'][$i] == '0' || $inputs['ProductOptionLatestPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionLatestPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'Son Fiyat giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionLatestPrice'][$i] > $inputs['ProductOptionVatInclusivePrice'][$i])
            {
                $data['inputerror'][][$i] = 'ProductOptionLatestPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İndirimli fiyat kdvli fiyattan büyük olamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSecondVatPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionSecondVatPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İkinci Kdvli Fiyat giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSecondDiscountPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionSecondDiscountPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İkinci İndirim Miktarı giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSecondVatInclusivePrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionSecondVatInclusivePrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İkinci Kdvli Fiyat giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSecondLatestPrice'][$i] == 'NaN')
            {
                $data['inputerror'][][$i] = 'ProductOptionSecondLatestPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İkinci Son Fiyat giriniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSecondLatestPrice'][$i] > $inputs['ProductOptionSecondVatInclusivePrice'][$i])
            {
                $data['inputerror'][][$i] = 'ProductOptionSecondLatestPrice';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'İkinci İndirimli fiyat kdvli fiyattan büyük olamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductOptionSKU'][$i] == '')
            {
                $data['inputerror'][][$i] = 'ProductOptionSKU';
                $data['inputerror_counter'][][$i] = 0;
                $data['error_string'][][$i] = 'Ürün SKU boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}