<?php

class Products extends MY_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');

        $this->load->model('product/product_model','product');
        $this->load->model('product/product_relation_model','product_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->product->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->product->getByContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if (!empty($result))
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);
        $file = json_decode($post_data['files'], true);

        $validate = $this->_product_validate($post, $file[1]['image'], 'POST', $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->product_relation->save(
                array('ProductRelationModul' => 'Product', 'ProductRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            $filename = '';

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'ProductName' => $post['ProductName'][$i],
                    'ProductCode' => $post['ProductCode'][$i],
                    'ProductSlug' => seo_url($post['ProductName'][$i]) . '-' . $content_id,
                    'ProductCategoryID' => $post['ProductCategoryID'][$i],
                    'ProductCategoryContentID' => $post['ProductCategoryContentID'][$i],
                    'ProductOptionGroups' => json_encode($post['ProductOptionGroups'][1]),
                    'ProductTags' => isset($post['ProductTags'][1]) ? json_encode($post['ProductTags'][1]) : NULL,
                    'ProductDescription' => $post['ProductDescription'][$i],
                    'ProductPhotoAlt' => $post['ProductPhotoAlt'][$i],
                    'ProductStatus' => 1,
                    'ProductLangID' => $post['ProductLangID'][$i],
                    'ProductContentID' => $content_id,
                    'ProductCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                if (!empty($file[$i]['image'])) 
                {
                    $image = $file[$i]['image'];
                    list($type, $image) = explode(';', $image);
                    list(, $image) = explode(',', $image);
                    $image = base64_decode($image);
                    $encryption = crc32(date('Y-m-d H:i:s') . uniqid(mt_rand(), true));
                    $filename = 'upload/product/default/' . $encryption . '-' . image_url($file[$i]['name']);
                    $image_upload = file_put_contents('./' . $filename, $image);

                    if (isset($image_upload)) 
                    {
                        $data['ProductPhoto'] = $filename;
                        $this->image_resize($filename, $filename);
                    }
                }

                if (!isset($post['ProductCommonPhoto'])) 
                {
                    $data['ProductCommonPhoto'] = 0;
                }
                else
                {
                    $data['ProductCommonPhoto'] = 1;
                    $data['ProductPhoto'] = $filename;
                }

                $product_id = $this->product->save($data);
            }
         
            $this->response(
                [
                    'ProductContentID' => $content_id,
                    'status' => true,
                    'message' => 'Ürün başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $file = json_decode($put_data['files'], true);
        $content_id = $put['ProductContentID'];

        $product = $this->product_relation->getByID($content_id);

        if ($product) 
        {
            $validate = $this->_product_validate($put, $file[1]['image'], 'PUT', $language_count);

            if ($validate) 
            {
                
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Ürün güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->product_relation->update(
                    array('ProductRelationID' => $content_id), 
                    array('ProductRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                $filename = '';

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'ProductName' => $put['ProductName'][$i],
                        'ProductCode' => $put['ProductCode'][$i],
                        'ProductSlug' => seo_url($put['ProductName'][$i]) . '-' . $content_id,
                        'ProductCategoryID' => $put['ProductCategoryID'][$i],
                        'ProductCategoryContentID' => $put['ProductCategoryContentID'][$i],
                        'ProductOptionGroups' => json_encode($put['ProductOptionGroups'][1]),
                        'ProductTags' => isset($put['ProductTags'][1]) ? json_encode($put['ProductTags'][1]) : NULL,
                        'ProductDescription' => $put['ProductDescription'][$i],
                        'ProductPhotoAlt' => $put['ProductPhotoAlt'][$i],
                        'ProductStatus' => 1,
                        'ProductLangID' => $put['ProductLangID'][$i],
                        'ProductContentID' => $content_id,
                        'ProductUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    if (!empty($file[$i]['image'])) 
                    {
                        $image = $file[$i]['image'];
                        list($type, $image) = explode(';', $image);
                        list(, $image) = explode(',', $image);
                        $image = base64_decode($image);
                        $encryption = crc32(date('Y-m-d H:i:s') . uniqid(mt_rand(), true));
                        $filename = 'upload/product/default/' . $encryption . '-' . image_url($file[$i]['name']);
                        $image_upload = file_put_contents('./' . $filename, $image);

                        if (isset($put['RemovePhoto'][$i])) 
                        {
                            if(file_exists($put['RemovePhoto'][$i]) && isset($image_upload))
                            {
                                $this->image_resize_remove($put['RemovePhoto'][$i]);
                            }
                        }

                        if (isset($image_upload)) 
                        {
                            $data['ProductPhoto'] = $filename;
                            $this->image_resize($filename, $filename);
                        }
                    }
                    elseif ($i > 1 && isset($put['ProductCommonPhoto']))
                    {
                        if (!empty($filename)) 
                        {
                            $data['ProductPhoto'] = $filename;
                        }
                        else
                        {
                            $data['ProductPhoto'] = $put['RemovePhoto'][1];
                        }
                    }

                    if (!isset($put['ProductCommonPhoto'])) 
                    {
                        $data['ProductCommonPhoto'] = 0;
                    }
                    else
                    {
                        $data['ProductCommonPhoto'] = 1;
                    }

                    $this->product->update(
                        array('ProductID' => $put['ProductID'][$i]),
                        $data
                    );
                }

                $this->response(
                    [
                        'ProductContentID' => $content_id,
                        'status' => true,
                        'message' => 'Ürün başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir ürün yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $this->load->model('product/product_option_model','product_option');
        $this->load->model('product/product_option_relation_model','product_option_relation');
        $this->load->model('product/product_option_photo_model', 'product_option_photo');

        $product = $this->product_relation->getByID($content_id);

        if ($product) 
        {
            $product_options = $this->product_option->getAllByProductContentID($content_id);
            if ($product_options)
            {
                foreach ($product_options as $key => $value)
                {
                    $this->product_option_relation->deleteByID($value['ProductOptionContentID']);

                    $photos = $this->product_option_photo->getAllByProductOptionContentID($value['ProductOptionContentID']);
                    if (!empty($photos)) 
                    {
                        foreach ($photos as $key => $value) 
                        {
                            if(file_exists($value['PhotoName']) && $value['PhotoName'])
                            {
                                $this->image_resize_remove($value['PhotoName']);  
                            }
                        }
                    }
                }
            }

            $product_content = $this->product->getByContentID($content_id);
            foreach ($product_content as $key => $value) 
            {
                $row = $this->product->getByID($value['ProductID']);
                if(file_exists($row->ProductPhoto) && $row->ProductPhoto)
                {
                    $this->image_resize_remove($row->ProductPhoto);  
                }
            }

            $this->product->deleteByContentID($content_id);
            $this->product_relation->deleteByID($content_id);
           
            $this->response(
                [
                    'status' => true,
                    'message' => 'Ürün silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir ürün yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function options_get($content_id = '', $option_content_id = '')
    {
        $this->load->model('product/product_option_model','product_option');

        if (!empty($this->get('lang_id'))) 
        {
            $this->product_option->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id) && !empty($option_content_id)) 
        {
            $result = NULL;
        }
        elseif (!empty($content_id))
        {
            $result = $this->product_option->getAllByProductContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Ürün opsiyonu bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->product->get_data = $get_data;
        $this->product->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->product->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->product->getCountAll();
    }
    
    private function count_filtered()
    {
        return $this->product->getCountFiltered();
    }

    private function _product_validate($inputs, $file, $request_type, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['ProductName'][$i] == '')
            {
                $data['inputerror'][][$i] = 'ProductName';
                $data['error_string'][][$i] = 'Ürün Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductMainCategoryID'][$i] == '0')
            {
                $data['inputerror'][][$i] = 'ProductMainCategoryID';
                $data['error_string'][][$i] = 'Kategori seçiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if($inputs['ProductCategoryContentID'][$i] == '0')
            {
                $data['inputerror'][][$i] = 'ProductCategoryContentID';
                $data['error_string'][][$i] = 'Alt Kategori seçiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if(empty($inputs['ProductOptionGroups'][1][0]))
            {
                $data['inputerror'][][$i] = 'ProductOptionGroups';
                $data['error_string'][][$i] = 'Opsiyon Gurubu seçiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if(empty($file) && $request_type == 'POST')
            {
                $data['inputerror'][][$i] = 'ProductPhoto';
                $data['error_string'][][$i] = 'Fotoğraf yükleyiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}
