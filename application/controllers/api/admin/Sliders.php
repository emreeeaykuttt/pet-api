<?php

class Sliders extends MY_Controller 
{

    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');

        $this->load->model('slider/slider_model','slider');
        $this->load->model('slider/slider_relation_model','slider_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {
        if (!empty($this->get('lang_id'))) 
        {
            $this->slider->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->slider->getByContentID($content_id);
        }
        else
        {
            $result = NULL;
        }

        if (!empty($result))
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Slider bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);
        $file = json_decode($post_data['files'], true);

        $validate = $this->_slider_validate($post, $file[1]['image'], 'POST', $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Slider oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->slider_relation->save(
                array('SliderRelationModul' => 'Slider', 'SliderRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            $filename = '';

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'SliderTitle' => $post['SliderTitle'][$i],
                    'SliderLink' => $post['SliderLink'][$i],
                    'SliderDescription' => $post['SliderDescription'][$i],
                    'SliderSort' => $post['SliderSort'][$i],
                    'SliderLangID' => $post['SliderLangID'][$i],
                    'SliderContentID' => $content_id,
                    'SliderCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                if (!empty($file[$i]['image'])) 
                {
                    $image = $file[$i]['image'];
                    list($type, $image) = explode(';', $image);
                    list(, $image) = explode(',', $image);
                    $image = base64_decode($image);
                    $encryption = crc32(date('Y-m-d H:i:s') . uniqid(mt_rand(), true));
                    $filename = 'upload/slider/' . $encryption . '-' . image_url($file[$i]['name']);
                    $image_upload = file_put_contents('./' . $filename, $image);

                    if (isset($image_upload)) 
                    {
                        $data['SliderPhoto'] = $filename;
                        $this->image_resize($filename, $filename);
                    }
                }

                $slider_id = $this->slider->save($data);
            }
         
            $this->response(
                [
                    'sliderContentID' => $content_id,
                    'status' => true,
                    'message' => 'Slider başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $file = json_decode($put_data['files'], true);
        $content_id = $put['SliderContentID'];

        $slider = $this->slider_relation->getByID($content_id);

        if ($slider) 
        {
            $validate = $this->_slider_validate($put, $file[1]['image'], 'PUT', $language_count);

            if ($validate) 
            {
                
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Slider güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->slider_relation->update(
                    array('SliderRelationID' => $content_id), 
                    array('SliderRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                $filename = '';

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'SliderTitle' => $put['SliderTitle'][$i],
                        'SliderLink' => $put['SliderLink'][$i],
                        'SliderDescription' => $put['SliderDescription'][$i],
                        'SliderSort' => $put['SliderSort'][$i],
                        'SliderLangID' => $put['SliderLangID'][$i],
                        'SliderContentID' => $content_id,
                        'SliderUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    if (!empty($file[$i]['image'])) 
                    {
                        $image = $file[$i]['image'];
                        list($type, $image) = explode(';', $image);
                        list(, $image) = explode(',', $image);
                        $image = base64_decode($image);
                        $encryption = crc32(date('Y-m-d H:i:s') . uniqid(mt_rand(), true));
                        $filename = 'upload/slider/' . $encryption . '-' . image_url($file[$i]['name']);
                        $image_upload = file_put_contents('./' . $filename, $image);

                        if (isset($put['RemovePhoto'][$i])) 
                        {
                            if(file_exists($put['RemovePhoto'][$i]) && isset($image_upload))
                            {
                                $this->image_resize_remove($put['RemovePhoto'][$i]);
                            }
                        }

                        if (isset($image_upload)) 
                        {
                            $data['SliderPhoto'] = $filename;
                            $this->image_resize($filename, $filename);
                        }
                    }

                    $this->slider->update(
                        array('SliderID' => $put['SliderID'][$i]),
                        $data
                    );
                }

                $this->response(
                    [
                        'SliderContentID' => $content_id,
                        'status' => true,
                        'message' => 'Slider başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir slider yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $slider = $this->slider_relation->getByID($content_id);

        if ($slider) 
        {
            $slider_content = $this->slider->getByContentID($content_id);
            foreach ($slider_content as $key => $value) 
            {
                $row = $this->slider->getByID($value['SliderID']);
                if(file_exists($row->SliderPhoto) && $row->SliderPhoto)
                {
                    $this->image_resize_remove($row->SliderPhoto);  
                }
            }

            $this->slider->deleteByContentID($content_id);
            $this->slider_relation->deleteByID($content_id);
           
            $this->response(
                [
                    'status' => true,
                    'message' => 'Slider silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir slider yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->slider->get_data = $get_data;
        $this->slider->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->slider->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->slider->getCountAll();
    }
    
    private function count_filtered()
    {
        return $this->slider->getCountFiltered();
    }

    private function _slider_validate($inputs, $file, $request_type, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['SliderTitle'][$i] == '')
            {
                $data['inputerror'][][$i] = 'SliderTitle';
                $data['error_string'][][$i] = 'Başlık alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }

            if(empty($file) && $request_type == 'POST')
            {
                $data['inputerror'][][$i] = 'SliderPhoto';
                $data['error_string'][][$i] = 'Fotoğraf yükleyiniz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}
