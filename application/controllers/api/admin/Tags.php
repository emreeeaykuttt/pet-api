<?php

class Tags extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->verify_request('admin');
        
        $this->load->model('product/tag_model','tag');
        $this->load->model('product/tag_relation_model','tag_relation');
        $this->load->model('language/language_model', 'language');
    }

    public function index_get($content_id = '')
    {   
        if (!empty($this->get('lang_id'))) 
        {
            $this->tag->lang_id = $this->get('lang_id');
        }

        if (!empty($content_id)) 
        {
            $result = $this->tag->getByContentID($content_id);
        }
        elseif (!empty($this->get('title'))) 
        {
            $result = $this->tag->getAllByTitle($this->get('title'));
        }
        elseif (!empty($this->get('content_ids'))) 
        {
            $result = $this->tag->getAllByContentIDs($this->get('content_ids'));
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Etiket bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_post()
    {
        $language_count = $this->language->getCountByStatus();
        $post_data = $this->post(null, true);
        $post = json_decode($post_data['data'], true);

        $validate = $this->_tag_validate($post, $language_count);

        if ($validate) 
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Etiket oluşturulurken bir sorun oluştu',
                    'error' => $validate,
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
        else
        {
            $content_id = $this->tag_relation->save(
                array('TagRelationModul' => 'ProductTag', 'TagRelationCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
            );

            for ($i=1; $i <= $language_count; $i++) 
            {
                $data = array(
                    'TagTitle' => $post['TagTitle'][$i],
                    'TagLangID' => $post['TagLangID'][$i],
                    'TagContentID' => $content_id,
                    'TagCreatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                );

                $tag_id = $this->tag->save($data);
            }
         
            $this->response(
                [
                    'TagContentID' => $content_id,
                    'status' => true,
                    'message' => 'Etiket başarılı bir şekilde oluşturuldu'
                ],
                REST_Controller::HTTP_CREATED
            );
        }
    }

    public function index_put()
    {
        $language_count = $this->language->getCountByStatus();
        $put_data = $this->put(null, true);
        $put = json_decode($put_data['data'], true);
        $content_id = $put['TagContentID'];

        $tag = $this->tag_relation->getByID($content_id);

        if ($tag) 
        {
            $validate = $this->_tag_validate($put, $language_count);

            if ($validate) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Etiket güncellerken bir sorun oluştu',
                        'error' => $validate,
                    ], 
                    REST_Controller::HTTP_BAD_REQUEST
                );
            }
            else
            {
                $this->tag_relation->update(
                    array('TagRelationID' => $content_id), 
                    array('TagRelationUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME))
                );

                for ($i=1; $i <= $language_count; $i++) 
                {
                    $data = array(
                        'TagTitle' => $put['TagTitle'][$i],
                        'TagLangID' => $put['TagLangID'][$i],
                        'TagContentID' => $content_id,
                        'TagUpdatedAt' => date('Y-m-d H:i:s', NOW_DATE_TIME),
                    );

                    $this->tag->update(
                        array('TagID' => $put['TagID'][$i]),
                        $data
                    );
                }
                
                $this->response(
                    [
                        'TagContentID' => $content_id,
                        'status' => true,
                        'message' => 'Etiket başarılı bir şekilde güncellendi'
                    ],
                    REST_Controller::HTTP_OK
                );
            }
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir etiket yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function index_delete($content_id)
    {
        $tag = $this->tag_relation->getByID($content_id);

        if ($tag) 
        {
            $this->tag->deleteByContentID($content_id);
            $this->tag_relation->deleteByID($content_id);

            $this->response(
                [
                    'status' => true,
                    'message' => 'Etiket silindi.'
                ],
                REST_Controller::HTTP_OK
            );
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Böyle bir etiket yoktur'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->tag->get_data = $get_data;
        $this->tag->lang_id = $this->get('lang_id');

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->tag->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->tag->getCountAll();
    }

    private function count_filtered()
    {
        return $this->tag->getCountFiltered();
    }

    private function _tag_validate($inputs, $language_count = '')
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['lang'] = array();
        $data['status'] = TRUE;

        for ($i=1; $i <= $language_count; $i++) 
        {
            if($inputs['TagTitle'][$i] == '')
            {
                $data['inputerror'][][$i] = 'TagTitle';
                $data['error_string'][][$i] = 'Tag Adı alanı boş bırakılamaz';
                $data['lang'][] = $i;
                $data['status'] = FALSE;
            }
            else
            {
                if (isset($inputs['TagContentID'])) 
                {
                    $is_tag = $this->tag->isThereTitle($inputs['TagTitle'][$i], $inputs['TagContentID']);
                }
                else
                {
                    $is_tag = $this->tag->isThereTitle($inputs['TagTitle'][$i]);
                }

                if ($is_tag) 
                {
                    $data['inputerror'][][$i] = 'TagTitle';
                    $data['error_string'][][$i] = $inputs['TagTitle'][$i] . ' etiketi zaten var';
                    $data['lang'][] = $i;
                    $data['status'] = FALSE;
                }
            }
        }       

        if($data['status'] === FALSE)
        {
            return $data;
        }
    }

}