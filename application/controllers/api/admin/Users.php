<?php

class Users extends MY_Controller {
    
    public function __construct()
    {
       parent::__construct();
       $this->verify_request('admin');
    
        $this->load->model('user/user_model','user');
    }

    public function index_get($id = '')
    {
        if (!empty($id)) 
        {
            $result = $this->user->getByID($id);
        }
        elseif (!empty($this->get('name_and_surname'))) 
        {
            $result = $this->user->getAllByNameAndSurname($this->get('name_and_surname'));
        }
        else
        {
            $result = NULL;
        }

        if (!empty($result))
        {
            $this->response($result, REST_Controller::HTTP_OK);   
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Kullanıcı bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function addresses_get($id = '', $address_id = '')
    {
        $this->load->model('user/address_model','address');

        if (!empty($id) && !empty($address_id)) 
        {
            $this->address->lang_id = 1;
            $result = $this->address->getByIDAndUserID($address_id, $id);
        }
        elseif (!empty($id))
        {
            $this->address->lang_id = 1;
            $result = $this->address->getAllByUserID($id);
        }
        else
        {
            $result = NULL;
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Adres Bulunamadı'
                ], 
                REST_Controller::HTTP_BAD_REQUEST
            );
        }
    }

    public function datatables_get()
    {
        $get_data = json_decode($this->get('data'), true);
        $this->user->get_data = $get_data;

        if (!empty($this->get('count'))) 
        {
            if ($this->get('count') == 'all') 
            {
                $result = $this->count_all();
            }
            elseif ($this->get('count') == 'filtered')
            {
                $result = $this->count_filtered();
            }
        }
        else
        {
            $result = $this->user->getDatatables();
        }

        if ($result)
        {
            $this->response($result, REST_Controller::HTTP_OK);
        }
        else
        {
            $this->response(0, REST_Controller::HTTP_OK);
        }
    }

    private function count_all()
    {
        return $this->user->getCountAll();
    }
    
    private function count_filtered()
    {
        return $this->user->getCountFiltered();
    }
}