<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class MY_Controller extends REST_Controller{

    public function __construct()
    {
        parent::__construct();
    }

    public function verify_request($scope = '')
    {
        $headers = $this->input->request_headers();
        $authorization = isset($headers['Authorization']) ? $headers['Authorization'] : '';

        try {
            $token = NULL;
            $auth_arr = explode('EA278602222342875160EA', $authorization);

            if (isset($auth_arr[1])) 
            {
                $token = $auth_arr[1];
            }

            $data = AUTHORIZATION::validateToken($token);

            if ($data === false) 
            {
                $this->response(
                    [
                        'status' => false,
                        'message' => 'Unauthorized Access'
                    ], 
                    REST_Controller::HTTP_UNAUTHORIZED
                );

                exit();
            } 
            else 
            {
                if (isset($data->scopes)) 
                {
                    $scopes = $data->scopes;

                    if (in_array($scope, $scopes))
                    {
                        return $data;
                    }
                    else
                    {
                        $this->response(
                            [
                                'status' => false,
                                'message' => 'Not Authorized'
                            ], 
                            REST_Controller::HTTP_UNAUTHORIZED
                        );
                    }
                }
                else
                {
                    $this->response(
                        [
                            'status' => false,
                            'message' => 'Not Authorized'
                        ], 
                        REST_Controller::HTTP_UNAUTHORIZED
                    );
                }
            }
        } catch (Exception $e) {
            $this->response(
                [
                    'status' => false,
                    'message' => 'Unauthorized Access'
                ], 
                REST_Controller::HTTP_UNAUTHORIZED
            );
        }
    }

    public function send_email($tos = array(), $ccs = array(), $bccs = array(), $sender_name = '', $subject = '', $message = '', $attachments = array())
    {
        require_once APPPATH . 'third_party/phpmailer/PHPMailerAutoload.php';

        $mail = new PHPMailer;

       // $mail->SMTPDebug = 3;

        $mail->isSMTP();
        $mail->SMTPAuth = true;
        $mail->Host = '46.45.158.252';
        $mail->Port = 587;
        $mail->CharSet = 'UTF-8';
        $mail->Username = 'bilgi@petshopevinde.com';
        $mail->Password = 'vqbMU!29';

        $mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $mail->setFrom('bilgi@petshopevinde.com', $sender_name);

        foreach ($tos as $to){
            if ($to){
                $mail->addAddress($to);
            }
        }

        $mail->addReplyTo('bilgi@petshopevinde.com', $sender_name);

        foreach ($ccs as $cc){
            if ($cc){
                $mail->addCC($cc);
            }
        }

        foreach ($bccs as $bcc){
            if ($bcc){
                $mail->addBCC($bcc);
            }

        }

        foreach ($attachments as $attachment){
            $mail->addAttachment($attachment['file_with_path']);
        }


        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = strip_tags($message);


        if(!$mail->send()) {
            echo 'Message could not be sent.';
            echo 'Mailer Error: ' . $mail->ErrorInfo;
            return false;
        } else {
            return true;
        }

    }

    public function mail_template($data, $link = '', $link_text = '', $img = '1.png')
    {
        $link_area = '';

        if (!empty($link)) 
        {
            $link_area = 
            '<tr>
                <td style="padding: 10px 0 30px 0;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="35%"></td>
                            <td align="center" style="padding: 15px 0 15px 0; border: 2px solid #000;">
                                <a href="'. $link .'" target="_blank" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 18px; text-decoration: none;">
                                    <b>'. $link_text .'</b>
                                </a>
                            </td>
                            <td width="35%"></td>
                        </tr>
                    </table>
                </td>
            </tr>'; 
        }

        $template_html = 
        '<!DOCTYPE html>
        <html>

        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>petshopevinde.com</title>
            <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        </head>

        <body style="margin: 0; padding: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"> 
                <tr>
                    <td style="padding: 10px 0 30px 0;">
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #000; border-collapse: collapse;">
                            <tr>
                                <td bgcolor="#0064d2" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#76b900" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#fbbc05" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#e50914" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" bgcolor="#eeeeee" style="padding: 30px 0 30px 0;">
                                    <img src="'.base_url().'assets/img/mailing/logo.png" style="display: block;" />
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#ffffff" style="padding: 30px 0 10px 0;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tr>
                                            <td align="center" style="padding: 10px 0 30px 0; color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 16px; line-height: 22px;">
                                                '. $data .'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            '. $link_area .'
                            <tr>
                                <td bgcolor="#eeeeee" align="center" style="padding: 20px 0px 20px 0px;">
                                    <a href="https://petshopevinde.com/" target="_blank" style="color: #000000; font-family: Helvetica, Arial, sans-serif; font-size: 14px; text-decoration: none;">www.petshopevinde.com</a>
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#0064d2" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#76b900" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#fbbc05" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="#e50914" align="center" style="padding: 4px 0px 4px 0px;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </body>
        </html>';

        return $template_html;
    }

    public function image_resize_remove($path = '')
    {
        @unlink('resize/lg/' . $path);
        @unlink('resize/md/' . $path);
        @unlink('resize/sm/' . $path);
        @unlink('resize/xs/' . $path);
        @unlink($path);
    }

    public function image_resize($source_image = '', $new_image = '')
    {
        $this->load->library('image_lib');

        $config['image_library'] = 'GD2';
        $config['source_image'] = './' . $source_image;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'auto';
        $config['quality'] = '90%';
        $config['width'] = 1024;
        $config['new_image'] = './resize/lg/' . $new_image;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();

        $config['image_library'] = 'GD2';
        $config['source_image'] = $source_image;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'auto';
        $config['quality'] = '90%';
        $config['width'] = 600;
        $config['new_image'] = './resize/md/' . $new_image;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();

        $config['image_library'] = 'GD2';
        $config['source_image'] = $source_image;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'auto';
        $config['quality'] = '90%';
        $config['width'] = 300;
        $config['new_image'] = './resize/sm/' . $new_image;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();

        $config['image_library'] = 'GD2';
        $config['source_image'] = $source_image;
        $config['maintain_ratio'] = TRUE;
        $config['master_dim'] = 'auto';
        $config['quality'] = '90%';
        $config['width'] = 150;
        $config['new_image'] = './resize/xs/' . $new_image;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
        $this->image_lib->clear();
    }
    
}

?>