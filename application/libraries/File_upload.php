<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class File_upload 
{

    public function do_upload($file = '', $type = 'svg|pdf|doc|docx|jpg|gif|png|jpeg|JPG|PNG', $size = 10000, $path = './upload')
    {
        $CI =& get_instance();
        
        $config['upload_path']          = $path;
        $config['allowed_types']        = $type;
        $config['max_size']             = $size; 
        // $config['max_width']            = $size;
        // $config['max_height']           = $size;

        $CI->load->library('upload', $config);
        
        $files = $_FILES[$file];
        $filename = $files['name'];
        $new_name = str_replace(array('(', ')'), '', $filename);

        $ext = substr($filename,strrpos($filename,'.',-1),strlen($filename));
        $hash = md5(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(1,10))) . '_-_' . $new_name;
        
        $_FILES[$file]['name'] = $hash.$ext;
        $_FILES[$file]['type'] = $files['type'];
        $_FILES[$file]['tmp_name'] = $files['tmp_name'];
        $_FILES[$file]['error'] = $files['error'];
        $_FILES[$file]['size'] = $files['size'];

        $CI->upload->initialize($config);

        if(!$CI->upload->do_upload($file))
        {
            $data['inputerror'][] = $file;
            $data['error_string'][] = $CI->upload->display_errors('','');
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }

        return $CI->upload->data('file_name');
    }

    public function do_upload_lang($file_array = '', $lang_key = 1, $type = 'svg|pdf|doc|docx|jpg|gif|png|jpeg|JPG|PNG', $size = 10000, $path = './upload')
    {
        $CI =& get_instance();
        
        $config['upload_path'] = $path;
        $config['allowed_types'] = $type;
        $config['max_size'] = $size; 

        $CI->load->library('upload', $config);

        $files = $file_array;
        $filename = $files['name'][$lang_key];
        $new_name = str_replace(array('(', ')'), '', $filename);

        $ext = substr($filename,strrpos($filename,'.',-1),strlen($filename));
        $hash = md5(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(1,10))) . '_-_' . $new_name;
        
        $_FILES[$filename]['name'] = $hash.$ext;
        $_FILES[$filename]['type'] = $files['type'][$lang_key];
        $_FILES[$filename]['tmp_name'] = $files['tmp_name'][$lang_key];
        $_FILES[$filename]['error'] = $files['error'][$lang_key];
        $_FILES[$filename]['size'] = $files['size'][$lang_key];

        $CI->upload->initialize($config);

        if(!$CI->upload->do_upload($filename))
        {
            $data['inputerror'][] = $filename;
            $data['error_string'][] = $CI->upload->display_errors('','');
            $data['status'] = FALSE;
            echo json_encode($data);
            exit();
        }

        return $CI->upload->data('file_name');
    }

    public function do_upload_multiple($file = '', $type = 'svg|pdf|doc|docx|jpg|gif|png|jpeg|JPG|PNG', $size = 10000, $path = './upload')
    {
        $CI =& get_instance();

        $number_of_files = sizeof($_FILES[$file]['tmp_name']);
        $files = $_FILES[$file];

        $config['upload_path']          = './upload/';
        $config['allowed_types']        = $type;
        $config['max_size']             = $size; 

        $CI->load->library('upload', $config);

        for ($i = 0; $i < $number_of_files; $i++)
        {
            $filename = $files['name'][$i];
            $new_name = str_replace(array('(', ')'), '', $filename);

            $ext = substr($filename,strrpos($filename,'.',-1),strlen($filename));
            $hash = md5(substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, rand(1,10))) . '_-_' . $new_name;

            $_FILES[$file]['name'] = $hash.$ext;
            $_FILES[$file]['type'] = $files['type'][$i];
            $_FILES[$file]['tmp_name'] = $files['tmp_name'][$i];
            $_FILES[$file]['error'] = $files['error'][$i];
            $_FILES[$file]['size'] = $files['size'][$i];

            $CI->upload->initialize($config);

            if ($CI->upload->do_upload($file))
            {
                $uploaded[$i] = $CI->upload->data();
            }
            else
            {
                return FALSE;
            }
        }

        return $uploaded;
    }

}