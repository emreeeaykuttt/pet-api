<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City_model extends CI_Model {

	var $table = 'Cities';

	public function getAllByCountryID($country_id)
	{
		$query = $this->db->where('CityCountryID', $country_id)->where('CityID !=', 0)->order_by('CitySort', 'asc')->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
    {
        $query = $this->db->where('CityID', $id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

}