<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country_model extends CI_Model {

	var $table = 'Countries';

	public function getAll()
	{
		if (!empty($this->lang_id)) 
		{
			$lang_id = $this->lang_id;
		}
		else
		{
			$lang_id = 1;
		}

		$query = $this->db->where('CountryContentID !=', 0)->where('CountryLangID', $lang_id)->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
    {
    	if (!empty($this->lang_id)) 
		{
			$lang_id = $this->lang_id;
		}
		else
		{
			$lang_id = 1;
		}

        $query = $this->db->where('CountryID', $id)->where('CountryLangID', $lang_id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }
}