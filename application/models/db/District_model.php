<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class District_model extends CI_Model {

	var $table = 'Districts';

	public function getAllByCityID($district_id)
	{
		$query = $this->db->where('DistrictCityID', $district_id)->where('DistrictID !=', 0)->order_by('DistrictName', 'asc')->get($this->table);	

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
    {
        $query = $this->db->where('DistrictID', $id)->get($this->table);

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }
}