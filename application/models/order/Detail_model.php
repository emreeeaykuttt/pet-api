<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Detail_model extends CI_Model {

	var $table = 'OrderDetails';
	var $orders_table = 'Orders';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function getByID($id)
	{
		$this->db->from($this->table);
		$this->db->where('DetailID',$id);
		$query = $this->db->get();

		return $query->row();
	}

	public function deleteByID($id)
	{
		$this->db->where('DetailID', $id);
		$this->db->delete($this->table);
	}

	public function getByIDAndOrderID($id, $order_id)
	{
		$this->db->from($this->table);
		$this->db->where('DetailID',$id);
		$this->db->where('DetailOrderID',$order_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function getAllByOrderID($order_id)
	{
		$this->db->where('DetailOrderID', $order_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByOrderIDAndUserID($order_id, $user_id)
	{
		$this->db->where('DetailOrderID', $order_id);
		$this->db->where('DetailUserID', $user_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function bestSellingProducts()
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->select('COUNT(DetailProductContentID) AS product_total, DetailProductContentID');
		$this->db->group_by('DetailProductContentID');
		$this->db->order_by('product_total', 'DESC');

		if (!empty($this->limit))
		{
			$this->db->limit($this->limit);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}