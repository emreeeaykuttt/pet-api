<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

	var $table = 'Orders';
	var $details_table = 'OrderDetails';
	var $column_order = array('OrderTrackingNumber', 'OrderDeliveryFirstName', 'OrderLatestAmount', 'OrderPaymentType', 'OrderStatus', 'OrderShippingStatus', null);
	var $column_search = array('OrderTrackingNumber', 'OrderDeliveryFirstName', 'OrderLatestAmount', 'OrderPaymentType', 'OrderStatus', 'OrderShippingStatus');
	var $order = array('OrderID' => 'desc'); 

	private function _getDatatablesQuery($data)
	{
		$this->db->select('*, 
			(CASE OrderShippingStatus
				WHEN "pending" THEN "Bekliyor"
				WHEN "preparing" THEN "Hazırlanıyor"
				WHEN "shipped" THEN "Kargoya Verildi"
				WHEN "delivered" THEN "Teslim Edildi"
				ELSE "Sipariş Gönderim Durumu Boş"
			END) as OrderShippingStatusText,
			(CASE OrderStatus
				WHEN "pendingPayment" THEN "Bekleyen Ödeme"
				WHEN "processing" THEN "İşleniyor"
				WHEN "onHold" THEN "Beklemede"
				WHEN "completed" THEN "Tamamlandı"
				WHEN "cancelled" THEN "İptal Edildi"
				WHEN "refunded" THEN "Geri Ödendi"
				ELSE "Sipariş Ödeme Durumu Boş"
			END) as OrderStatusText,'
		);
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					if ($item == 'OrderDeliveryFirstName') 
					{
						$this->db->or_like('CONCAT(OrderDeliveryFirstName, " ", OrderDeliveryLastName)', $data['search']['value']);
					}
					else
					{
						$this->db->or_like($item, $data['search']['value']);
					}
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data);

		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function getByID($id)
	{
		$this->db->select('*, 
			(CASE OrderShippingStatus
				WHEN "pending" THEN "Bekliyor"
				WHEN "preparing" THEN "Hazırlanıyor"
				WHEN "shipped" THEN "Kargoya Verildi"
				WHEN "delivered" THEN "Teslim Edildi"
				ELSE "Sipariş Gönderim Durumu Boş"
			END) as OrderShippingStatusText,
			(CASE OrderStatus
				WHEN "pendingPayment" THEN "Bekleyen Ödeme"
				WHEN "processing" THEN "İşleniyor"
				WHEN "onHold" THEN "Beklemede"
				WHEN "completed" THEN "Tamamlandı"
				WHEN "cancelled" THEN "İptal Edildi"
				WHEN "refunded" THEN "Geri Ödendi"
				ELSE "Sipariş Ödeme Durumu Boş"
			END) as OrderStatusText,'
		);

		$this->db->from($this->table);
		$this->db->where('OrderID', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function getByIDAndUserID($id, $user_id)
	{
		$this->db->select('*, 
			(CASE OrderShippingStatus
				WHEN "pending" THEN "Bekliyor"
				WHEN "preparing" THEN "Hazırlanıyor"
				WHEN "shipped" THEN "Kargoya Verildi"
				WHEN "delivered" THEN "Teslim Edildi"
				ELSE "Sipariş Gönderim Durumu Boş"
			END) as OrderShippingStatusText,
			(CASE OrderStatus
				WHEN "pendingPayment" THEN "Bekleyen Ödeme"
				WHEN "processing" THEN "İşleniyor"
				WHEN "onHold" THEN "Beklemede"
				WHEN "completed" THEN "Tamamlandı"
				WHEN "cancelled" THEN "İptal Edildi"
				WHEN "refunded" THEN "Geri Ödendi"
				ELSE "Sipariş Ödeme Durumu Boş"
			END) as OrderStatusText,'
		);

		$this->db->from($this->table);
		$this->db->where('OrderID', $id);
		$this->db->where('OrderUserID', $user_id);
		$query = $this->db->get();

		return $query->row();
	}

	public function getAllByUserID($user_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->select('*, 
			(CASE OrderShippingStatus
				WHEN "pending" THEN "Bekliyor"
				WHEN "preparing" THEN "Hazırlanıyor"
				WHEN "shipped" THEN "Kargoya Verildi"
				WHEN "delivered" THEN "Teslim Edildi"
				ELSE "Sipariş Gönderim Durumu Boş"
			END) as OrderShippingStatusText,
			(CASE OrderStatus
				WHEN "pendingPayment" THEN "Bekleyen Ödeme"
				WHEN "processing" THEN "İşleniyor"
				WHEN "onHold" THEN "Beklemede"
				WHEN "completed" THEN "Tamamlandı"
				WHEN "cancelled" THEN "İptal Edildi"
				WHEN "refunded" THEN "Geri Ödendi"
				ELSE "Sipariş Ödeme Durumu Boş"
			END) as OrderStatusText,
			SUM(DetailQuantity) as OrderDetailTotal'
		);

		$this->db->from($this->table);

		$this->db->join($this->details_table, 'DetailOrderID = OrderID', 'LEFT');
		$this->db->group_by('DetailOrderID');
		$this->db->order_by('OrderID', 'DESC');

		$this->db->where('OrderUserID', $user_id);

		$query = $this->db->get();

		if ($query->num_rows() > 0) 
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

}