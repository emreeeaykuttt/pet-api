<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Basket_model extends CI_Model {

	var $table = 'ProductBaskets';
	var $products_table = 'Products';
	var $product_options_table = 'ProductOptions';
	var $product_option_photos_table = 'ProductOptionPhotos';
	var $categories_table = 'ProductCategories';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByIDAndUserID($id, $user_id)
	{
		$this->db->where('BasketID', $id);
		
		$this->db->group_start();
		$this->db->where('BasketUserID', $user_id)->or_where('BasketUnregisteredID', $user_id);
		$this->db->group_end();

		$this->db->delete($this->table);
	}

	public function deleteByUserID($user_id)
	{
		$this->db->where('BasketUserID', $user_id);
		$this->db->delete($this->table);
	}

	public function deleteByUnregisteredID()
	{
		$this->db->where('BasketUnregisteredID !=', NULL);
		$this->db->delete($this->table);
	}

	public function getAllByUserID($user_id)
	{
		$this->db->group_start();
		$this->db->where('BasketUserID', $user_id); 
		$this->db->or_where('BasketUnregisteredID', $user_id);
		$this->db->group_end(); 
		
		$this->db->order_by('BasketID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByUserIDAndJoin($user_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->group_start();
		$this->db->where('BasketUserID', $user_id); 
		$this->db->or_where('BasketUnregisteredID', $user_id);
		$this->db->group_end(); 

		$this->db->group_start();
		$this->db->where('ProductOptionUnlimited', 1);
		$this->db->or_where('ProductOptionQuantity >', 0);
		$this->db->group_end(); 
		
		$this->db->join($this->products_table, 'ProductID = BasketProductID');
		$this->db->join($this->product_options_table, 'ProductOptionID = BasketProductOptionID');
		$this->db->join($this->categories_table, 'CategoryID = ProductCategoryID', 'left');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		$this->db->order_by('BasketID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getCountAllByUserID($user_id)
	{
		$this->db->group_start();
		$this->db->where('BasketUserID', $user_id); 
		$this->db->or_where('BasketUnregisteredID', $user_id);
		$this->db->group_end(); 

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getByUserID($user_id)
	{
		if (!empty($this->product_option_content_id))
		{
			$this->db->where('BasketProductOptionContentID', $this->product_option_content_id);
		}

		$this->db->group_start();
		$this->db->where('BasketUserID', $user_id); 
		$this->db->or_where('BasketUnregisteredID', $user_id);
		$this->db->group_end(); 

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

}