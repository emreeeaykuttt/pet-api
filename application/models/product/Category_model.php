<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

	var $table = 'ProductCategories';
	var $column_order = array('ProductCategories.CategoryName', 'HeadCategory.CategoryName', null);
	var $column_search = array('ProductCategories.CategoryName', 'HeadCategory.CategoryName'); 
	var $order = array('ProductCategories.CategoryID' => 'desc'); 

	private function _getDatatablesQuery($data)
	{
		$this->db->select($this->table . '.*, HeadCategory.CategoryName as CategoryHeadName');

		$this->db->from($this->table);

		$this->db->join($this->table . ' AS HeadCategory', $this->table . '.CategoryParentID = HeadCategory.CategoryContentID', 'left');

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $data['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$this->db->where($this->table . '.CategoryLangID', $this->lang_id);
		$this->db->where('HeadCategory.CategoryLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data);

		$this->db->where($this->table . '.CategoryLangID', $this->lang_id);
		$this->db->where('HeadCategory.CategoryLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		$this->db->where('CategoryParentID !=', 0);
		$this->db->where('CategoryLangID', $this->lang_id);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('CategoryContentID', $content_id);
		$this->db->delete($this->table);
    }

	public function getAll()
	{
		if (!empty($this->lang_id))
		{
			$this->db->where('CategoryLangID', $this->lang_id);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentID($content_id)
	{
		if (!empty($this->lang_id))
		{
			$this->db->where('CategoryLangID', $this->lang_id);
		}

		$this->db->where('CategoryContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByParentID($parent_id)
	{
		if (!empty($this->lang_id))
		{
			$this->db->where('CategoryLangID', $this->lang_id);
		}

		$this->db->where('CategoryParentID', $parent_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}