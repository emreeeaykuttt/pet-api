<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_relation_model extends CI_Model {

	var $table = 'ProductCategoryRelations';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function getByID($id)
	{
		$this->db->from($this->table);
		$this->db->where('CategoryRelationID', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function deleteByID($id)
	{
		$this->db->where('CategoryRelationID', $id);
		$this->db->delete($this->table);
	}


}