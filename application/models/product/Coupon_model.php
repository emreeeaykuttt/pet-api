<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coupon_model extends CI_Model {

	var $table = 'Coupons';
	var $user_coupons_table = 'UserCoupons';
	var $column_order = array('CouponName', null);
	var $column_search = array('CouponName'); 
	var $order = array('CouponID' => 'desc'); 

	private function _getDatatablesQuery($data, $lang_id)
	{
		$this->db->select(
			$this->table . '.*, 
			COALESCE(UserCouponCount, 0) AS UserCouponCount,'
		);

		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $data['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}

		$this->db->join(
			'(SELECT UserCouponCouponID, COUNT(*) AS UserCouponCount FROM '.$this->user_coupons_table.' GROUP BY UserCouponCouponID) UserCouponCounts', 
			'UserCouponCounts.UserCouponCouponID = CouponID', 'LEFT'
		);
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$this->db->where('CouponLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		$this->db->where('CouponLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		$this->db->where('CouponLangID', $this->lang_id);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('CouponContentID', $content_id);
		$this->db->delete($this->table);
    }

    public function quantity_change($content_id, $operation)
	{
		$coupon = $this->db->where('CouponContentID', $content_id)->get($this->table)->row();

		if ($coupon->CuoponUnLimited == 0) 
		{
			if ($operation == 'plus') 
			{
				$quantity = $coupon->CuoponQuantity + 1;
			}
			elseif ($operation == 'minus') 
			{
				$quantity = $coupon->CuoponQuantity - 1;
			}

			$this->db->update($this->table, array('CuoponQuantity' => $quantity), array('CouponContentID' => $content_id));
		}
	}

    public function getAll()
	{
		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByUserID($user_id)
	{
		$this->db->where('CouponIsActive', 1);
		$this->db->where('CouponLangID', $this->lang_id);
		$this->db->where('UserCouponUserID', $user_id);

		$this->db->join($this->user_coupons_table, 'UserCouponCouponID = CouponID', 'LEFT');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentID($content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('CouponLangID', $this->lang_id);
		}

		$this->db->where('CouponContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentIDAndUserID($content_id, $user_id)
	{
		$this->db->where('CouponIsActive', 1);
		$this->db->where('CouponLangID', $this->lang_id);
		$this->db->where('CouponContentID', $content_id);
		$this->db->where('UserCouponUserID', $user_id);

		$this->db->join($this->user_coupons_table, 'UserCouponCouponID = CouponID', 'LEFT');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByCode($code)
	{
		$this->db->where('CouponIsActive', 1);
		$this->db->where('CouponLangID', $this->lang_id);
		$this->db->where('CouponCode', $code);

		$this->db->group_start();
			$this->db->where('CouponStartDate <=', date('Y-m-d H:i:s'));
			$this->db->where('CouponEndDate >=', date('Y-m-d H:i:s'));
			$this->db->or_group_start();
                $this->db->where('CouponStartDate <=', date('Y-m-d H:i:s'));
				$this->db->where('CouponIsEndDate', 1);
            $this->db->group_end();
		$this->db->group_end();

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByCodeAndUserID($code, $user_id)
	{
		$this->db->where('CouponIsActive', 1);
		$this->db->where('CouponLangID', $this->lang_id);
		$this->db->where('CouponCode', $code);
		$this->db->where('UserCouponUserID', $user_id);

		$this->db->join($this->user_coupons_table, 'UserCouponCouponID = CouponID', 'LEFT');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}
}