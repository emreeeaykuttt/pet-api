<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_group_model extends CI_Model {

	var $table = 'OptionGroups';
	var $options_table = 'Options';
	var $column_order = array('OptionGroups.GroupName', 'OptionGroups.GroupSort', null);
	var $column_search = array('OptionGroups.GroupName', 'OptionGroups.GroupSort'); 
	var $order = array('OptionGroups.GroupID' => 'desc'); 

	private function _getDatatablesQuery($data, $lang_id)
	{
		$this->db->select(
			$this->table . '.*, 
			COALESCE(OptionCount, 0) AS OptionCount,'
		);

		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $data['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}

		$this->db->join(
			'(SELECT OptionGroupID, COUNT(*) AS OptionCount FROM '.$this->options_table.' WHERE OptionLangID = '.$lang_id.' GROUP BY OptionGroupID) OptionCounts', 
			'OptionCounts.OptionGroupID = GroupID', 'LEFT'
		);
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$this->db->where('GroupLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		$this->db->where('GroupLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		$this->db->where('GroupLangID', $this->lang_id);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('GroupContentID', $content_id);
		$this->db->delete($this->table);
    }

    public function getAll()
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllAndJoinOption()
	{
		$this->db->join($this->options_table, 'OptionGroupID = GroupID');

		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
			$this->db->where('OptionLangID', $this->lang_id);
		}

		$this->db->order_by('GroupSort', 'ASC');
		$this->db->order_by('GroupName', 'ASC');
		$this->db->order_by('OptionSort', 'ASC');
		$this->db->order_by('OptionName', 'ASC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllAndIsProducts($products)
	{
		$this->db->join($this->options_table, 'OptionGroupID = GroupID');

		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
			$this->db->where('OptionLangID', $this->lang_id);
		}

		if ($products) {
			$group_arr = array();
			$option_arr = array();
			
			$this->db->group_start();
			foreach ($products as $key => $product) {
				foreach (json_decode($product['ProductOptionOptionContentIDs']) as $key => $option_content_id) 
				{
					if (!in_array($option_content_id, $option_arr)) 
					{
						$option_arr[] = $option_content_id;
						
						if ($key == 0) {
							$this->db->where('OptionContentID', $option_content_id);
						} else {
							$this->db->or_where('OptionContentID', $option_content_id);
						}
					}
				}
			}
			$this->db->group_end(); 
		} else {
			$this->db->where('OptionContentID', 0);
		}

		$this->db->order_by('GroupSort', 'ASC');
		$this->db->order_by('GroupName', 'ASC');
		$this->db->order_by('OptionSort', 'ASC');
		$this->db->order_by('OptionName', 'ASC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentID($content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
		}

		$this->db->where('GroupContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentIDAndJoinOption($content_id)
	{
		$this->db->join($this->options_table, 'OptionGroupID = GroupID');

		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
			$this->db->where('OptionLangID', $this->lang_id);
		}

		$this->db->where('GroupContentID', $content_id);
		$this->db->order_by('OptionSort', 'ASC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}