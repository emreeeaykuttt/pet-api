<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option_model extends CI_Model {

	var $table = 'Options';
	var $option_groups_table = 'OptionGroups';

	public function getAllByGroupContentID($group_content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('OptionLangID', $this->lang_id);
		}

		$this->db->where('OptionGroupContentID', $group_content_id);
		$this->db->order_by('OptionContentID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

    public function getByContentID($content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('OptionLangID', $this->lang_id);
		}
		
		$this->db->where('OptionContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentIDAndJoinOption($content_id)
	{
		$this->db->join($this->option_groups_table, 'OptionGroupID = GroupID');

		if (!empty($this->lang_id)) 
		{
			$this->db->where('GroupLangID', $this->lang_id);
			$this->db->where('OptionLangID', $this->lang_id);
		}

		$this->db->where('OptionContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByName($name)
	{
		$this->db->where('OptionLangID', $this->lang_id);
		$this->db->where('OptionName', $name);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('OptionContentID', $content_id);
		$this->db->delete($this->table);
    }

}