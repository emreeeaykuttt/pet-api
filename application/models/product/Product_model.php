<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

	var $table = 'Products';
	var $categories_table = 'ProductCategories';
	var $product_options_table = 'ProductOptions';
	var $product_option_photos_table = 'ProductOptionPhotos';
	var $user_product_favorites_table = 'UserProductFavorites';
	var $column_order = array('Products.ProductName', 'Products.ProductCode', 'ProductCategories.CategoryName', null);
	var $column_search = array('Products.ProductName', 'Products.ProductCode', 'ProductCategories.CategoryName'); 
	var $order = array('ProductID' => 'desc'); 

	private function _getDatatablesQuery($data, $lang_id)
	{
		$this->db->select(
			$this->table . '.*, ' . 
			$this->categories_table . '.CategoryName,
			COALESCE(ProductOptionCount, 0) AS ProductOptionCount,'
		);
		
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $data['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		$this->db->join(
			'(SELECT ProductOptionProductID, COUNT(*) AS ProductOptionCount FROM '.$this->product_options_table.' WHERE ProductOptionLangID = '.$lang_id.' GROUP BY ProductOptionProductID) ProductOptionCounts', 
			'ProductOptionCounts.ProductOptionProductID = ProductID', 'LEFT'
		);
		$this->db->join($this->categories_table, 'CategoryID = ProductCategoryID');
		$this->db->where('CategoryLangID', $lang_id);
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$this->db->where('ProductLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		$this->db->where('ProductLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		$this->db->where('ProductLangID', $this->lang_id);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('ProductContentID', $content_id);
		$this->db->delete($this->table);
    }

	public function getAllByStatus($status, $filter_status = true)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');
		
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionFirstOption', 1);

		if ($filter_status) 
		{
			if (!empty($this->limit))
			{
				$this->db->limit($this->limit, $this->offset);
			}
			elseif (!empty($this->per_page)) 
			{
				$per_page = $this->per_page;
				$page = $this->page;
				$limit_start = $per_page * ($page - 1);
				$this->db->limit($per_page, $limit_start);
			}

			if (!empty($this->min) && !empty($this->max))
			{
				$this->db->where('ProductOptionLatestPrice >=', $this->min);
				$this->db->where('ProductOptionLatestPrice <=', $this->max);
			}

			if (!empty($this->filter))
			{
				$filter_arr = [];
				foreach (json_decode($this->filter, true) as $key => $val) {
					$filter_arr[$val['name']][] = $val['value'];
				}
				
				foreach ($filter_arr as $k => $filters) {
					foreach ($filters as $key => $filter) {
						if ($key == 0) 
						{
							$this->db->group_start();
							$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
						}
						else
						{
							$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
						}
					}
					$this->db->group_end();
				}
			}

			if (!empty($this->tag))
			{
				foreach ($this->tag as $key => $value)
				{
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductTags', '"' . $value . '"');
					}
					else
					{
						$this->db->or_like('ProductTags', '"' . $value . '"');
					}
				}

				$this->db->group_end();
			}

			if (!empty($this->search))
			{
				if ($this->search != '') 
				{
					$this->db->group_start();
					$this->db->like('ProductName', $this->search);
					$this->db->or_like('ProductDescription', $this->search);
					$this->db->or_like('ProductOptionSKU', $this->search);
					$this->db->group_end();
				}
			}
		}

		$this->db->where('ProductStatus', $status);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$this->db->order_by('ProductOptionStockStatus', 'DESC');

		if (!empty($this->sort))
		{
			if ($this->sort == 'fiyata-gore-artan') 
			{
				$this->db->order_by('ProductOptionLatestPrice', 'ASC');
			}
			elseif ($this->sort == 'fiyata-gore-azalan')  
			{
				$this->db->order_by('ProductOptionLatestPrice', 'DESC');
			}
			elseif ($this->sort == 'urun-adina-gore-a-z')  
			{
				$this->db->order_by('ProductName', 'ASC');
			}
			elseif ($this->sort == 'urun-adina-gore-z-a')  
			{
				$this->db->order_by('ProductName', 'DESC');
			}
			else 
			{
				$this->db->order_by('ProductID', 'DESC');
			}
		} else {
			$this->db->order_by('ProductID', 'DESC');
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getCountAllByStatus($status)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');
		
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}
			
		$this->db->where('ProductOptionFirstOption', 1);

		if (!empty($this->min) && !empty($this->max))
		{
			$this->db->where('ProductOptionLatestPrice >=', $this->min);
			$this->db->where('ProductOptionLatestPrice <=', $this->max);
		}

		if (!empty($this->filter))
		{
			$filter_arr = [];
			foreach (json_decode($this->filter, true) as $key => $val) {
				$filter_arr[$val['name']][] = $val['value'];
			}
			
			foreach ($filter_arr as $k => $filters) {
				foreach ($filters as $key => $filter) {
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
					else
					{
						$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
				}
				$this->db->group_end();
			}
		}

		if (!empty($this->tag))
		{
			foreach ($this->tag as $key => $value)
			{
				if ($key == 0) 
				{
					$this->db->group_start();
					$this->db->like('ProductTags', '"' . $value . '"');
				}
				else
				{
					$this->db->or_like('ProductTags', '"' . $value . '"');
				}
			}

			$this->db->group_end();
		}

		if (!empty($this->search))
		{
			if ($this->search != '') 
			{
				$this->db->group_start();
				$this->db->like('ProductName', $this->search);
				$this->db->or_like('ProductDescription', $this->search);
				$this->db->or_like('ProductOptionSKU', $this->search);
				$this->db->group_end();
			}
		}

		$this->db->where('ProductStatus', $status);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getAllByProductContentIDs($products)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');
		
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		if (!empty($this->limit))
		{
			$this->db->limit($this->limit, $this->offset);
		}
		elseif (!empty($this->per_page)) 
		{
			$per_page = $this->per_page;
			$page = $this->page;
			$limit_start = $per_page * ($page - 1);
			$this->db->limit($per_page, $limit_start);
		}

		$this->db->where('ProductOptionFirstOption', 1);

		if (!empty($this->min) && !empty($this->max))
		{
			$this->db->where('ProductOptionLatestPrice >=', $this->min);
			$this->db->where('ProductOptionLatestPrice <=', $this->max);
		}

		if (!empty($this->filter))
		{
			$filter_arr = [];
			foreach (json_decode($this->filter, true) as $key => $val) {
				$filter_arr[$val['name']][] = $val['value'];
			}
			
			foreach ($filter_arr as $k => $filters) {
				foreach ($filters as $key => $filter) {
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
					else
					{
						$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
				}
				$this->db->group_end();
			}
		}

		if (!empty($this->tag))
		{
			foreach ($this->tag as $key => $value)
			{
				if ($key == 0) 
				{
					$this->db->group_start();
					$this->db->like('ProductTags', '"' . $value . '"');
				}
				else
				{
					$this->db->or_like('ProductTags', '"' . $value . '"');
				}
			}

			$this->db->group_end();
		}

		if (!empty($this->search))
		{
			if ($this->search != '') 
			{
				$this->db->group_start();
				$this->db->like('ProductName', $this->search);
				$this->db->or_like('ProductDescription', $this->search);
				$this->db->or_like('ProductOptionSKU', $this->search);
				$this->db->group_end();
			}
		}

		foreach ($products as $key => $value) 
		{
			if ($key == 0) 
			{
				$this->db->group_start();
				$this->db->where('ProductContentID', $value['DetailProductContentID']);
			}
			else
			{
				$this->db->or_where('ProductContentID', $value['DetailProductContentID']);
			}
		}

		$this->db->group_end();

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$this->db->order_by('ProductOptionStockStatus', 'DESC');

		if (!empty($this->sort))
		{
			if ($this->sort == 'fiyata-gore-artan') 
			{
				$this->db->order_by('ProductOptionLatestPrice', 'ASC');
			}
			elseif ($this->sort == 'fiyata-gore-azalan')  
			{
				$this->db->order_by('ProductOptionLatestPrice', 'DESC');
			}
			elseif ($this->sort == 'urun-adina-gore-a-z')  
			{
				$this->db->order_by('ProductName', 'ASC');
			}
			elseif ($this->sort == 'urun-adina-gore-z-a')  
			{
				$this->db->order_by('ProductName', 'DESC');
			}
			else 
			{
				$this->db->order_by('ProductID', 'DESC');
			}
		} else {
			$this->db->order_by('ProductID', 'DESC');
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getCountAllProductContentIDs($products)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');
		
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionFirstOption', 1);

		if (!empty($this->min) && !empty($this->max))
		{
			$this->db->where('ProductOptionLatestPrice >=', $this->min);
			$this->db->where('ProductOptionLatestPrice <=', $this->max);
		}

		if (!empty($this->filter))
		{
			$filter_arr = [];
			foreach (json_decode($this->filter, true) as $key => $val) {
				$filter_arr[$val['name']][] = $val['value'];
			}
			
			foreach ($filter_arr as $k => $filters) {
				foreach ($filters as $key => $filter) {
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
					else
					{
						$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
				}
				$this->db->group_end();
			}
		}

		if (!empty($this->tag))
		{
			foreach ($this->tag as $key => $value)
			{
				if ($key == 0) 
				{
					$this->db->group_start();
					$this->db->like('ProductTags', '"' . $value . '"');
				}
				else
				{
					$this->db->or_like('ProductTags', '"' . $value . '"');
				}
			}

			$this->db->group_end();
		}

		if (!empty($this->search))
		{
			if ($this->search != '') 
			{
				$this->db->group_start();
				$this->db->like('ProductName', $this->search);
				$this->db->or_like('ProductDescription', $this->search);
				$this->db->or_like('ProductOptionSKU', $this->search);
				$this->db->group_end();
			}
		}

		foreach ($products as $key => $value) 
		{
			if ($key == 0) 
			{
				$this->db->group_start();
				$this->db->where('ProductContentID', $value['DetailProductContentID']);
			}
			else
			{
				$this->db->or_where('ProductContentID', $value['DetailProductContentID']);
			}
		}

		$this->db->group_end();

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getAllByCategoryID($category_content_id, $filter_status = true)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');

		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionFirstOption', 1);

		if ($filter_status) 
		{
			if (!empty($this->limit))
			{
				$this->db->limit($this->limit, $this->offset);
			}
			elseif (!empty($this->per_page)) 
			{
				$per_page = $this->per_page;
				$page = $this->page;
				$limit_start = $per_page * ($page - 1);
				$this->db->limit($per_page, $limit_start);
			}
			
			if (!empty($this->min) && !empty($this->max))
			{
				$this->db->where('ProductOptionLatestPrice >=', $this->min);
				$this->db->where('ProductOptionLatestPrice <=', $this->max);
			}

			if (!empty($this->filter))
			{
				$filter_arr = [];
				foreach (json_decode($this->filter, true) as $key => $val) {
					$filter_arr[$val['name']][] = $val['value'];
				}
				
				foreach ($filter_arr as $k => $filters) {
					foreach ($filters as $key => $filter) {
						if ($key == 0) 
						{
							$this->db->group_start();
							$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
						}
						else
						{
							$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
						}
					}
					$this->db->group_end();
				}
			}

			if (!empty($this->tag))
			{
				foreach ($this->tag as $key => $value)
				{
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductTags', '"' . $value . '"');
					}
					else
					{
						$this->db->or_like('ProductTags', '"' . $value . '"');
					}
				}

				$this->db->group_end();
			}

			if (!empty($this->search))
			{
				if ($this->search != '') 
				{
					$this->db->group_start();
					$this->db->like('ProductName', $this->search);
					$this->db->or_like('ProductDescription', $this->search);
					$this->db->or_like('ProductOptionSKU', $this->search);
					$this->db->group_end();
				}
			}
		}

		$this->db->where('ProductCategoryContentID', $category_content_id);
		$this->db->where('ProductStatus', 1);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$this->db->order_by('ProductOptionStockStatus', 'DESC');

		if (!empty($this->sort))
		{
			if ($this->sort == 'fiyata-gore-artan') 
			{
				$this->db->order_by('ProductOptionLatestPrice', 'ASC');
			}
			elseif ($this->sort == 'fiyata-gore-azalan')  
			{
				$this->db->order_by('ProductOptionLatestPrice', 'DESC');
			}
			elseif ($this->sort == 'urun-adina-gore-a-z')  
			{
				$this->db->order_by('ProductName', 'ASC');
			}
			elseif ($this->sort == 'urun-adina-gore-z-a')  
			{
				$this->db->order_by('ProductName', 'DESC');
			}
			else 
			{
				$this->db->order_by('ProductID', 'DESC');
			}
		} else {
			$this->db->order_by('ProductID', 'DESC');
		}

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getCountAllByCategoryID($category_content_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join('(SELECT * FROM ProductOptionPhotos GROUP BY PhotoProductOptionID ORDER BY PhotoCreatedAt) AS NewProductOptionPhotos', 'NewProductOptionPhotos.PhotoProductOptionID = ProductOptionID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');

		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionFirstOption', 1);

		if (!empty($this->min) && !empty($this->max))
		{
			$this->db->where('ProductOptionLatestPrice >=', $this->min);
			$this->db->where('ProductOptionLatestPrice <=', $this->max);
		}

		if (!empty($this->filter))
		{
			$filter_arr = [];
			foreach (json_decode($this->filter, true) as $key => $val) {
				$filter_arr[$val['name']][] = $val['value'];
			}
			
			foreach ($filter_arr as $k => $filters) {
				foreach ($filters as $key => $filter) {
					if ($key == 0) 
					{
						$this->db->group_start();
						$this->db->like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
					else
					{
						$this->db->or_like('ProductOptionOptionContentIDs', '"' . $filter . '"');
					}
				}
				$this->db->group_end();
			}
		}

		if (!empty($this->tag))
		{
			foreach ($this->tag as $key => $value)
			{
				if ($key == 0) 
				{
					$this->db->group_start();
					$this->db->like('ProductTags', '"' . $value . '"');
				}
				else
				{
					$this->db->or_like('ProductTags', '"' . $value . '"');
				}
			}

			$this->db->group_end();
		}

		if (!empty($this->search))
		{
			if ($this->search != '') 
			{
				$this->db->group_start();
				$this->db->like('ProductName', $this->search);
				$this->db->or_like('ProductDescription', $this->search);
				$this->db->or_like('ProductOptionSKU', $this->search);
				$this->db->group_end();
			}
		}

		$this->db->where('ProductCategoryContentID', $category_content_id);
		$this->db->where('ProductStatus', 1);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getByContentIDAndFirstOption($content_id)
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");

		$this->db->select(
			$this->table . '.*, ' . 
			$this->product_options_table . '.*, ' . 
			$this->product_option_photos_table . '.*, ' . 
			$this->user_product_favorites_table . '.*, ' .  
			$this->categories_table . '.CategoryName,' . $this->categories_table . '.CategorySlug'
		);

		$this->db->join($this->categories_table, 'CategoryID = ProductCategoryID');
		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');
		$this->db->join($this->product_option_photos_table, 'PhotoProductOptionID = ProductOptionID', 'LEFT');
		$this->db->join($this->user_product_favorites_table, 'FavoriteProductID = ProductID', 'LEFT');

		// $this->db->where('ProductPhoto !=', '');

		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
			$this->db->where('ProductOptionLangID', $this->lang_id);
			$this->db->where('CategoryLangID', $this->lang_id);
		}

		$this->db->where('ProductContentID', $content_id);
		
		$this->db->where('ProductOptionFirstOption', 1);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByID($id)
	{
		$this->db->where('ProductID', $id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentID($content_id)
	{
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductLangID', $this->lang_id);
		}

		$this->db->where('ProductContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}
}