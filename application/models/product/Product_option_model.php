<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option_model extends CI_Model {

	var $table = 'ProductOptions';
	var $products_table = 'Products';

	public function getAllByProductContentID($product_content_id)
	{
		if (!empty($this->lang_id))
		{
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionProductContentID', $product_content_id);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();
		
		$this->db->order_by('ProductOptionContentID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByProductContentIDAndOptionContentIDJson($product_content_id, $option_val)
	{
		$this->db->join($this->products_table, 'ProductID = ProductOptionProductID', 'LEFT');

		if (!empty($this->lang_id))
		{
			$this->db->where('ProductOptionLangID', $this->lang_id);
			$this->db->where('ProductLangID', $this->lang_id);
		}

		$this->db->where('ProductOptionProductContentID', $product_content_id);
		$this->db->like('ProductOptionOptionContentIDs', $option_val);

		// $this->db->group_start();
		// $this->db->where('ProductOptionUnlimited', 1);
		// $this->db->or_where('ProductOptionQuantity >', 0);
		// $this->db->group_end();

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function getByContentID($content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}
		
		$this->db->where('ProductOptionContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByProductContentIDAndContentID($product_content_id, $content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('ProductOptionLangID', $this->lang_id);
		}
		
		$this->db->where('ProductOptionProductContentID', $product_content_id);
		$this->db->where('ProductOptionContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function quantity_update($content_id, $quantity)
    {
    	$this->db->set('ProductOptionQuantity', 'ProductOptionQuantity-' . $quantity, false);
    	$this->db->where('ProductOptionContentID', $content_id);
    	$this->db->where('ProductOptionUnlimited', 0);
    	$this->db->update($this->table);
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('ProductOptionContentID', $content_id);
		$this->db->delete($this->table);
    }

    public function isThereOptionWithProductRelation($option_val, $product_content_id, $content_id = '')
    {
    	if (!empty($content_id)) 
    	{
    		$this->db->where('ProductOptionContentID !=', $content_id);
    	}
    	
		$this->db->like('ProductOptionOptionContentIDs', $option_val);
		$this->db->where('ProductOptionProductContentID', $product_content_id);

		$query = $this->db->get($this->table);
		
		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }

    public function isThereOptionRelation($option_val)
    {
    	$this->db->like('ProductOptionOptionContentIDs', $option_val);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }

    public function isThereOptionGroupRelation($option_group_val)
    {
    	$this->db->like('ProductOptionGroupContentIDs', $option_group_val);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }

    public function firstOptionUpdate($content_id, $product_content_id)
    {
    	$this->db->update($this->table, array('ProductOptionFirstOption' => 0), array('ProductOptionProductContentID' => $product_content_id));
    	$this->db->update($this->table, array('ProductOptionFirstOption' => 1), array('ProductOptionContentID' => $content_id));
    }

}