<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_option_photo_model extends CI_Model {

	var $table = 'ProductOptionPhotos';

    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function deleteByProductOptionID($product_option_id)
    {
    	$this->db->where('PhotoProductOptionID', $product_option_id);
		$this->db->delete($this->table);
    }

    public function deleteByProductOptionContentID($product_option_content_id)
    {
    	$this->db->where('PhotoProductOptionContentID', $product_option_content_id);
		$this->db->delete($this->table);
    }

    public function getAllByProductOptionID($product_option_id)
	{		
		$this->db->where('PhotoProductOptionID', $product_option_id);
		$this->db->order_by('PhotoSort', 'ASC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

    public function getAllByProductOptionContentID($product_option_content_id)
	{
		$this->db->where('PhotoProductOptionContentID', $product_option_content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}