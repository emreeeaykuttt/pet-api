<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends CI_Model {

	var $table = 'ProductTags';
	var $column_order = array('TagTitle', null);
	var $column_search = array('TagTitle'); 
	var $order = array('TagID' => 'desc'); 

	private function _getDatatablesQuery($data, $lang_id)
	{
		$this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item)
		{
			if($data['search']['value'])
			{
				
				if($i===0)
				{
					$this->db->group_start();
					$this->db->like($item, $data['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $data['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) 
					$this->db->group_end(); 
			}
			$i++;
		}
		
		if(isset($data['order'])) 
		{
			$this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function getDatatables()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		if($data['length'] != -1)
		{
			$this->db->limit($data['length'], $data['start']);
		}

		$this->db->where('TagLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->result();
	}

	public function getCountFiltered()
	{
		$data = $this->get_data;
		$this->_getDatatablesQuery($data, $this->lang_id);

		$this->db->where('TagLangID', $this->lang_id);
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function getCountAll()
	{
		$this->db->from($this->table);
		$this->db->where('TagLangID', $this->lang_id);
		return $this->db->count_all_results();
	}

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data)
    {
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByContentID($content_id)
    {
    	$this->db->where('TagContentID', $content_id);
		$this->db->delete($this->table);
    }

    public function getAll()
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('TagLangID', $this->lang_id);
		}
		
		$this->db->order_by('TagTitle');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

	public function getByContentID($content_id)
	{
		if (!empty($this->lang_id)) 
		{
			$this->db->where('TagLangID', $this->lang_id);
		}

		$this->db->where('TagContentID', $content_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			if (!empty($this->lang_id))
			{
				return $query->row();
			}
			else
			{
				return $query->result_array();
			}
		}
		else
		{
			return FALSE;
		}
	}

	public function getByTitle($title)
    {
    	$this->db->like('TagTitle', $title, 'none');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
    }

    public function getAllByTitle($title)
    {
        $this->db->like('TagTitle', $title);
        $this->db->where('TagLangID', 1);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function getAllByContentIDs($content_ids)
    {
        $content_id_json = json_decode($content_ids, true);

        foreach ($content_id_json as $key => $value) 
        {
        	if ($key == 0) 
			{
				$this->db->group_start();
				$this->db->where('TagContentID', $value);
			}
			else
			{
				$this->db->or_where('TagContentID', $value);
			}
        }
        $this->db->group_end();

        $this->db->where('TagLangID', 1);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

	public function isThereTitle($title, $content_id = '')
    {
    	if ($content_id != '') 
    	{
    		$this->db->where('TagContentID !=', $content_id);
    	}

    	$this->db->like('TagTitle', $title, 'none');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
}