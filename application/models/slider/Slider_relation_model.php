<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_relation_model extends CI_Model {

	var $table = 'SliderRelations';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function getByID($id)
	{
		$this->db->from($this->table);
		$this->db->where('SliderRelationID', $id);
		$query = $this->db->get();

		return $query->row();
	}

	public function deleteByID($id)
	{
		$this->db->where('SliderRelationID', $id);
		$this->db->delete($this->table);
	}


}