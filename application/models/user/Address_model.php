<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address_model extends CI_Model {

	var $table = 'UserAddress';
	var $countries_table = 'Countries';
	var $cities_table = 'Cities';
	var $districts_table = 'Districts';

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	public function delete($where)
	{
		$this->db->where($where);
		$this->db->delete($this->table);
	}

	public function getByIDAndUserID($id, $user_id)
	{
		$this->db->where('AddressID', $id);
		$this->db->where('AddressUserID', $user_id);
		$this->db->where('CountryLangID', $this->lang_id);

		$this->db->join($this->countries_table, 'CountryContentID = AddressCountryID');
		$this->db->join($this->cities_table, 'CityID = AddressCityID');
		$this->db->join($this->districts_table, 'DistrictID = AddressDistrictID');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

	public function getAllByUserID($user_id)
	{
		$this->db->where('AddressUserID', $user_id);
		$this->db->where('CountryLangID', $this->lang_id);

		$this->db->join($this->countries_table, 'CountryContentID = AddressCountryID');
		$this->db->join($this->cities_table, 'CityID = AddressCityID');
		$this->db->join($this->districts_table, 'DistrictID = AddressDistrictID');

		$this->db->order_by('AddressID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

}