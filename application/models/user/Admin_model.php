<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	var $table = 'Admins';

	public function validate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('UserEmail', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->UserPassword;
            $id = $query->UserID;

            if (password_verify($password, $hashed_password) && $query->UserVerified == 1)
            {
                $last_login = date('Y-m-d H:i:s', NOW_DATE_TIME);
                $this->db->trans_start();
                $this->db->trans_strict(FALSE);
                $this->db->update($this->table, array('UserLastLogin' => $last_login, 'UserLastLoginIpAddress' => $this->input->ip_address()), array('UserID' => $id));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    return FALSE;
                }
                else
                {
                    $this->db->trans_commit();
                    return $query;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

}