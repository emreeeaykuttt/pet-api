<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorite_model extends CI_Model {

	var $table = 'UserProductFavorites';
	var $products_table = 'Products';
	var $categories_table = 'ProductCategories';
	var $product_options_table = 'ProductOptions';

    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function deleteByIDAndUserID($id, $user_id)
    {
    	$this->db->where('FavoriteID', $id);
    	$this->db->where('FavoriteUserID', $user_id);
		$this->db->delete($this->table);
    }

    public function getAllByUserID($user_id)
	{
		$this->db->join($this->products_table, 'ProductID = FavoriteProductID');
		$this->db->join($this->categories_table, 'CategoryID = ProductCategoryID');
		$this->db->join($this->product_options_table, 'ProductOptionProductID = ProductID', 'LEFT');

		$this->db->where('FavoriteUserID', $user_id);
		$this->db->where('ProductLangID', $this->lang_id);
		$this->db->where('ProductOptionLangID', $this->lang_id);
		$this->db->where('CategoryLangID', $this->lang_id);
		$this->db->where('ProductOptionFirstOption', 1);

		$this->db->order_by('FavoriteID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}
}