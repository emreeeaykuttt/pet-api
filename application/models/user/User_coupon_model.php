<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_coupon_model extends CI_Model {

	var $table = 'UserCoupons';
	var $users_table = 'Users';

	public function getAllByCouponContentID($coupon_content_id)
	{
		$this->db->where('UserCouponCouponContentID', $coupon_content_id);

		$this->db->join($this->users_table, 'UserID = UserCouponUserID');

		$this->db->order_by('UserCouponID', 'DESC');

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return FALSE;
		}
	}

    public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function deleteByIDAndCouponContentID($id, $coupon_content_id)
    {
    	$this->db->where('UserCouponID', $id);
    	$this->db->where('UserCouponCouponContentID', $coupon_content_id);
		$this->db->delete($this->table);
    }

    public function deleteByCouponContentID($coupon_content_id)
    {
    	$this->db->where('UserCouponCouponContentID', $coupon_content_id);
		$this->db->delete($this->table);
    }

    public function isThereUser($coupon_content_id, $user_id)
    {
    	$this->db->where('UserCouponCouponContentID', $coupon_content_id);
    	$this->db->where('UserCouponUserID', $user_id);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
    }
}