<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	var $table = 'Users';
    var $column_order = array('UserFirstName', 'UserEmail', 'UserPhone', 'UserGender', null);
    var $column_search = array('UserFirstName', 'UserEmail', 'UserPhone', 'UserGender');
    var $order = array('UserID' => 'desc');

    private function _getDatatablesQuery($data)
    {
        $this->db->select('*');
        
        $this->db->from($this->table);

        $i = 0;
    
        foreach ($this->column_search as $item)
        {
            if($data['search']['value'])
            {
                
                if($i===0)
                {
                    $this->db->group_start();

                    if ($item == 'UserFirstName') 
                    {
                        $this->db->like('CONCAT(UserFirstName, " ", UserLastName)', $data['search']['value']);
                    }
                    else
                    {
                        $this->db->like($item, $data['search']['value']);
                    }
                }
                else
                {
                    $this->db->or_like($item, $data['search']['value']);
                }

                if(count($this->column_search) - 1 == $i) 
                    $this->db->group_end(); 
            }
            $i++;
        }
        
        if(isset($data['order'])) 
        {
            $this->db->order_by($this->column_order[$data['order']['0']['column']], $data['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    public function getDatatables()
    {
        $data = $this->get_data;
        $this->_getDatatablesQuery($data);

        if($data['length'] != -1)
        {
            $this->db->limit($data['length'], $data['start']);
        }

        $query = $this->db->get();
        return $query->result();
    }

    public function getCountFiltered()
    {
        $data = $this->get_data;
        $this->_getDatatablesQuery($data);

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getCountAll()
    {
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }

    public function getByID($id)
    {
        $this->db->where('UserID', $id);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
        else
        {
            return FALSE;
        }
    }

	public function getByEmail($email)
	{
		$this->db->where('UserEmail', $email);

		$query = $this->db->get($this->table);

		if ($query->num_rows() > 0)
		{
			return $query->row();
		}
		else
		{
			return FALSE;
		}
	}

    public function getAllByNameAndSurname($name_and_surname)
    {
        $this->db->like('CONCAT(UserFirstName, " ", UserLastName)', $name_and_surname);

        $query = $this->db->get($this->table);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function passwordValidate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('UserEmail', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->UserPassword;

            if (password_verify($password, $hashed_password) && $query->UserVerified == 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

	public function validate($email, $password)
    {
        $query = $this->db->select('*')->from($this->table)->where('UserEmail', $email)->get()->row();

        if ($query != "")
        {
            $hashed_password = $query->UserPassword;
            $id = $query->UserID;

            if (password_verify($password, $hashed_password) && $query->UserVerified == 1)
            {
                $last_login = date('Y-m-d H:i:s', NOW_DATE_TIME);
                $this->db->trans_start();
                $this->db->trans_strict(FALSE);
                $this->db->update($this->table, array('UserLastLogin' => $last_login, 'UserLastLoginIpAddress' => $this->input->ip_address()), array('UserID' => $id));
                $this->db->trans_complete();
                if ($this->db->trans_status() === FALSE)
                {
                    $this->db->trans_rollback();
                    return FALSE;
                }
                else
                {
                    $this->db->trans_commit();
                    return $query;
                }
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function verification($verification_token, $id)
    {
        $this->db->update($this->table, 
        	array('UserVerified' => 1, 'UserVerificationDate' => date('Y-m-d H:i:s', NOW_DATE_TIME)), 
        	array('UserID' => $id, 'UserVerificationToken' => $verification_token)
        );

        return $this->db->affected_rows();
    }

    public function passwordUpdate($data, $remember_token, $id)
    {
    	$this->db->update($this->table, $data, array('UserID' => $id, 'UserRememberToken' => $remember_token, 'UserVerified' => 1));

    	return $this->db->affected_rows();
    }

}